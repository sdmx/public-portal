FROM node:9-alpine

COPY . /app/soenda/public-portal
WORKDIR /app/soenda/public-portal

COPY .env.docker /app/soenda/public-portal/.env
COPY config/app.docker.json /app/soenda/public-portal/config/app.json

RUN sh -c "yarn"
# RUN sh -c "yarn build"

EXPOSE 3001
# ENTRYPOINT ["sh", "-c", "yarn prod"]
ENTRYPOINT ["sh", "-c", "yarn dev"]
