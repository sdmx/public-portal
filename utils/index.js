export default {
  isNumeric: (value) => {
    return ((typeof value === 'number' || typeof value === 'string') && !isNaN(Number(value)));
  },
  truncate: (str, len, suffix='...') => {
    return str.length > len ? str.substring(0, len) + suffix : str;
  },
}
