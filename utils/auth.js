import axios from 'axios';
import _ from 'lodash';

import redirect from './redirect';

const fetchAsync = (req, success, error) => {
  const funcSuccess = _.isFunction(success);
  const funcError = _.isFunction(error);

  if (funcSuccess || funcError) {
    if (funcSuccess) req.then(success);
    if (funcError) req.catch(error);

    return req;
  }

  return false;
};

const Auth = {
  async check(success, error) {
    const req = axios.get('/auth/check');

    // Asynchronous
    const fetch = fetchAsync(req, success, error);
    if (fetch) return fetch;

    // Synchronous
    try {
      await req;
      return true;
    }
    catch(err) {
      return false;
    }
  },

  user(success, error) {
    const req = axios.get('/auth/user');

    if (_.isFunction(success)) req.then(res => success(res.data));
    if (_.isFunction(error)) req.catch(res => error(res.data));
  },

  hasRole(roles) {
    try {
      const user = Auth.user();

      if (_.isArray(roles)) {
        return _.intersection(user.roles, roles).length === roles.length;
      }

      return _.includes(user.roles, roles);
    }
    catch (err) {
      return false;
    }
  },

  redirectIfAuthenticated(res) {
    Auth.check(() => redirect('/', res));
  },

  redirectIfNotAuthenticated(res) {
    Auth.check(null, () => redirect('/', res));
  },
};

export default Auth;
