import Router from 'next/router';

export default (url, res = null) => {
  if (res) {
    res.redirect(url);
    res.end();
  }
  else Router.replace(url);
};
