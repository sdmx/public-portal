const config = {
  company: {
    name: 'SOENDA',
    address: 'PH.H. Mustofa Surapati Core K-18 Bandung, West Java 40192',
    contact: '(+6222) 87241349',
    email: 'admin@saranaprimadata.co.id',
    social: {
      youtube: 'https://www.youtube.com/user/BankIndonesiaChannel',
      twitter: 'https://twitter.com/bank_indonesia?lang=en',
      facebook: 'https://id-id.facebook.com/BankIndonesiaOfficial/',
      instagram: 'https://www.instagram.com/bank_indonesia/',
      playstore: 'https://play.google.com/store/apps/details?id=com.bi.mobile&hl=en',
      appstore: 'https://itunes.apple.com/id/app/bi-mobile/id1198899727?mt=8',
    },
  },
  links: {
    'internal-portal': {
      name: 'Registered Publication System',
      url: process.env.INTERNAL_PORTAL_URL,
    },
    'dashboard-executive': {
      name: 'Executive Information System',
      url: process.env.EXECUTIVE_DASHBOARD_URL,
    },
    'data-analytic': {
      name: 'Data Analyst Information System',
      url: process.env.DATA_ANALYTIC_URL,
    },
    geoanalytic: {
      name: 'Geospatial Analytic',
      url: process.env.GEOSPATIAL_ANALYTIC_URL,
    },
    'data-scientist': {
      name: 'Data Scientist Information System',
      url: process.env.DSIS_URL,
    },
    admin: {
      name: 'Administrator',
      url: process.env.ADMIN_PORTAL_URL,
    },
  },
};

export default (path = null) => {
  if (path) {
    const env = process.env[path];

    if (env !== undefined) {
      return env;
    }

    let res = config;

    path.split('.').map((key) => {
      res = res[key];
    });

    return res;
  }
  return config;
};
