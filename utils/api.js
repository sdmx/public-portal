import _ from 'lodash';
import axios from 'axios';
import store from 'store';
import expirePlugin from 'store/plugins/expire';

// const expirePlugin = require('store/plugins/expire');
store.addPlugin(expirePlugin);

let token = null;

axios.interceptors.request.use(
  (config) => {
    const { headers = {} } = config;
    // headers.Authorization = `Bearer ${token}`;

    return { ...config, headers };
  },
  error => Promise.reject(error),
);

export const DataService = {
  endpoint: process.env.SDMX_ENDPOINT,
  getDataflows(provider, query, pageNum = 1, pageSize = 10, queryParams = {}) {
    return axios.get(`${DataService.endpoint}/data/sdmx/${provider}`, {
      params: _.omitBy({
        ...queryParams,
        q: query,
        size: pageSize,
        page: pageNum,
      }, v => v === null || v === undefined),
    });
  },
  /* Fame only */
  filter(provider, query, pageNum = 1, pageSize = 10, queryParams = {}) {
    const formData = new FormData();
    formData.append('query', query);
    formData.append('size', pageSize);
    formData.append('page', pageNum);

    Object.keys(queryParams).forEach((k) => {
      formData.append(k, queryParams[k]);
    });

    return axios.post(`${DataService.endpoint}/data/${provider}`, formData, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
    });
  },
  getDataset(provider, id, key = null, startPeriod = null, endPeriod = null) {
    const params = _.omitBy({ startPeriod, endPeriod }, _.isNull);

    if (_.isArray(key)) {
      const formData = new FormData();
      formData.append('obs', key.join('|'));

      Object.keys(params).forEach((k) => {
        formData.append(k, decodeURIComponent(params[k]));
      });

      return axios.post(`${DataService.endpoint}/data/sdmx/${provider}/${id}`, formData, {
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
      });
    }

    return axios.get(`${DataService.endpoint}/data/sdmx/${provider}/${id}`, params);
  },
  getDimensions(dataIds) {
    const formData = new FormData();

    if (_.isArray(dataIds)) {
      dataIds.forEach(dataId => formData.append('dataIds', dataId));
    }
    else {
      formData.append('dataIds', dataIds);
    }

    return axios.post(`${DataService.endpoint}/data/query/dimensions`, formData, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
    });
  },
  getSeries(dataIds, filter = {}, startPeriod = null, endPeriod = null) {
    const formData = new FormData();

    // key filter
    Object.keys(filter).forEach((k) => {
      if (!_.isNil(filter[k])) {
        formData.append(`filter[${k}]`, filter[k]);
      }
    });

    // time period filter
    if (startPeriod) {
      formData.append("startPeriod", startPeriod);
    }

    if (endPeriod) {
      formData.append("endPeriod", endPeriod);
    }

    // dataset ids
    if (_.isArray(dataIds)) {
      dataIds.forEach(dataId => formData.append('dataIds', dataId));
    }
    else {
      formData.append('dataIds', dataIds);
    }

    return axios.post(`${DataService.endpoint}/data/query/series`, formData, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
    });
  },
  getCategories(provider, key = null) {
    const params = {};

    if (key) {
      params.key = key;
    }

    return axios.get(`${process.env.REPORT_ENDPOINT}/category/${provider}`, { params });
  },
};

export const Metadata = {
  list: () =>
    axios.get(`${process.env.METADATA_ENDPOINT}/all`),
  getStructure: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/structure/${id}`),
  getData: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/data/${id}`),
  getDataByDataflow: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/dataByDataflow/${id}`),
  getDataflowMapping: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/map/${id}`),
};

export const Content = {
  privilege: process.env.CONTENT_PRIVILEGE,
  endpoint: process.env.CMS_ENDPOINT,
  status: [
    { id: 'draft', value: '0', text: 'Draft', className: 'bg-light-gray black-80' },
    { id: 'submitted', value: '1', text: 'Submitted', className: 'bg-dark-pink white' },
    { id: 'approved', value: '2', text: 'Approved', className: 'bg-dark-green white' },
    { id: 'rejected', value: '3', text: 'Rejected', className: 'bg-dark-red white' },
  ],
  fileUrl(filepath) {
    return `${Content.endpoint}/file/${filepath}`;
  },
  downloadUrl(filepath) {
    return `${Content.endpoint}/download/${filepath}`;
  },
  get(contentType, id) {
    return axios.get(`${Content.endpoint}/content/${contentType}/${Content.privilege}/${id}`);
  },
  list(contentType, q = '', limit = 10, offset = 0) {
    return axios.get(`${Content.endpoint}/content/${contentType}/${Content.privilege}`, {
      params: { limit, offset, q },
    });
  },
  create(contentType, formData) {
    return axios.post(`${Content.endpoint}/content/${contentType}/${Content.privilege}`, formData, {
      headers: { 'content-type': 'multipart/form-data' },
    });
  },
  update(contentType, formData, id) {
    return axios.put(`${Content.endpoint}/content/${contentType}/${Content.privilege}/${id}`, formData, {
      headers: { 'content-type': 'multipart/form-data' },
    });
  },
  delete(contentType, id) {
    return axios.delete(`${Content.endpoint}/content/${contentType}/${Content.privilege}/${id}`);
  },
};

export const Auth = {
  init() {
    return new Promise((resolve, reject) => {
      const cache = store.get('token');

      if (cache) {
        resolve(cache);
        Auth.setToken(cache);
      }
      else {
        Auth.getToken()
          .then(({ data }) => {
            resolve(data);
            store.set('token', data, new Date().getTime() + (1000 * 60 * 10));
            Auth.setToken(data);
          })
          .catch(err => reject(err));
      }
    });
  },
  user() {
    return new Promise((resolve, reject) => {
      const cache = store.get('user');

      if (cache) {
        resolve(cache);
      }
      else {
        axios.get('/auth/user')
          .then(({ data }) => {
            store.set('user', data, new Date().getTime() + (1000 * 60 * 10));
            resolve(data);
          })
          .catch(e => reject(e));
      }
    });
  },
  logout(doRedirect = true) {
    store.remove('user');
    store.remove('token');

    if (doRedirect) {
      window.location = '/auth/logout';
    }
  },
  token() {
    return new Promise((resolve, reject) => {
      axios.get('/auth/token')
        .then(({ data }) => resolve(data))
        .catch(err => reject(err));
    });
  },
  getRoles() {
    return axios.get(`${process.env.AUTH_ENDPOINT}/auth/roles`);
  },
  getToken() {
    return axios.get('/auth/token');
  },
  setToken(_token) {
    token = _token;
  },
};

export const Report = {
  endpoint: process.env.REPORT_ENDPOINT,
  list(search = '', limit = 10, offset = 0) {
    return axios.get(`${Report.endpoint}/reports`, {
      params: { limit, offset, search },
    });
  },
  get(reportPath) {
    return axios.get(`${Report.endpoint}/report/desc/${reportPath}`);
  },
  exportUrl(type, reportPath) {
    return `${Report.endpoint}/report/export/${type}?file=${reportPath}`;
  },
};

export const Storage = {
  set(key, value, expireTime = null) {
    store.set(key, value, expireTime);
  },
  get(key, defaultValue) {
    const value = store.get(key);

    if (value === undefined) {
      return defaultValue;
    }

    return value;
  },
  has(key) {
    return store.get(key) !== undefined;
  },
  remove(key) {
    store.remove(key);
  },
};
