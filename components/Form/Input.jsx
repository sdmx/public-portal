import React from 'react';
import PropType from 'prop-types';
import { Icon, Input as AntInput, Select } from 'antd';
import dynamic from 'next/dynamic';

const ReactQuill = dynamic(import('react-quill'), { ssr: false });

class Input extends React.Component {
  render() {
    const {
      name,
      type,
      large,
      icon,
      value,
      ...attrs
    } = this.props;

    switch (type) {
      case 'textarea':
        return (
          <AntInput.TextArea
            type="file"
            name={name}
            value={value}
            {...attrs}
          />
        );

      case 'editor':
        return (
          <ReactQuill
            modules={{
              toolbar: [
                [{'header': '1'}, {'header': '2'}, {'font': []}],
                [{size: []}],
                ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                [
                  {'list': 'ordered'},
                  {'list': 'bullet'},
                  {'indent': '-1'},
                  {'indent': '+1'},
                ],
                ['link', 'image', 'video'],
                ['clean']
              ],
              clipboard: {
                // toggle to add extra line breaks when pasting HTML:
                matchVisual: false,
              }
            }}
            value={value || ''}
            {...attrs}
          />
        );

      case 'file':
        return (
          <input
            type="file"
            name={name}
            {...attrs}
            value={undefined}
          />
        );

      case 'select':
        const { options = [], ...selectAttrs } = attrs;

        return (
          <Select defaultValue={value} {...selectAttrs}>
            {options.map(opt => (
              <Select.Option key={opt.value} value={opt.value}>
                {opt.label || opt.value}
              </Select.Option>
            ))}
          </Select>
        );

      case 'password':
        if (large) {
          attrs.size = "large";
        }

        return (
          <AntInput
            type={type}
            name={name}
            prefix={icon && <Icon type={icon} />}
            {...attrs}
          />
        );

      default:
        if (large) {
          attrs.size = "large";
        }

        return (
          <AntInput
            type={type}
            name={name}
            prefix={icon && <Icon type={icon} />}
            defaultValue={value}
            {...attrs}
          />
        );
    }
  }
}

Input.propTypes = {
  type: PropType.string,
  name: PropType.string,
  icon: PropType.string,
  large: PropType.bool,
};

Input.defaultProps = {
  type: 'text',
  name: '',
  icon: null,
  large: false,
};

export default Input;
