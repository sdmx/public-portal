import React from 'react';
import PropType from 'prop-types';
import classnames from 'classnames';

import Icon from '~/components/Icon';

const Input = ({
  type,
  value,
  icon,
  children,
}) => (
  <button
    type={type}
    value={value}
    className={classnames('')}
  >
    {icon !== null ? (<Icon type={icon} />) : ''}
    {children}
  </button>
);

Input.propTypes = {
  type: PropType.string,
  icon: PropType.string,
  value: PropType.node,
  children: PropType.node,
};

Input.defaultProps = {
  type: 'button',
  children: '',
  icon: null,
  value: true,
};

export default Input;
