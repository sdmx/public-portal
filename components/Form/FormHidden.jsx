import React from 'react';

export default class FormHidden extends React.Component {
  render() {
    const {
      method = 'post', data = [], children, formRef, ...props
    } = this.props;

    return (
      <form method={method} ref={formRef} {...props}>
        {Object.keys(data).map(name => (
          <input type="hidden" key={name} name={name} value={data[name]} />
        ))}

        {children}
      </form>
    );
  }
}
