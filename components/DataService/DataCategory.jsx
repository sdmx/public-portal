import React from 'react';
import { message, Tree } from 'antd';
import Router from 'next/router';
import _ from 'lodash';

import Loading from '~/components/Loading';
import { DataService } from '~/utils/api';

class DataCategory extends React.Component {
  state = {
    categories: [],
    loading: false,
  };

  componentDidMount() {
    this.fetchCategories(this.props.source);
  }

  componentWillReceiveProps({ source }) {
    if (source !== this.props.source) {
      this.fetchCategories(source);
    }
  }

  onLoadData = treeNode => new Promise((resolve) => {
    if (treeNode.props.children) {
      resolve();
      return;
    }

    DataService.getCategories(this.props.source, treeNode.props.eventKey)
      .then(({ data }) => {
        const { dataRef } = treeNode.props;
        dataRef.children = _.isArray(data) ? data : [];

        this.setState({ categories: [...this.state.categories] });
        resolve();
      })
      .catch(() => resolve());
  })

  fetchCategories = (source, key = null) => {
    this.setState({ loading: true });

    DataService.getCategories(source, key)
      .then(({ data }) => {
        this.setState({ categories: _.isArray(data) ? data : [], loading: false });
      })
      .catch((err) => {
        this.setState({ loading: false });
        message.error(err.message);
      });
  }

  renderTreeNodes = data => data.map((item) => {
    const dims = item.code.split('.');
    const title = (dims.length > 2 ? `${_.last(dims)} : ` : '') + item.name;

    if (item.children) {
      return (
        <Tree.TreeNode key={item.code} dataRef={item} title={title}>
          {this.renderTreeNodes(item.children)}
        </Tree.TreeNode>
      );
    }

    return (
      <Tree.TreeNode
        key={item.code}
        dataRef={item}
        title={title}
        isLeaf={item.type === 'dataflow'}
      />
    );
  })

  render() {
    const { loading, categories = [] } = this.state;
    const { onSelect } = this.props;

    const loadTreeNode = (key, { node }) => {
      const { isLeaf, eventKey } = node.props;

      if (isLeaf) {
        Router.push(`/data/${this.props.source}/view/${eventKey}`);
      }
      else if (_.isFunction(onSelect)) {
        onSelect(eventKey);
      }
    };

    return (
      <Loading isFinish={!loading}>
        <div className="overflow-hidden w-100 h-100">
          <Tree.DirectoryTree
            multiple
            loadData={this.onLoadData}
            defaultExpandedKeys={categories.length > 0 ? [categories[0].code] : []}
            onLoad={loadTreeNode}
            onSelect={loadTreeNode}
          >
            {this.renderTreeNodes(categories)}
          </Tree.DirectoryTree>
        </div>
      </Loading>
    );
  }
}

export default DataCategory;
