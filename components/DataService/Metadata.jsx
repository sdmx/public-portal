import React from 'react';
import moment from 'moment';

import { Metadata } from '~/utils/api';
import Loading from '~/components/Loading';

export default class MetadataComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metadataset: [],
      loading: true,
    };

    this.fetchMetadataset = this.fetchMetadataset.bind(this);
  }

  componentDidMount() {
    const { dataflowId } = this.props;

    this.fetchMetadataset(dataflowId);
  }

  fetchMetadataset = (dataflowId) => {
    Metadata.getDataByDataflow(dataflowId)
      .then(({ data }) => {
        this.setState({ metadataset: data.metadataset, loading: false });
      })
      .catch((err) => {
        this.setState({ loading: false });
      });
  }

  renderMetadata = (metadatasets) => {
    return (
      <div>
        {metadatasets.map(({ id, label, type, value, child = [] }) => {
          let formattedValue;

          switch (type.toLowerCase()) {
            case 'datetime':
              formattedValue = moment(value, 'x').format('DD MMM YYYY');
              break;

            default:
              formattedValue = value;
              break;
          }

          return (
            <div className="br2 pa1 bg-white ba b--moon-gray mv1">
              <div className="pa1 cf">
                <div className="fl b pr1" style={{ fontSize: '10px' }}>{`${label}:`}</div>
                <div className="fl" style={{ fontSize: '10px' }}>{formattedValue}</div>
              </div>
              {child.length > 0 && this.renderMetadata(child)}
            </div>
          );
        })}
      </div>
    );
  }

  render() {
    const { title } = this.props;
    const { metadataset, loading } = this.state;

    return (
      <div className="pl3 h-100 overflow-y-auto overflow-x-hidden relative">
        <Loading isFinish={!loading}>
          <h1>{title}</h1>
          {this.renderMetadata(metadataset)}
        </Loading>
      </div>
    );
  }
}
