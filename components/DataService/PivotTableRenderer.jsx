import React from 'react';
import { PivotData } from 'react-pivottable/Utilities';
import _ from 'lodash';
import { Modal, Icon, Dropdown, Menu } from 'antd';

import PivotDetails from './PivotDetails';
import KeySeries from './KeySeries';

const MARK_EMPTY = '##empty##';

class PivotTableRenderer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      breakdown: {},
      detail: {
        visible: false,
        data: [],
      },
      keySeries: {
        visible: false,
        keySeries: '',
      },
    };

    this.showDetails = this.showDetails.bind(this);
    this.closeDetails = this.closeDetails.bind(this);

    this.showKeySeries = this.showKeySeries.bind(this);
    this.closeKeySeries = this.closeKeySeries.bind(this);
    this.loadChildren = this.loadChildren.bind(this);
  }

  getDimensionFilter(pivotData, rowKey, colKey) {
    const filter = {};

    pivotData.props.cols.map((attr, i) => { filter[attr] = colKey[i]; });
    pivotData.props.rows.map((attr, i) => { filter[attr] = rowKey[i]; });

    return filter;
  }

  showKeySeries(keySeries) {
    this.setState({ keySeries: { visible: true, keySeries } });
  }

  closeKeySeries() {
    this.setState({ keySeries: { visible: false } });
  }

  closeDetails() {
    this.setState({ detail: { visible: false } });
  }

  showDetails(data) {
    this.setState({ detail: { visible: true, data } });
  }

  loadChildren({ attrs, keys, parentKey, parentAttr, data }) {
    let tmpData = [ ...this.state.data ];
    const breakdown = { ...this.state.breakdown };

    // breakdown
    if (!breakdown[parentAttr]) {
      breakdown[parentAttr] = {
        keys: [],
        length: 0,
      };
    }

    // key formatting
    tmpData = tmpData.concat(data.map((item) => {
      item[parentAttr] = `${parentKey}$$${item[parentAttr]}`;
      breakdown[parentAttr].keys.push(parentKey);

      const breakdownLength = item[parentAttr].split('$$').length;

      if (breakdownLength > breakdown[parentAttr].length) {
        breakdown[parentAttr].length = breakdownLength;
      }

      return item;
    }));

    this.setState({ data: tmpData, breakdown });
  }

  render() {
    const { data, ...props } = this.props;
    const { opts, spanSize, redColorScaleGenerator } = PivotTableRenderer;
    const pivotData = new PivotData({ ...props, data: this.state.data });
    const colAttrs = pivotData.props.cols;
    const rowAttrs = pivotData.props.rows;
    let rowKeys = pivotData.getRowKeys();
    let colKeys = pivotData.getColKeys();
    const grandTotalAggregator = pivotData.getAggregator([], []);

    let valueCellColors = () => {};
    let rowTotalColors = () => {};
    let colTotalColors = () => {};

    const { detail, keySeries, breakdown } = this.state;
    const { dimensions, filter, router } = this.props;

    /** Sort column by name */
    if (colKeys.length > 0) {
      const sortParam = [];

      colKeys[0].forEach((colKey, i) => {
        sortParam.push(o => o[i]);
      })

      colKeys = _.sortBy(colKeys, sortParam);
    }

    if (rowKeys.length > 0) {
      const sortParam = [];

      rowKeys[0].forEach((colKey, i) => {
        sortParam.push(o => o[i]);
      })

      rowKeys = _.sortBy(rowKeys, sortParam);
    }

    // hierarcy horizontal columns header
    const headerColKeys = [...colKeys].map((xkeys) => {
      const keys = [...xkeys];

      const res = colAttrs.map((attr, i) => {
        const bdKeys = [];
        const key = xkeys[i];
        const splits = key.split('$$');
        const attrBreakdown = breakdown[attr];

        // breakdown selected as total
        if (attrBreakdown && _.includes(attrBreakdown.keys, key)) {
          if (splits.length > 1) {
            splits.forEach((splitKey, ii) => {
              bdKeys.push(splitKey);
            });
          }
          else {
            bdKeys.push(key);
          }

          bdKeys.push(`${key.replace(/[^\$\$]+\$\$/g, '')} (Total)`);
        }
        // breakdown as child
        else if (splits.length > 1) {
          splits.forEach((splitKey) => {
            bdKeys.push(splitKey);
          });
        }
        // fill empty mark
        else if (attrBreakdown) {
          bdKeys.push(key);

          for (let j = attrBreakdown.length - 1; j > 0; j--) {
            bdKeys.push(MARK_EMPTY);
          }
        }
        else {
          bdKeys.push(key);
        }

        if (attrBreakdown) {
          keys.splice(i, 1);

          // fill the rest with empty mark
          if (attrBreakdown.length > bdKeys.length) {
            for (let j = attrBreakdown.length - bdKeys.length; j > 0; j--) {
              bdKeys.push(MARK_EMPTY);
            }
          }
        }

        return bdKeys;
      });

      return _.flatten(res);
    });

    // console.log(rowKeys);

    // calculating total rowspan
    let hrowSpan = 0;

    colAttrs.forEach((attr) => {
      if (!!breakdown[attr]) {
        hrowSpan += breakdown[attr].length;
      }
      else hrowSpan++;
    });

    let rowCounter = 0;

    return (
      <div className="pvtWrapper">
        <table className="pvtTable">
          <thead>
            {colAttrs.map((c, i) => {
              const hasBreakdown = !!breakdown[c];
              const breakdownSpan = hasBreakdown ? breakdown[c].length : 1;
              const tableRows = [];
              const isLastRowAttr = i === colAttrs.length - 1;

              let j = 0;

              while (j < breakdownSpan) {
                const ky = i + j + rowCounter;
                const isLastRowLabel = j === breakdownSpan - 1;

                tableRows.push(
                  <tr key={`colAttr${i}-${j}`}>
                    {i === 0 && j === 0 && rowAttrs.length !== 0 && (
                      <th colSpan={rowAttrs.length} rowSpan={hrowSpan} />
                    )}
                    {j === 0 && (
                      <th className="pvtAxisLabel" rowSpan={breakdownSpan}>{c}</th>
                    )}
                    {headerColKeys.map((colKey, k) => {
                      const x = spanSize(headerColKeys, k, ky);
                      const label = colKey[ky];

                      if (x === -1 || label === MARK_EMPTY) {
                        return null;
                      }

                      let labelRowSpan = isLastRowAttr && isLastRowLabel && rowAttrs.length > 0 ? 2 : 1;

                      for (let l = ky; l <= breakdownSpan; l++) {
                        if (colKey[l] === MARK_EMPTY) {
                          labelRowSpan++;

                          if (isLastRowAttr) {
                            labelRowSpan++;
                          }
                        }
                        else if (l > ky) {
                          break;
                        }
                      }

                      return (
                        <th
                          key={`colKey${i}-${j}-${k}`}
                          className="pvtColLabel"
                          colSpan={x}
                          rowSpan={labelRowSpan}
                        >
                          <span dangerouslySetInnerHTML={{ __html: label }} />

                          {/* Hierarchy CodeList Breakdown Button (Horizontal Label) */}
                          {/* <button
                            type="button"
                            className="b bg-black-80 white bw0 ph1 pv0 br2 mh2 pointer"
                            onClick={(e) => {
                              //#DEV: Generate data samples
                              const childData = [];
                              const recordSample = this.props.data[0];

                              for (let dataCount = 2; dataCount > 0; dataCount--) {
                                const fieldName = 'x' + Math.round(Math.random()*1000);
                                const record = {
                                  ...recordSample,
                                  OBS_VALUE: Math.random()*50,
                                  [colAttrs[i]]: fieldName,
                                };

                                colKeys[k].forEach((ckey, ci) => {
                                  if (colAttrs[i] !== colAttrs[ci]) {
                                    record[colAttrs[ci]] = ckey;
                                  }
                                });

                                rowKeys.forEach((rkeys, ri) => {
                                  rkeys.forEach((rkey, rj) => {
                                    record[rowAttrs[rj]] = rkey;
                                    childData.push({ ...record });
                                  });
                                });
                              }

                              // load data into table
                              this.loadChildren({
                                parentAttr: colAttrs[i],
                                parentKey: colKeys[k][i],
                                data: childData,
                              });
                            }}
                          >
                           +
                          </button> */}
                        </th>
                      );
                    })}

                    {i === 0 && j === 0 && (
                      <th className="pvtTotalLabel" rowSpan={hrowSpan + (rowAttrs.length === 0 ? 0 : 1)}>
                        Totals
                      </th>
                    )}
                  </tr>
                );

                j++;
              }

              rowCounter += j - 1;
              return tableRows;
            })}

            {rowAttrs.length !== 0 && (
              <tr>
                {rowAttrs.map((r, i) => (
                  <th className="pvtAxisLabel" key={`rowAttr${i}`}> {r} </th>
                ))}
                <th className="pvtTotalLabel">
                  {colAttrs.length === 0 ? 'Totals' : null}
                </th>
              </tr>
            )}
          </thead>

          <tbody>
            {rowKeys.map((rowKey, i) => {
              const totalAggregator = pivotData.getAggregator(rowKey, []);

              return (
                <tr key={`rowKeyRow${i}`}>
                  {rowKey.map((txt, j) => {
                    const x = spanSize(rowKeys, i, j);

                    if (x === -1) {
                      return null;
                    }

                    return (
                      <th
                        key={`rowKeyLabel${i}-${j}`}
                        className="pvtRowLabel"
                        rowSpan={x}
                        colSpan={j === rowAttrs.length - 1 && colAttrs.length !== 0 ? 2 : 1}
                      >
                        <span
                          dangerouslySetInnerHTML={{
                            __html: txt.replace(/[^\$\$]+\$\$/g, '<span class="ph2">&nbsp;</span>')
                          }}
                        />

                        {/* Hierarchy CodeList Breakdown Button (Vertical Label) */}
                        {/* <button
                          type="button"
                          className="b bg-black-80 white bw0 ph1 pv0 br2 mh2 pointer"
                          onClick={(e) => {
                            // #DEV: Generate data samples
                            const childData = [];
                            const recordSample = this.props.data[0];

                            for (let dataCount = 2; dataCount > 0; dataCount--) {
                              const fieldName = 'x' + Math.round(Math.random()*100);
                              const record = {
                                ...recordSample,
                                OBS_VALUE: Math.random()*50,
                                [rowAttrs[j]]: fieldName,
                              };

                              rowKey.forEach((rkeys, ri) => {
                                if (rowAttrs[j] !== rowAttrs[ri]) {
                                  record[rowAttrs[ri]] = rkeys;
                                }
                              });

                              colKeys.forEach((ckeys, ci) => {
                                ckeys.forEach((ckey, cj) => {
                                  record[colAttrs[cj]] = ckey;
                                  childData.push({ ...record });
                                });
                              });
                            }

                            // load data into table
                            this.loadChildren({
                              attrs: rowAttrs,
                              keys: rowKey,
                              parentAttr: rowAttrs[j],
                              parentKey: txt,
                              data: childData,
                            });
                          }}
                        >
                         +
                        </button> */}
                      </th>
                    );
                  })}
                  {colKeys.map((colKey, j) => {
                    const aggregator = pivotData.getAggregator(rowKey, colKey);
                    const value = aggregator.value();

                    return (
                      <td
                        className="pvtVal"
                        key={`pvtVal${i}-${j}`}
                        style={valueCellColors(rowKey, colKey, value)}
                      >
                        {value === null ? '' : aggregator.format(value)}

                        <Dropdown
                          trigger={['click']}
                          overlay={(
                            <Menu>
                              {/* Key Series */}
                              {/*<Menu.Item
                                  key="0"
                                  onClick={() => {
                                    const dimensionFilter = this.getDimensionFilter(pivotData, rowKey, colKey);
                                    const keySeriesArr = [];
                                    const keySeriesFilter = filter.key.split('.');

                                    dimensions.map((dimension, i) => {
                                      keySeriesArr.push(_.has(dimensionFilter, dimension.id)
                                          ? dimensionFilter[dimension.id]
                                          : keySeriesFilter[i]);
                                    });

                                    this.showKeySeries(keySeriesArr.join('.'));
                                  }}
                                >
                                  <Icon type="line-chart" /> Key Series
                                </Menu.Item>*/}
                              <Menu.Item
                                key="1"
                                onClick={() => {
                                  const dimensionFilter = this.getDimensionFilter(pivotData, rowKey, colKey);
                                  const data = _.filter(pivotData.props.data, dimensionFilter);

                                  this.showDetails(data);
                                }}
                              >
                                <Icon type="bars" /> Detail
                              </Menu.Item>
                            </Menu>
                          )}
                        >
                          <button className="pointer fr val-opt bw0 bg-white pt1 pl1 pr0">
                            <Icon type="ellipsis" className="rotate-90" />
                          </button>
                        </Dropdown>
                      </td>
                    );
                  })}
                  <td
                    className="pvtTotal"
                    style={colTotalColors(totalAggregator.value())}
                  >
                    {totalAggregator.format(totalAggregator.value())}
                  </td>
                </tr>
              );
            })}

            <tr>
              <th
                className="pvtTotalLabel"
                colSpan={rowAttrs.length + (colAttrs.length === 0 ? 0 : 1)}
              >
                Totals
              </th>

              {colKeys.map((colKey, i) => {
                const totalAggregator = pivotData.getAggregator([], colKey);
                return (
                  <td
                    className="pvtTotal"
                    key={`total${i}`}
                    style={rowTotalColors(totalAggregator.value())}
                  >
                    {totalAggregator.format(totalAggregator.value())}
                  </td>
                );
              })}

              <td className="pvtGrandTotal">
                {grandTotalAggregator.format(grandTotalAggregator.value())}
              </td>
            </tr>
          </tbody>
        </table>

        {/* Cell Detail Modal */}
        <Modal
          title="Details"
          footer={null}
          visible={detail.visible}
          onCancel={this.closeDetails}
          width={760}
        >
          <PivotDetails {...detail} />
        </Modal>

        {/* Key Series Modal */}
        {/* <Modal
          title={null}
          footer={null}
          visible={keySeries.visible}
          onCancel={this.closeKeySeries}
          bodyStyle={{ padding: 0 }}
          style={{ top: '50px' }}
          width={960}
        >
          <KeySeries
            dataId={router.query.id}
            dimensions={dimensions}
            source={router.query.source}
            {...keySeries}
          />
        </Modal> */}
      </div>
    );
  }
}

PivotTableRenderer.defaultProps = PivotData.defaultProps;
PivotTableRenderer.propTypes = PivotData.propTypes;

export default PivotTableRenderer;
