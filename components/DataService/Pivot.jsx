import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import dynamic from 'next/dynamic';

import createPlotlyRenderers from '~/components/DataService/PlotlyRenderers';

import PivotTableUI from './PivotTableUI';
import TableRenderer from './TableRenderer';

const Plot = dynamic(import('react-plotly.js'), { ssr: false });

class Pivot extends React.PureComponent {
  constructor(props) {
    super(props);
    const { defaultPrefer = {} } = props;

    this.state = {
      rows: defaultPrefer.yAxis,
      cols: defaultPrefer.xAxis,
      vals: defaultPrefer.obsVal || ['OBS_VALUE'],
      aggregatorName: 'Sum',
      ...props,
    };
  }

  componentDidUpdate() {
    const { rendererName } = this.props;
    this.setState({ rendererName });
  }

  render() {
    const { series, dimensions } = this.props;
    const data = series.map((row) => {
      const res = { ...row };

      Object.keys(row).forEach((k) => {
        dimensions.forEach(({ id, codeList = {}, concept = {} }) => {
          if (id === k) {
            const { codes = {} } = codeList;
            delete res[k];
            res[concept.name] = _.has(codes, row[k]) ? `${codes[row[k]]} (${row[k]})` : row[k];
          }
        });
      });

      return res;
    });

    return (
      <div>
        <div className="table-responsive w-100">
          <PivotTableUI
            onChange={s => this.setState(s)}
            renderers={Object.assign({}, TableRenderer, createPlotlyRenderers(Plot))}
            data={data}
            dimensions={dimensions}
            {...this.state}
          />
        </div>
      </div>
    );
  }
}

export default Pivot;
