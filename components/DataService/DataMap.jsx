import React from 'react';
import numeral from 'numeral';
import _ from 'lodash';

import geoWorld from './geo/world.json';

export default class DataMap extends React.Component {
  static defaultProps = {
    // accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
    accessToken: '6170aad10dfd42a38d4d8c709a536f38',
    lat: 0.3893,
    lng: 121.9213,
    zoom: 4,
    colorGrades: ['#800026', '#BD0026', '#E31A1C', '#FC4E2A', '#FD8D3C', '#FEB24C', '#FED976', '#FFEDA0'],
    series: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      ReactLeaflet: null,
      geoData: [],
      grades: [],
      displayFeature: null,
    };
  }

  componentDidMount() {
    this.setState({
      ReactLeaflet: require('react-leaflet'),
      Control: require('react-leaflet-control').default,
    });

    const { series } = this.props;

    this.initGeoData(series);
    this.setMap = node => { if (node) this.map = node.leafletElement; };
    this.setGeoJson = node => { if (node) this.geoJson = node.leafletElement; };

    this.timeout = setTimeout(() => {
      // set view
      const bounds = [];

      this.map.eachLayer((layer) => {
        bounds.push(layer._bounds);
      });

      this.map.fitBounds(bounds);
    }, 100);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  initGeoData(series) {
    const geoData = [];
    const valDimension = 'OBS_VALUE';

    const geoValues = _.transform(
      _.groupBy(series, (item) => item.GEO),
      (res, item, key) => {
        if (_.isUndefined(res[key])) {
          res[key] = 0;
        }

        item.map((val) => {
          res[key] += val[valDimension];
        })
      },
      {}
    );

    console.log('geo values', geoValues);

    geoWorld.features.map(({ id, properties, ...attr }) => {
      const density = geoValues[id];

      if (!_.isUndefined(density)) {
        geoData.push({
          ...attr,
          properties: {
            ...properties,
            density: density,
          },
        });
      }
    });

    this.setState({ geoData });
    this.initGrades(_.flow(_.values, _.max)(geoValues));
  }

  initGrades(maxValue) {
    const maxValueLength = Math.round(maxValue).toString().length;
    const rangeLen = this.props.colorGrades.length;
    const basis = maxValueLength > 5 ? 2 : 1;
    const intervalLen = new String(Math.round(maxValue / rangeLen));
    const intervalTmpl = (new Array(intervalLen.length + 1 - basis)).join("0");
    const interval = parseInt(intervalLen.substr(0, basis) + intervalTmpl);
    const grades = [];

    for (var i = 0; i < rangeLen; i++) {
      grades[i] = interval * (rangeLen - i - 1);
    }

    this.setState({ grades });
  }

  getColor(density) {
    for (var i in this.state.grades) {
      if (density > this.state.grades[i] && !_.isUndefined(this.props.colorGrades[i])) {
        return this.props.colorGrades[i];
      }
    }

    return this.props.colorGrades[this.props.colorGrades.length - 1];
  }

  render() {
    if (!this.state.ReactLeaflet || !this.state.Control) {
      return null;
    }

    const { lat, lng, zoom, accessToken, colorGrades } = this.props;
    const position = [lat, lng];
    const { Map, TileLayer, GeoJSON, Tooltip, LayersControl, FeatureGroup, Popup, Circle } = this.state.ReactLeaflet;
    const Control = this.state.Control;

    return (
      <Map
        ref={this.setMap}
        style={{ height: '400px', width: '100%', zIndex: 0 }}
        maxZoom={18}
        center={position}
        zoom={zoom}
      >
        <TileLayer
          url={`https://{s}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=${accessToken}`}
        />
        <GeoJSON
          ref={this.setGeoJson}
          data={{ type: 'FeatureCollection', features: this.state.geoData }}
          style={(feature) => ({
            fillColor: this.getColor(feature.properties.density),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7,
          })}
          onEachFeature={(feature, geoLayer) => {
            const self = this;

            geoLayer.on({
              mouseover(e) {
                const layer = e.target;

                layer.setStyle({
                  weight: 6,
                  color: '#666',
                  dashArray: '',
                  fillOpacity: 1,
                });

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                  layer.bringToFront();
                }

                self.setState({ displayFeature: layer.feature });
              },
              mouseout(e) {
                self.geoJson.resetStyle(e.target);
                self.setState({ displayFeature: null });
              },
              click(e) {
                self.map.fitBounds(e.target.getBounds());
              },
            });
          }}
        >
          {this.state.displayFeature && (
            <Tooltip>
              <div>
                <h4 className="ma0 pa0">{this.state.displayFeature.properties.name}</h4>
                <span className="gray">
                  {numeral(this.state.displayFeature.properties.density).format('0,0.0')}
                </span>
              </div>
            </Tooltip>
          )}
        </GeoJSON>

        {/* Legend */}
        <Control position="bottomright">
          <div className="bg-white br2 pa2">
            {this.state.grades.map((grade, i) => (
              <div className="cf" key={i}>
                <div
                  className="fl mr2"
                  style={{
                    background: colorGrades[i],
                    width: '40px',
                    height: '20px',
                  }}
                >
                </div>
                <span className="fl gray">{numeral(grade).format('0,0')}</span>
              </div>
            ))}
          </div>
        </Control>
      </Map>
    );
  }
}

