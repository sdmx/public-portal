import React from 'react';
import Echart from 'echarts-for-react';
import _ from 'lodash';
import { Select, Dropdown, Icon } from 'antd';

export const chartTypes = [
  { id: 'line', name: 'Line' },
  { id: 'bar', name: 'Bar' },
  { id: 'scatter', name: 'Scatter Plot' },
  { id: 'area', name: 'Area' },
];

class Chart extends React.Component {
  constructor(props) {
    super(props);

    const {
      type = chartTypes[0].id,
      xAxis = 'TIME_PERIOD',
      yAxis = 'GEO',
      obsVal = 'OBS_VALUE',
    } = props;

    this.state = {
      type, xAxis, yAxis, obsVal, dataExport: {},
    };
    this.chartRef = React.createRef();
    this.formExportRef = React.createRef();
  }

  getOption = (type, xAxis, yAxis, obsVal, series) => {
    const chartOpt = {
      title: {},
      tooltip: {},
      legend: {
        type: 'scroll',
      },
      xAxis: {},
      yAxis: {},
      series: [],
      dataZoom: [
        {
          type: 'slider',
          xAxisIndex: 0,
          start: 0,
          end: 100,
        },
      ],
    };

    const dataGroup = _.transform(
      _.groupBy(series, yAxis),
      (result, values, yAxis) => {
        result[yAxis] = {};

        values.forEach((item) => {
          const value = item[obsVal];

          if (_.isUndefined(result[yAxis][item[xAxis]])) {
            result[yAxis][item[xAxis]] = value;
          } else {
            result[yAxis][item[xAxis]] += value;
          }
        });
      },
      {},
    );

    console.log(type, xAxis, yAxis, obsVal, series);
    console.log(series, dataGroup);
    Object.keys(dataGroup).forEach((yAxis) => {
      let yAxisTmp = yAxis;
      const dataSeries = [];
      const xAxisColumns = [];

      Object.keys(dataGroup[yAxis]).forEach((xAxis) => {
        if (yAxisTmp === yAxis) {
          xAxisColumns.push(xAxis);
        }
        else {
          yAxisTmp = yAxis;
        }

        dataSeries.push(dataGroup[yAxis][xAxis]);
      });

      chartOpt.xAxis.data = xAxisColumns;

      switch (type) {
        case 'pie':
          chartOpt.tooltip = {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)",
          };
          chartOpt.series.push({
            name: yAxis,
            type: 'pie',
            data: dataSeries,
            areaStyle: {},
            radius : '55%',
            center: ['40%', '50%'],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)',
              },
            },
          });
          break;

        case 'area':
          chartOpt.series.push({
            name: yAxis,
            type: 'line',
            data: dataSeries,
            areaStyle: {},
          });
          break;

        default:
          chartOpt.series.push({
            name: yAxis,
            type,
            data: dataSeries,
          });
          break;
      }
    });

    console.log(chartOpt);
    return chartOpt;
  }

  render() {
    const { dimensions = [], enableSwitch = true, enableExport = true, series } = this.props;
    const { dataExport = {}, type, xAxis, yAxis, obsVal } = this.state;
    const exportTypes = [
      { type: 'png', icon: 'picture', label: 'PNG' },
      { type: 'pdf', icon: 'file-pdf', label: 'PDF' },
      { type: 'xls', icon: 'file-excel', label: 'Excel' },
    ];

    return (
      <div className="bg-white draw-chart">
        <div className="pa2 bg-white db flex justify-end bb mb1 b--light-gray">
          {enableSwitch && (
            <div className="mr2">
              <Select defaultValue={yAxis} onChange={val => this.setState({ yAxis: val })}>
                {dimensions.map(dimension => !dimension.timeDimension && (
                  <Select.Option key={dimension.id} value={dimension.id}>
                    {dimension.concept.name}
                  </Select.Option>
                ))}
              </Select>
            </div>
          )}
          {enableExport && (
            <Dropdown
              trigger={['click']}
              overlay={(
                <div className="bg-white ba b--black-10 br2 pv2 ph1 shadow-1 flex">
                  {exportTypes.map(({ type: exportType, icon, label }) => (
                    <button
                      key={exportType}
                      type="button"
                      className="pointer tc mh3 gray hover-dark-gray bg-transparent bw0"
                      onClick={() => {
                        const instance = this.chartRef.current.getEchartsInstance();

                        this.setState(
                          { dataExport: { exportType, data: instance.getDataURL() } },
                          () => {
                            this.formExportRef.current.submit();
                          },
                        );
                      }}
                    >
                      <Icon type={icon} className="f1" /><br />
                      <span className="text-primary b"> {label} </span>
                    </button>
                  ))}
                </div>
              )}
            >
              <div>
                <form
                  ref={this.formExportRef}
                  method="post"
                  action={`${process.env.SDMX_ENDPOINT}/export/chart/${dataExport.type}`}
                >
                  <input type="hidden" name="data" value={dataExport.data} />
                </form>

                <button className="pointer pv2 ph3 br2 b bg-primary white br2 bw0">
                  <Icon type="export" style={{ fontSize: '14px' }} />
                  <span> Export</span>
                  <Icon type="down" className="white pa1" />
                </button>
              </div>
            </Dropdown>
          )}
        </div>
        <Echart
          notMerge
          ref={this.chartRef}
          option={this.getOption(type, xAxis, yAxis, obsVal, series)}
        />
      </div>
    );
  }
}

export default Chart;
