import React from 'react';
import PropTypes from 'prop-types';
import { PivotData } from 'react-pivottable/Utilities';

import PivotTableRenderer from './PivotTableRenderer';

// helper for setting row/col-span in pivotTableRendere=> r
const spanSize = (arr, i, j) => {
  let x;
  if (i !== 0) {
    let asc,
      end;
    let noDraw = true;
    for (
      x = 0, end = j, asc = end >= 0;
      asc ? x <= end : x >= end;
      asc ? x++ : x--
    ) {
      if (arr[i - 1][x] !== arr[i][x]) {
        noDraw = false;
      }
    }
    if (noDraw) {
      return -1;
    }
  }
  let len = 0;
  while (i + len < arr.length) {
    let asc1;
    let end1;
    let stop = false;
    for (
      x = 0, end1 = j, asc1 = end1 >= 0;
      asc1 ? x <= end1 : x >= end1;
      asc1 ? x++ : x--
    ) {
      if (arr[i][x] !== arr[i + len][x]) {
        stop = true;
      }
    }
    if (stop) {
      break;
    }
    len++;
  }
  return len;
};

const redColorScaleGenerator = (values) => {
  const min = Math.min(...values);
  const max = Math.max(...values);
  return (x) => {
    const opacity = Math.round((x - min) / (max - min));
    return { backgroundColor: `rgb(210, 168, 125, ,${opacity})` };

    // // eslint-disable-next-line no-magic-numbers
    // const nonRed = 255 - Math.round(255 * (x - min) / (max - min));
    // return { backgroundColor: `rgb(210,${nonRed},${nonRed})` };
  };
};

const makeRenderer = (opts = {}) => {
  PivotTableRenderer.opts = opts;
  PivotTableRenderer.spanSize = spanSize;
  PivotTableRenderer.redColorScaleGenerator = redColorScaleGenerator;

  return PivotTableRenderer;
};

export default {
  Table: makeRenderer(),
  'Table Heatmap': makeRenderer({ heatmapMode: 'full' }),
  'Table Col Heatmap': makeRenderer({ heatmapMode: 'col' }),
  'Table Row Heatmap': makeRenderer({ heatmapMode: 'row' }),
  // 'Exportable TSV': TSVExportRenderer,
};
