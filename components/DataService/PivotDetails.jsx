import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

class PivotDetail extends React.PureComponent {
  render() {
    const { data = [] } = this.props;
    let columns = [];

    if (data.length > 0) {
      data.map((item, i) => {
        item.key = i;

        if (i == 0) {
          columns = Object.keys(item).map(key => (key === 'key' ? {} : {
            title: key,
            dataIndex: key,
          }));
        }
      });
    }

    return data.length > 0
      ? <Table columns={columns} dataSource={data} />
      : <h1 className="tc silver">Empty</h1>;
  }
}

export default PivotDetail;
