import React from 'react';
import Router from 'next/router';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Button, Checkbox, Pagination, Input, Icon } from 'antd';
import _ from 'lodash';
import uuid from 'uuid/v1';

import { Storage } from '~/utils/api';
import Empty from '~/components/Empty';

export default class ListTable extends React.Component {
  render() {
    const { content = [], columns, onSearch, searchBar = true } = this.props;
    const columnLength = columns.length;

    return (
      <div className="cf overflow-auto">
        <table className="w-100 center" cellSpacing="0">
          <colgroup>
            <col width="60px" />
            <col width="120px" />
          </colgroup>
          <thead>
            <tr className="bg-primary">
              {columns.map(({ label = '' }, i) => (
                <th key={`column${i}`} className="pa2 tc white">{label}</th>
              ))}
              {searchBar && (
                <th className="pa1 tr" width="25%">
                  <Input.Search
                    placeholder="Search..."
                    className=" navbar-search bg-primary white"
                    onSearch={query => _.isFunction(onSearch) ? onSearch(query) : {}}
                  />
                </th>
              )}
            </tr>
          </thead>
          <tbody className="lh-copy">
            {content.map((row, i) => (
              <tr key={`row${i}`}>
                {columns.map(({ content, border = true }, j) => (
                  <td
                    key={`row${i}-col${j}`}
                    className="bb b--black-30"
                    colSpan={searchBar && j==columnLength-1 ? 2 : 1}
                  >
                    {content(row)}
                  </td>
                ))}
                {/* <td className="tc pa3 br mr2 b">{row.id}</td>
                <td>
                  <div className="ml3 pa3 bb b--black-30 db" style={{ lineHeight: '26px' }}>
                    <Link className="b black-80" href={`/data/sdmx/${source}/view/${row.id}`}>
                      {row.name}
                    </Link>
                  </div>
                </td> */}
                {/* <td className="tr">
                  <div className="pa3 bb b--black-30 db">
                    <Icon type="file-pdf" className="f3 ph2 dark-blue" />
                    <Icon type="file-text" className="f3 ph2 dark-blue" />
                    <Icon type="share-alt" className="dark-blue ph2" />
                  </div>
                </td> */}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
