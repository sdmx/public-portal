import React from 'react';
import PropTypes from 'prop-types';
import ImmutabilityHelper from 'immutability-helper';
import { PivotData, getSort, sortAs } from 'react-pivottable/Utilities';
import PivotTable from 'react-pivottable/PivotTable';
import ReactSortable from 'react-sortablejs';
import Draggable from 'react-draggable';
import _ from 'lodash';
import { Tooltip } from 'antd';

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value, enumerable: true, configurable: true, writable: true,
    });
  }
  else {
    obj[key] = value;
  }

  return obj;
}

/* eslint-disable react/prop-types */
// eslint can't see inherited propTypes!

class DraggableAttribute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      top: 0,
      left: 0,
      filterText: '',
    };
  }

  toggleValue(value) {
    if (value in this.props.valueFilter) {
      this.props.removeValuesFromFilter(this.props.name, [value]);
    }
    else {
      this.props.addValuesToFilter(this.props.name, [value]);
    }
  }

  matchesFilter(x) {
    return x.toLowerCase().trim().includes(this.state.filterText.toLowerCase().trim());
  }

  selectOnly(e, value) {
    const { name, attrValues } = this.props;

    e.stopPropagation();
    this.props.setValuesInFilter(name, Object.keys(attrValues).filter(y => y !== value));
  }

  getFilterBox() {
    const {
      name,
      attrValues = {},
      menuLimit,
      sorter,
      zIndex,
      moveFilterBoxToTop,
      removeValuesFromFilter,
      addValuesToFilter,
      valueFilter,
    } = this.props;

    const { top, left, filterText } = this.state;
    const showMenu = Object.keys(attrValues).length < menuLimit;

    const values = Object.keys(attrValues);
    const shown = values.filter(this.matchesFilter.bind(this)).sort(sorter);

    return (
      <Draggable handle=".pvtDragHandle">
        <div
          className="pvtFilterBox"
          style={{
            display: 'block',
            cursor: 'initial',
            top: `${top}px`,
            left: `${left}px`,
            zIndex,
          }}
          onClick={() => moveFilterBoxToTop(name)}
        >
          <a className="pvtCloseX" onClick={() => this.setState({ open: false })}>{'\xD7'}</a>
          <span className="pvtDragHandle">{'\u2630'}</span>
          <h4>{name}</h4>
          {showMenu || <p>(too many values to show)</p>}
          {showMenu && (
            <p>
              <input
                type="text"
                placeholder="Filter values"
                className="pvtSearch"
                value={filterText}
                onChange={(e) => this.setState({ filterText: e.target.value })}
              />
              <br />
              <a
                role="button"
                className="pvtButton"
                onClick={() => {
                  removeValuesFromFilter(
                    name,
                    Object.keys(attrValues).filter(this.matchesFilter.bind(this))
                  );
                }}
              >
                Select {values.length === shown.length ? 'All' : shown.length}
              </a>{' '}
              <a
                role="button"
                className="pvtButton"
                onClick={() => {
                  addValuesToFilter(
                    name,
                    Object.keys(attrValues).filter(this.matchesFilter.bind(this))
                  );
                }}
              >
                Deselect {values.length === shown.length ? 'All' : shown.length}
              </a>
            </p>
          )}
          {showMenu && (
            <div className="pvtCheckContainer">
              {shown.map(x => (
                <p
                  key={x}
                  className={x in valueFilter ? '' : 'selected'}
                  onClick={() => this.toggleValue(x)}
                >
                  <a className="pvtOnly" onClick={(e) => this.selectOnly(e, x)}>
                    only
                  </a>
                  <a className="pvtOnlySpacer">{'\xA0'}</a>
                  {x === '' ? <em>null</em> : x}
                </p>
              ))}
            </div>
          )}
        </div>
      </Draggable>
    );
  }

  toggleFilterBox(event) {
    const bodyRect = document.body.getBoundingClientRect();
    const rect = event.nativeEvent.target.getBoundingClientRect();

    this.setState({
      open: !this.state.open,
      top: 10 + rect.top - bodyRect.top,
      left: 10 + rect.left - bodyRect.left,
    });

    this.props.moveFilterBoxToTop(this.props.name);
  }

  render() {
    const { dimension, name, valueFilter } = this.props;
    const filtered = Object.keys(valueFilter).length !== 0 ? 'pvtFilteredAttribute' : '';

    return (
      <li data-id={name} style={{ float: 'left' }} title={`${dimension.id || name} : ${name}`}>
        <div className={`pvtAttr ${filtered} truncate`}>
          {name}
          <span className="pvtTriangle" onClick={this.toggleFilterBox.bind(this)}>
            {' \u25BE'}
          </span>
        </div>
        {this.state.open ? this.getFilterBox() : null}
      </li>
    );
  }
}

DraggableAttribute.defaultProps = {
  valueFilter: {},
};

DraggableAttribute.propTypes = {
  name: PropTypes.string.isRequired,
  addValuesToFilter: PropTypes.func.isRequired,
  removeValuesFromFilter: PropTypes.func.isRequired,
  attrValues: PropTypes.objectOf(PropTypes.number).isRequired,
  valueFilter: PropTypes.objectOf(PropTypes.bool),
  moveFilterBoxToTop: PropTypes.func.isRequired,
  sorter: PropTypes.func.isRequired,
  menuLimit: PropTypes.number,
  zIndex: PropTypes.number,
};

class Dropdown extends React.PureComponent {
  render() {
    const { zIndex, open, current, toggle, values, setValue } = this.props;

    return (
      <div className="pvtDropdown" style={{ zIndex }}>
        <div
          role="button"
          className={`pvtDropdownValue pvtDropdownCurrent ${open ? 'pvtDropdownCurrentOpen' : ''}`}
          onClick={(e) => {
            e.stopPropagation();
            toggle();
          }}
        >
          <div className="pvtDropdownIcon">
            {open ? '×' : '▾'}
          </div>
          {current || <span>{'\xA0'}</span>}
        </div>
        {open && (
          <div className="pvtDropdownMenu">
            {values.map((r) => (
              <div
                key={r}
                role="button"
                className={`pvtDropdownValue ${r === current ? 'pvtDropdownActiveValue' : ''}`}
                onClick={(e) => {
                  e.stopPropagation();

                  if (current === r) {
                    toggle();
                  }
                  else {
                    setValue(r);
                  }
                }}
              >
                {r}
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

class PivotTableUI extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      unusedOrder: [],
      zIndices: {},
      maxZIndex: 1000,
      openDropdown: false,
    };
  }

  componentWillMount() {
    this.materializeInput(this.props.data);
  }

  componentWillUpdate({ data }) {
    this.materializeInput(data);
  }

  materializeInput(nextData) {
    if (this.data === nextData) {
      return;
    }

    this.data = nextData;

    const attrValues = {};
    const materializedInput = [];
    let recordsProcessed = 0;

    const { derivedAttributes } = this.props;

    PivotData.forEachRecord(this.data, derivedAttributes, (record) => {
      materializedInput.push(record);

      let _iteratorNormalCompletion = true;
      let _didIteratorError = false;
      let _iteratorError;

      try {
        for (
          var _iterator = Object.keys(record)[Symbol.iterator](), _step;
          !(_iteratorNormalCompletion = (_step = _iterator.next()).done);
          _iteratorNormalCompletion = true
        ) {
          const attr = _step.value;

          if (!(attr in attrValues)) {
            attrValues[attr] = {};

            if (recordsProcessed > 0) {
              attrValues[attr].null = recordsProcessed;
            }
          }
        }
      }
      catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      }
      finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        }
        finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      for (const _attr in attrValues) {
        const value = _attr in record ? record[_attr] : 'null';

        if (!(value in attrValues[_attr])) {
          attrValues[_attr][value] = 0;
        }

        attrValues[_attr][value]++;
      }

      recordsProcessed++;
    });

    this.materializedInput = materializedInput;
    this.attrValues = attrValues;

    const dimensionsRank = _.sortBy(
      Object.keys(attrValues).map((k) => ({
        field: k,
        count: Object.keys(attrValues[k]).length,
      })),
      ['count']
    );

    if (dimensionsRank.length > 0) {
      const colDim = dimensionsRank.pop();
      this.propUpdater('cols')([colDim.field]);
    }

    if (dimensionsRank.length > 0) {
      const rowDim = dimensionsRank.pop();
      this.propUpdater('rows')([rowDim.field]);
    }
  }

  sendPropUpdate(command) {
    this.props.onChange(ImmutabilityHelper(this.props, command));
  }

  propUpdater(key) {
    return value => this.sendPropUpdate(_defineProperty({}, key, { $set: value }));
  }

  setValuesInFilter(attribute, values) {
    this.sendPropUpdate({
      valueFilter: _defineProperty({}, attribute, {
        $set: values.reduce((r, v) => {
          r[v] = true;
          return r;
        }, {}),
      }),
    });
  }

  addValuesToFilter(attribute, values) {
    if (attribute in this.props.valueFilter) {
      this.sendPropUpdate({
        valueFilter: _defineProperty({}, attribute, values.reduce((r, v) => {
          r[v] = { $set: true };
          return r;
        }, {})),
      });
    }
    else {
      this.setValuesInFilter(attribute, values);
    }
  }

  removeValuesFromFilter(attribute, values) {
    this.sendPropUpdate({
      valueFilter: _defineProperty({}, attribute, { $unset: values }),
    });
  }

  moveFilterBoxToTop(attribute) {
    this.setState(ImmutabilityHelper(this.state, {
      maxZIndex: { $set: this.state.maxZIndex + 1 },
      zIndices: _defineProperty({}, attribute, { $set: this.state.maxZIndex + 1 }),
    }));
  }

  isOpen(dropdown) {
    return this.state.openDropdown === dropdown;
  }

  makeDnDCell(items, onChange, classes) {
    const { dimensions = [], valueFilter, menuLimit, sorters } = this.props;

    return (
      <ReactSortable
        tag="td"
        className={classes}
        onChange={onChange}
        options={{
          group: 'shared',
          ghostClass: 'pvtPlaceholder',
          filter: '.pvtFilterBox',
          preventOnFilter: false,
        }}
        style={{ padding: '5px' }}
      >
        {items.map((x) => {
          let dimension = {};

          dimensions.forEach((dim) => {
            if (dim.concept.name === x) {
              dimension = dim;
            }
          });

          return (
            <DraggableAttribute
              key={x}
              name={x}
              dimension={dimension}
              attrValues={this.attrValues[x]}
              valueFilter={valueFilter[x] || {}}
              sorter={getSort(sorters, x)}
              menuLimit={menuLimit}
              setValuesInFilter={this.setValuesInFilter.bind(this)}
              addValuesToFilter={this.addValuesToFilter.bind(this)}
              moveFilterBoxToTop={this.moveFilterBoxToTop.bind(this)}
              removeValuesFromFilter={this.removeValuesFromFilter.bind(this)}
              zIndex={this.state.zIndices[x] || this.state.maxZIndex}
            />
          );
        })}
      </ReactSortable>
    );
  }

  render() {
    const { aggregators, aggregatorName, renderers } = this.props;

    const numValsAllowed = aggregators[aggregatorName]([])().numInputs || 0;
    const rendererName = this.props.rendererName in renderers
      ? this.props.rendererName : Object.keys(renderers)[0];
    const rendererCell = (<td className="pvtRenderers" />);

    const sortIcons = {
      key_a_to_z: {
        rowSymbol: '↕',
        colSymbol: '↔',
        next: 'value_a_to_z',
      },
      value_a_to_z: {
        rowSymbol: '↓',
        colSymbol: '→',
        next: 'value_z_to_a',
      },
      value_z_to_a: { rowSymbol: '↑', colSymbol: '←', next: 'key_a_to_z' },
    };

    const aggregatorCell = (
      <td className="pvtVals" rowSpan="2">
        <div className="f7 gray tl pl1">Calculation</div>
        <Dropdown
          current={aggregatorName}
          values={Object.keys(aggregators)}
          open={this.isOpen('aggregators')}
          zIndex={this.isOpen('aggregators') ? this.state.maxZIndex + 1 : 1}
          setValue={(value) => {
            this.setState({ openDropdown: false });
            this.propUpdater('aggregatorName')(value);
          }}
          toggle={() => this.setState({
            openDropdown: this.isOpen('aggregators') ? false : 'aggregators',
          })}
        >
          Calculation
        </Dropdown>
        <a
          className="pvtRowOrder"
          onClick={() => this.propUpdater('rowOrder')(sortIcons[this.props.rowOrder].next)}
        >
          {sortIcons[this.props.rowOrder].rowSymbol}
        </a>
        <a
          className="pvtColOrder"
          onClick={() => this.propUpdater('colOrder')(sortIcons[this.props.colOrder].next)}
        >
          {sortIcons[this.props.colOrder].colSymbol}
        </a>
        {numValsAllowed > 0 && <br />}
        {numValsAllowed > 0 && (
          <div className="f7 gray tl" style={{ marginLeft: '22px' }}>Dimensions</div>
        )}
        {new Array(numValsAllowed).fill().map((n, i) => [
          <Dropdown
            key={i}
            current={this.props.vals[i]}
            values={
              Object.keys(this.attrValues).filter(e =>
                !this.props.hiddenAttributes.includes(e)
                  && !this.props.hiddenFromAggregators.includes(e)
              )
            }
            open={this.isOpen(`val${i}`)}
            zIndex={this.isOpen(`val${i}`) ? this.state.maxZIndex + 1 : 1}
            toggle={() => this.setState({
              openDropdown: this.isOpen(`val${i}`) ? false : `val${i}`,
            })}
            setValue={(value) => {
              this.setState({ openDropdown: false });
              this.sendPropUpdate({ vals: { $splice: [[i, 1, value]] } });
            }}
          >
            {i + 1 !== numValsAllowed ? <br key={`br${i}`} /> : null}
          </Dropdown>
        ])}
      </td>
    );

    const unusedAttrs = Object.keys(this.attrValues)
      .filter((e) => !this.props.rows.includes(e)
        && !this.props.cols.includes(e)
        && !this.props.hiddenAttributes.includes(e)
        && !this.props.hiddenFromDragDrop.includes(e)
        && !_.includes(this.props.vals, e)
      )
      .sort(sortAs(this.state.unusedOrder));

    const unusedLength = unusedAttrs.reduce((r, e) => r + e.length, 0);
    const horizUnused = unusedLength < this.props.unusedOrientationCutoff;

    const sortedUnusedAttrs = [];

    this.props.dimensions.forEach(({ concept }) => {
      if (_.includes(unusedAttrs, concept.name)) {
        sortedUnusedAttrs.push(concept.name);
      }
    });

    const unusedAttrsCell = this.makeDnDCell(
      sortedUnusedAttrs,
      order => this.setState({ unusedOrder: order }),
      `pvtAxisContainer pvtUnused ${horizUnused ? 'pvtHorizList' : 'pvtVertList'} bg-primary-light`,
    );

    const colAttrs = this.props.cols.filter((e) => {
      return !this.props.hiddenAttributes.includes(e)
        && !this.props.hiddenFromDragDrop.includes(e);
    });

    const colAttrsCell = this.makeDnDCell(
      colAttrs,
      this.propUpdater('cols'),
      'pvtAxisContainer pvtHorizList pvtCols'
    );

    const rowAttrs = this.props.rows.filter((e) => {
      return !this.props.hiddenAttributes.includes(e)
        && !this.props.hiddenFromDragDrop.includes(e);
    });
    const rowAttrsCell = this.makeDnDCell(
      rowAttrs,
      this.propUpdater('rows'),
      'pvtAxisContainer pvtVertList pvtRows'
    );
    const outputCell = (
      <td className="pvtOutput">
        {React.createElement(PivotTable, ImmutabilityHelper(this.props, {
          data: { $set: this.materializedInput },
        }))}
      </td>
    );

    // if (horizUnused) {
    if (true) {
      return (
        <table className="pvtUi">
          <colgroup>
            <col width="200px" />
          </colgroup>
          <tbody onClick={() => this.setState({ openDropdown: false })}>
            <tr>
              {aggregatorCell}
              {unusedAttrsCell}
            </tr>
            <tr>
              {colAttrsCell}
            </tr>
            <tr>
              {rowAttrsCell}
              {outputCell}
            </tr>
          </tbody>
        </table>
      );
    }

    return (
      <table className="pvtUi">
        <colgroup>
          <col width="250px" />
          <col width="270px" />
        </colgroup>
        <tbody onClick={() => this.setState({ openDropdown: false })}>
          <tr>
            {rendererCell}
            {aggregatorCell}
            {colAttrsCell}
          </tr>
          <tr>
            {unusedAttrsCell}
            {rowAttrsCell}
            {outputCell}
          </tr>
        </tbody>
      </table>
    );
  }
}

PivotTableUI.propTypes = Object.assign({}, PivotTable.propTypes, {
  onChange: PropTypes.func.isRequired,
  hiddenAttributes: PropTypes.arrayOf(PropTypes.string),
  hiddenFromAggregators: PropTypes.arrayOf(PropTypes.string),
  hiddenFromDragDrop: PropTypes.arrayOf(PropTypes.string),
  unusedOrientationCutoff: PropTypes.number,
  menuLimit: PropTypes.number,
});

PivotTableUI.defaultProps = Object.assign({}, PivotTable.defaultProps, {
  hiddenAttributes: [],
  hiddenFromAggregators: [],
  hiddenFromDragDrop: [],
  unusedOrientationCutoff: 85,
  menuLimit: 500,
});

export default PivotTableUI;
