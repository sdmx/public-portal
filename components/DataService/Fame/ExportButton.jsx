import React from 'react';
import { Dropdown, Icon } from 'antd';

const ExportButton = ({
  dataId, type, filter, text, icon, child, source, params = {},
}) => (
  <form method="post" action={`${process.env.SDMX_ENDPOINT}/data/${source}/${dataId}/export`}>
    {Object.keys(params).map(name => (
      <input type="hidden" name={name} value={params[name]} />
    ))}
    <input type="hidden" name="t" value={type} />
    <input type="hidden" name="key" value={filter.key} />
    <input type="hidden" name="startPeriod" value={filter.startPeriod} />
    <input type="hidden" name="endPeriod" value={filter.endPeriod} />
    {child
      ? (
        <Dropdown overlay={child}>
          <div
            style={{ height: '50px' }}
            className="flex pointer fl w-50 pl3 gray pv2 hover-dark-gray"
          >
            {icon && icon}
            <div className="text-primary b pv2 ml1 fl">
              {text} <Icon type="down" className="pv2" />
            </div>
          </div>
        </Dropdown>
      ) : (
        <button
          type="submit"
          style={{ height: '50px' }}
          className="pointer fl w-50 pl3 gray pv2 hover-dark-gray bg-white bw0"
        >
          {icon && icon}
          <div className="text-primary b pv2 ml1 fl"> {text} </div>
        </button>
      )
    }
  </form>
);

export default ExportButton;
