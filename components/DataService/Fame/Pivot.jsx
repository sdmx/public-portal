import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import PivotTable from './PivotTableUI';
import TableRenderer from './TableRenderer';

class Pivot extends React.PureComponent {
  constructor(props) {
    super(props);
    const { defaultPrefer = {} } = props;

    this.state = {
      rows: [defaultPrefer.yAxis],
      cols: [defaultPrefer.xAxis],
      vals: [defaultPrefer.obsVal],
      aggregatorName: 'Sum',
      ...props,
    };
  }

  render() {
    const { series, dimensions } = this.props;
    const data = series.map((row) => {
      const res = { ...row };

      Object.keys(row).forEach((k) => {
        dimensions.forEach((dim) => {
          if (dim.id === k) {
            const { codes } = dim.codeList;
            delete res[k];
            res[dim.concept.name] = _.has(codes, row[k]) ? `${codes[row[k]]} (${row[k]})` : row[k];
          }
        });
      });

      return res;
    });

    // console.log(dimensions, data);
    // console.log(this.props.defaultPrefer, data, series);

    return (
      <div>
        <div className="table-responsive w-100">
          <PivotTable
            onChange={s => this.setState(s)}
            renderers={TableRenderer}
            data={data}
            dimensions={dimensions}
            {...this.state}
          />
        </div>
      </div>
    );
  }
}

export default connect(props => props.dataset.data)(Pivot);
