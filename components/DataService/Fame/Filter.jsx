import React from 'react';
import { connect } from 'react-redux';
import { Button, Form, Select, DatePicker, Input } from 'antd';
import moment from 'moment';
import _ from 'lodash';

class Filter extends React.PureComponent {
  constructor(props) {
    super(props);
    const { dimensions, keySeries = '', startPeriod, endPeriod } = props;
    const filter = {};

    if (_.isArray(dimensions) && !!keySeries) {
      const keySeriesValues = keySeries.split('.');

      dimensions.forEach((dimension, i) => {
        if (dimension.id === 'TIME_PERIOD') {
          filter[dimension.id] = [moment(startPeriod), moment(endPeriod)];
        }
        else if (keySeriesValues[i]) {
          filter[dimension.id] = _.filter(keySeriesValues[i].split('+'));
        }
      });
    }

    this.state = { filter };
  }

  getInputFilter(dimension) {
    const { startPeriod, endPeriod } = this.props;
    const { id, codeList } = dimension;

    const handleChange = (value) => {
      const filter = { ...this.state.filter };
      filter[id] = value;

      this.setState({ filter });
    };

    if (id === 'FREQ') {
      return (
        <Form.Item className="fl w-60">
          <Select
            showSearch
            placeholder="Select..."
            optionFilterProp="children"
            onChange={handleChange}
            defaultValue={this.state.filter[id]}
            filterOption={(input, option) => (
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            )}
          >
            {Object.keys(codeList.codes).map(keyCode => (
              <Select.Option key={keyCode} value={keyCode}>
                {codeList.codes[keyCode]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      );
    }
    else if (id === 'TIME_PERIOD') {
      return (
        <Form.Item className="fl w-60">
          <DatePicker.RangePicker
            defaultValue={this.state.filter[id]}
            onChange={handleChange}
          />
        </Form.Item>
      );
    }
    else if (codeList !== null) {
      return (
        <Form.Item className="fl w-60">
          <Select
            showSearch
            allowClear
            placeholder="All"
            optionFilterProp="children"
            onChange={handleChange}
            mode="multiple"
            defaultValue={this.state.filter[id]}
            filterOption={(input, option) => (
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            )}
          >
            {Object.keys(codeList.codes).map(keyCode => (
              <Select.Option key={keyCode} value={keyCode}>
                {codeList.codes[keyCode]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      );
    }

    return (
      <Form.Item className="fl w-60">
        <Input
          name={id}
          defaultValue={this.state.filter[id]}
          onChange={handleChange}
        />
      </Form.Item>
    );
  }

  render() {
    const { dimensions = [] } = this.props;

    return (
      <div>
        <form onSubmit={this.props.handleSubmit} data={this.props.data}>
          {dimensions.map((dimension, i) => {
            const { id, concept } = dimension;

            return (
              <div key={id} className="cf">
                <label className="fl w-30 tr pr4 pv2" title={concept.description}>
                  {concept.name}
                </label>
                {this.getInputFilter(dimension)}
              </div>
            );
          })}

          <div className="cf tr w-90 mt2">
            <Button
              type="primary"
              onClick={() => {
                const keySeries = [];
                let startPeriod;
                let endPeriod;

                dimensions.forEach((dimension, i) => {
                  const { id } = dimension;
                  const value = this.state.filter[id];

                  if (id === 'TIME_PERIOD' && value && value.length === 2) {
                    startPeriod = value[0].format('YYYY-MM-DD');
                    endPeriod = value[1].format('YYYY-MM-DD');
                    keySeries.push('');
                  }
                  else if (_.isArray(value)) {
                    keySeries.push(value.join('+'));
                  }
                  else {
                    keySeries.push(value);
                  }
                });

                this.props.onSubmit({
                  key: keySeries.join('.'),
                  startPeriod,
                  endPeriod,
                });
              }}
            >
              Filter
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(props => props.dataset.data)(Filter);
