import React from 'react';
import Router from 'next/router';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Button, Checkbox, Pagination, Input, Icon, Modal } from 'antd';
import URI from 'urijs';
import _ from 'lodash';
import moment from 'moment';
import uuid from 'uuid/v1';

import { Storage, DataService } from '~/utils/api';
import Link from '~/components/Link';
import Loading from '~/components/Loading';
import Empty from '~/components/Empty';
import Filter from './FameFilter';

export default class DataflowList extends React.Component {
  static propTypes = {
    pageSize: PropTypes.number,
  };

  static defaultProps = {
    pageSize: 10,
  };

  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      filter: {
        query: null,
        startPeriod: null,
        endPeriod: null,
        values: {},
      },
      selected: [],
      dimensions: [],
      showFilterModal: false,
      data: [],
      totalPages: 1,
      count: 0,
      page: 1,
      loading: true,
    };

    this.fetchList = this.fetchList.bind(this);
  }

  componentDidMount() {
    const { query = '', category } = this.props;
    this.fetchList(1, query, { category });
  }

  componentWillReceiveProps({ category }) {
    if (this.props.category !== category) {
      this.fetchList(1, '', { category });
    }
  }

  fetchList(page = 1, query = '', params = {}) {
    const { pageSize, source, dataId } = this.props;
    const queryParams = _.omitBy(params, _.isNull);

    this.setState({ loading: true });

    // get dsd
    if (_.has(params, 'category')) {
      DataService.getDimensions(dataId, params.category)
        .then(({ data }) => {
          this.setState({ dimensions: data });
        })
        .catch(() => {
          this.setState({ dimensions: [] });
        });
    }

    if (_.isArray(query)) {
      // DataService.filter(source, query, params.startPeriod, params.endPeriod)
      DataService.filter(source, query.join('|'), page)
        .then(({ data }) => {
          const { content = [], totalPages, totalElements } = data;

          this.setState({
            page,
            data: content,
            totalPages,
            count: totalElements,
            loading: false,
          });
        })
        .catch(() => this.setState({ loading: false }));
    }
    else {
      // data list
      axios.get(`${process.env.SDMX_ENDPOINT}/data/${source}`, {
        params: {
          q: query,
          size: pageSize,
          page,
          ...queryParams,
        },
      })
        .then((res) => {
          const { content = [], totalPages, totalElements } = res.data;

          this.setState({
            page,
            data: content,
            totalPages,
            count: totalElements,
            loading: false,
          });
        })
        .catch(() => this.setState({ loading: false }));
    }
  }

  buildDataset(source, items) {
    this.setState({ loading: true });

    const { category } = this.props;
    const { dimensions } = this.state;

    const expireTime = new Date().getTime() + (1000 * 3600 * 24 * 3); // 3 days
    const buildId = uuid();

    const keySeriesArr = category.split('.');
    const code = keySeriesArr[keySeriesArr.length - 1];
    const meta = {
      title: keySeriesArr.length > 1 ? dimensions[keySeriesArr.length - 2].codeList.codes[code] : 'Data',
      id: category,
    };

    Storage.set(`${buildId}-meta`, meta, expireTime);
    Storage.set(buildId, items.map(i => i.id).join('|'), expireTime);

    const { startPeriod, endPeriod } = this.state.filter;
    const uri = URI(`/data/internal/build/${buildId}`)
      .query(_.omitBy({ startPeriod, endPeriod }, _.isNull));

    Router.push(uri.toString());
  }

  render() {
    const {
      dimensions = [],
      data = [],
      totalPages,
      page,
      count,
      searchQuery,
      loading,
      selected = [],
      filter,
    } = this.state;
    const { pageSize, source, category } = this.props;

    return (
      <Loading isFinish={!loading}>
        <Empty isEmpty={data.length === 0}>
          {selected.length > 0 && (
            <div className="cf tr f7 pv1">
              <i className="pr2">{selected.length} items are selected.</i>
              <Button type="primary" onClick={() => this.buildDataset(source, selected)}>Go</Button>
            </div>
          )}

          <div className="cf overflow-auto">
            <table className="w-100 center" cellSpacing="0">
              <colgroup>
                <col width="120px" />
              </colgroup>
              <thead>
                <tr className="bg-primary">
                  <th className="pa2 tc white">
                    {/* <Checkbox onChange={() => {}} /> */}
                  </th>
                  <th className="pa2 tc white">Code</th>
                  <th className="pa2 tc white">Name</th>
                  <th className="pa2 tr">
                    <Input.Search
                      placeholder="Search..."
                      className="navbar-search bg-primary white fr"
                      style={{ width: '200px' }}
                      value={searchQuery}
                      onChange={e => this.setState({ searchQuery: e.target.value })}
                      onSearch={query => this.fetchList(1, query)}
                    />

                    {dimensions.length > 0 && (
                      <Button
                        type="primary"
                        size="small"
                        className="ph2 mr2 fr"
                        onClick={() => this.setState({ showFilterModal: true })}
                      >
                        <Icon type="filter" /> Filter
                      </Button>
                    )}

                    <Modal
                      title={<div><Icon type="filter" /> Filter</div>}
                      visible={this.state.showFilterModal}
                      onCancel={() => this.setState({ showFilterModal: false })}
                      footer={null}
                    >
                      <Filter
                        dimensions={dimensions}
                        category={category}
                        defaultFilter={filter.values}
                        onSubmit={(values) => {
                          const vals = dimensions.map((dim) => {
                            if (dim.timeDimension) {
                              return ['*'];
                            }
                            else if (_.has(values, dim.id)) {
                              return _.isArray(values[dim.id])
                                ? values[dim.id] : [values[dim.id]];
                            }

                            return ['*'];
                          });

                          const r = [];
                          const max = vals.length - 1;
                          const helper = (arr, i) => {
                            for (let j = 0, l = vals[i].length; j < l; j += 1) {
                              const a = arr.slice(0); // clone arr
                              a.push(vals[i][j]);

                              if (i === max) {
                                r.push(a);
                              }
                              else {
                                helper(a, i + 1);
                              }
                            }
                          };

                          helper([], 0);

                          const seriesCode = category.split('.');
                          const query = r.map(row =>
                            `${seriesCode[0]}.${row.join('.').replace(/(\.\*)+/, '*')}`);

                          this.fetchList(1, query);

                          // time filter
                          const timePeriod = _.get(values, 'TIME_PERIOD', []);

                          this.setState({
                            showFilterModal: false,
                            filter: {
                              values,
                              query,
                              startPeriod: timePeriod[0]
                                ? moment(timePeriod[0]).format('DD/MM/YYYY') : null,
                              endPeriod: timePeriod[1]
                                ? moment(timePeriod[1]).format('DD/MM/YYYY') : null,
                            },
                          });
                        }}
                      />
                    </Modal>
                  </th>
                </tr>
              </thead>
              <tbody className="lh-copy">
                {data.map(item => (
                  <tr key={item.id}>
                    <td className="tc bb b--black-30">
                      <Checkbox
                        checked={_.find(selected, i => i.id === item.id) !== undefined}
                        onChange={({ target }) => {
                          let newSelected;

                          if (target.checked) {
                            selected.push(item);
                            newSelected = selected;
                          }
                          else {
                            newSelected = _.filter(selected, selItem => selItem.id !== item.id);
                          }

                          this.setState({ selected: [...newSelected] });
                        }}
                      />
                    </td>
                    <td className="tc pa3 b bb b--black-30">{item.id}</td>
                    <td colSpan="2" className="pa3 bb b--black-30">
                      <Link className="b black-80" href={`/data/internal/view/${item.id}`}>
                        {item.name}
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>

          <br />
          <div className="tc">
            {totalPages > 1 && (
              <Pagination
                total={count}
                current={page}
                pageSize={pageSize}
                onChange={pageNum => this.fetchList(pageNum, searchQuery)}
              />
            )}
          </div>
        </Empty>
      </Loading>
    );
  }
}
