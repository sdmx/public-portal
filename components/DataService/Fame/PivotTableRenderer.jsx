import React from 'react';
import { PivotData } from 'react-pivottable/Utilities';
import _ from 'lodash';
import { Modal, Icon, Dropdown, Menu } from 'antd';

import PivotDetails from './PivotDetails';
import KeySeries from './KeySeries';

class PivotTableRenderer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      detail: {
        visible: false,
        data: [],
      },
      keySeries: {
        visible: false,
        keySeries: '',
      },
    };

    this.showDetails = this.showDetails.bind(this);
    this.closeDetails = this.closeDetails.bind(this);

    this.showKeySeries = this.showKeySeries.bind(this);
    this.closeKeySeries = this.closeKeySeries.bind(this);
  }

  getDimensionFilter(pivotData, rowKey, colKey) {
    const filter = {};

    pivotData.props.cols.map((attr, i) => { filter[attr] = colKey[i]; });
    pivotData.props.rows.map((attr, i) => { filter[attr] = rowKey[i]; });

    return filter;
  }

  showKeySeries(keySeries) {
    this.setState({ keySeries: { visible: true, keySeries } });
  }

  closeKeySeries() {
    this.setState({ keySeries: { visible: false } });
  }

  closeDetails() {
    this.setState({ detail: { visible: false } });
  }

  showDetails(data) {
    this.setState({ detail: { visible: true, data } });
  }

  render() {
    const { opts, spanSize, redColorScaleGenerator } = PivotTableRenderer;
    const pivotData = new PivotData(this.props);
    const colAttrs = pivotData.props.cols;
    const rowAttrs = pivotData.props.rows;
    const rowKeys = pivotData.getRowKeys();
    const colKeys = pivotData.getColKeys();
    const grandTotalAggregator = pivotData.getAggregator([], []);

    let valueCellColors = () => {};
    let rowTotalColors = () => {};
    let colTotalColors = () => {};

    if (opts.heatmapMode) {
      // const colorScaleGenerator = this.props.tableColorScaleGenerator;
      const colorScaleGenerator = redColorScaleGenerator;
      const rowTotalValues = colKeys.map(x =>
        pivotData.getAggregator([], x).value());
      rowTotalColors = colorScaleGenerator(rowTotalValues);
      const colTotalValues = rowKeys.map(x =>
        pivotData.getAggregator(x, []).value());
      colTotalColors = colorScaleGenerator(colTotalValues);

      if (opts.heatmapMode === 'full') {
        const allValues = [];
        rowKeys.map(r =>
          colKeys.map(c =>
            allValues.push(pivotData.getAggregator(r, c).value())));
        const colorScale = colorScaleGenerator(allValues);
        valueCellColors = (r, c, v) => colorScale(v);
      }
      else if (opts.heatmapMode === 'row') {
        const rowColorScales = {};
        rowKeys.map((r) => {
          const rowValues = colKeys.map(x =>
            pivotData.getAggregator(r, x).value());
          rowColorScales[r] = colorScaleGenerator(rowValues);
        });
        valueCellColors = (r, c, v) => rowColorScales[r](v);
      }
      else if (opts.heatmapMode === 'col') {
        const colColorScales = {};
        colKeys.map((c) => {
          const colValues = rowKeys.map(x =>
            pivotData.getAggregator(x, c).value());
          colColorScales[c] = colorScaleGenerator(colValues);
        });
        valueCellColors = (r, c, v) => colColorScales[c](v);
      }
    }

    const { detail, keySeries } = this.state;
    const { dimensions, filter, router } = this.props;

    return (
      <div className="pvtWrapper">
        <table className="pvtTable">
          <thead>
            {colAttrs.map((c, j) => (
              <tr key={`colAttr${j}`}>
                {j === 0 &&
                    rowAttrs.length !== 0 && (
                      <th colSpan={rowAttrs.length} rowSpan={colAttrs.length} />
                    )}
                <th className="pvtAxisLabel">{c}</th>
                {colKeys.map((colKey, i) => {
                    const x = spanSize(colKeys, i, j);
                    if (x === -1) {
                      return null;
                    }
                    return (
                      <th
                        className="pvtColLabel"
                        key={`colKey${i}`}
                        colSpan={x}
                        rowSpan={j === colAttrs.length - 1 && rowAttrs.length !== 0 ? 2 : 1}
                      >
                        {colKey[j]}
                      </th>
                    );
                  })}

                {j === 0 && (
                <th
                  className="pvtTotalLabel"
                  rowSpan={colAttrs.length + (rowAttrs.length === 0 ? 0 : 1)}
                >
                      Totals
                </th>
                  )}
              </tr>
              ))}

            {rowAttrs.length !== 0 && (
              <tr>
                {rowAttrs.map((r, i) => (
                  <th className="pvtAxisLabel" key={`rowAttr${i}`}>
                    {r}
                  </th>
                  ))}
                <th className="pvtTotalLabel">
                  {colAttrs.length === 0 ? 'Totals' : null}
                </th>
              </tr>
            )}
          </thead>

          <tbody>
            {rowKeys.map((rowKey, i) => {
              const totalAggregator = pivotData.getAggregator(rowKey, []);

              return (
                <tr key={`rowKeyRow${i}`}>
                  {rowKey.map((txt, j) => {
                    const x = spanSize(rowKeys, i, j);
                    if (x === -1) {
                      return null;
                    }
                    return (
                      <th
                        key={`rowKeyLabel${i}-${j}`}
                        className="pvtRowLabel"
                        rowSpan={x}
                        colSpan={j === rowAttrs.length - 1 && colAttrs.length !== 0 ? 2 : 1}
                      >
                        {txt}
                      </th>
                    );
                  })}
                  {colKeys.map((colKey, j) => {
                    const aggregator = pivotData.getAggregator(rowKey, colKey);
                    const value = aggregator.value();

                    return (
                      <td
                        className="pvtVal"
                        key={`pvtVal${i}-${j}`}
                        style={valueCellColors(rowKey, colKey, value)}
                      >
                        {value === null ? '' : aggregator.format(value)}

                        <Dropdown
                          trigger={['click']}
                          overlay={(
                            <Menu>
                              {/* Key Series */}
                              {/*<Menu.Item
                                  key="0"
                                  onClick={() => {
                                    const dimensionFilter = this.getDimensionFilter(pivotData, rowKey, colKey);
                                    const keySeriesArr = [];
                                    const keySeriesFilter = filter.key.split('.');

                                    dimensions.map((dimension, i) => {
                                      keySeriesArr.push(_.has(dimensionFilter, dimension.id)
                                          ? dimensionFilter[dimension.id]
                                          : keySeriesFilter[i]);
                                    });

                                    this.showKeySeries(keySeriesArr.join('.'));
                                  }}
                                >
                                  <Icon type="line-chart" /> Key Series
                                </Menu.Item>*/}
                              <Menu.Item
                                key="1"
                                onClick={() => {
                                  const dimensionFilter = this.getDimensionFilter(pivotData, rowKey, colKey);
                                  const data = _.filter(pivotData.props.data, dimensionFilter);

                                  this.showDetails(data);
                                }}
                              >
                                <Icon type="bars" /> Detail
                              </Menu.Item>
                            </Menu>
                          )}
                        >
                          <button className="pointer fr val-opt bw0 bg-white pt1 pl1 pr0">
                            <Icon type="ellipsis" className="rotate-90" />
                          </button>
                        </Dropdown>
                      </td>
                    );
                  })}
                  <td
                    className="pvtTotal"
                    style={colTotalColors(totalAggregator.value())}
                  >
                    {totalAggregator.format(totalAggregator.value())}
                  </td>
                </tr>
              );
            })}

            <tr>
              <th
                className="pvtTotalLabel"
                colSpan={rowAttrs.length + (colAttrs.length === 0 ? 0 : 1)}
              >
                Totals
              </th>

              {colKeys.map((colKey, i) => {
                const totalAggregator = pivotData.getAggregator([], colKey);
                return (
                  <td
                    className="pvtTotal"
                    key={`total${i}`}
                    style={rowTotalColors(totalAggregator.value())}
                  >
                    {totalAggregator.format(totalAggregator.value())}
                  </td>
                );
              })}

              <td className="pvtGrandTotal">
                {grandTotalAggregator.format(grandTotalAggregator.value())}
              </td>
            </tr>
          </tbody>
        </table>

        {/* Cell Detail Modal */}
        <Modal
          title="Details"
          footer={null}
          visible={detail.visible}
          onCancel={this.closeDetails}
          width={760}
        >
          <PivotDetails {...detail} />
        </Modal>

        {/* Key Series Modal */}
        <Modal
          title={null}
          footer={null}
          visible={keySeries.visible}
          onCancel={this.closeKeySeries}
          bodyStyle={{ padding: 0 }}
          style={{ top: '50px' }}
          width={960}
        >
          <KeySeries
            dataId={router.query.id}
            dimensions={dimensions}
            source={router.query.source}
            {...keySeries}
          />
        </Modal>
      </div>
    );
  }
}

PivotTableRenderer.defaultProps = PivotData.defaultProps;
PivotTableRenderer.propTypes = PivotData.propTypes;

export default PivotTableRenderer;
