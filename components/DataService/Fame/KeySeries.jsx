import React from 'react';
import _ from 'lodash';
import { Icon, Dropdown, Menu, Select } from 'antd';
import axios from 'axios';

import Chart from './Chart';

class KeySeries extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      keySeries: props.keySeries,
      loading: true,
      type: 'line',
      data: [],
      filter: {
        visible: false,
        dimensionId: undefined,
        codes: {},
        keySeries: '',
        data: [],
        values: [],
      },
    };

    this.setKeySeriesChart = this.setKeySeriesChart.bind(this);
  }

  componentDidMount() {
    this.fetchData(this.props)
      .then((res) => {
        this.setState({ data: res.data.series });
      });
  }

  setKeySeriesChart(type) {
    this.setState({ type });
  }

  fetchData({ source, keySeries, dataId }) {
    this.setState({ loading: true });

    return axios
      .get(`${process.env.SDMX_ENDPOINT}/data/${source}/${dataId}`, { params: { key: keySeries } })
      .then((res) => {
        this.setState({ loading: false });
        return res;
      });
  }

  filter(values = []) {
    const { dimensions = [], keySeries } = this.props;
    const filteredKeySeries = [];

    keySeries.split('.').forEach((code, i) => {
      if (dimensions[i].id === this.state.filter.dimensionId) {
        filteredKeySeries.push(values.join('+'));
      }
      else {
        filteredKeySeries.push(code);
      }
    });

    this.fetchData({ ...this.props, keySeries: filteredKeySeries.join('.') }).then((res) => {
      this.setState({
        filter: {
          ...(this.state.filter),
          keySeries: filteredKeySeries.join('.'),
          data: res.data.series,
          values,
        },
      });
    });
  }

  filterAfterState() {
    setTimeout(() => this.filter(this.state.filter.values), 500);
  }

  render() {
    const { dimensions = [] } = this.props;
    const {
      data = [], loading, keySeries, filter, type,
    } = this.state;
    const keySeriesArr = keySeries.split('.');

    return (
      <div className="flex">
        <div className="w-40">
          <table className="w-100">
            <thead className="bg-primary white">
              <tr style={{ height: '50px' }}>
                <th className="b pa3 tc w-10"> Dimension </th>
                <th className="b pa3 tc w-5"> Code </th>
                <th className="b pa3 tc w-15"> Description </th>
              </tr>
            </thead>
            <tbody>
              {filter.visible && (
                <tr style={{ height: '46px' }}>
                  <td colSpan="3" className="pv2 pl3 pr0 br bb b--moon-gray">
                    <Select
                      mode="multiple"
                      className="w-90"
                      showSearch
                      placeholder={`${filter.dimensionId} : All`}
                      value={filter.values}
                      onChange={values => this.setState({ filter: { ...filter, values } })}
                      onBlur={() => this.filterAfterState()}
                      onDeselect={() => this.filterAfterState()}
                    >
                      {_.isObject(filter.codes) && Object.keys(filter.codes).map(code => (
                        <Select.Option key={code}>
                          {filter.codes[code]}
                        </Select.Option>
                      ))}
                    </Select>

                    <Icon
                      type="close"
                      className="w-10 pa2 pointer"
                      onClick={() => {
                        this.setState({
                          filter: {
                            ...filter,
                            visible: false,
                            dimensionId: undefined,
                          },
                        });
                      }}
                    />
                  </td>
                </tr>
              )}
              {dimensions.map((dimension, i) => (!dimension.timeDimension && (
                <tr
                  key={dimension.id}
                  className={filter.dimensionId === dimension.id ? 'bg-light-gray' : ''}
                >
                  <td
                    className="ph3 pv1 bb br b--moon-gray b pointer primary hover-underline tr"
                    onClick={() => this.setState({
                      filter: {
                        ...filter,
                        visible: true,
                        dimensionId: dimension.id,
                        codes: dimension.codeList.codes,
                      },
                    }, () => this.filter())}
                  >
                    {dimension.concept.name}
                  </td>
                  <td className="ph3 pv1 bb br b--moon-gray text-primary tc b">
                    {keySeriesArr[i] === '' ? '*' : keySeriesArr[i]}
                  </td>
                  <td className="ph3 pv1 bb b--moon-gray">
                    {dimension.concept.description}
                  </td>
                </tr>
              )))}
            </tbody>
          </table>
        </div>
        <div className="w-60">
          <div className="flex justify-end bg-primary" style={{ height: '50px' }}>
            <div className="pt3 mt1 mr5 white b">
              <Dropdown
                overlay={(
                  <Menu style={{ width: '200px', right: 0 }}>
                    <Menu.Item key="0" onClick={() => this.setKeySeriesChart('line')}>
                      <Icon type="line-chart" /> Line
                    </Menu.Item>
                    <Menu.Item key="1" onClick={() => this.setKeySeriesChart('bar')}>
                      <Icon type="bar-chart" /> Bar
                    </Menu.Item>
                    <Menu.Item key="2" onClick={() => this.setKeySeriesChart('scatter')}>
                      <Icon type="dot-chart" /> Scatter
                    </Menu.Item>
                    <Menu.Item key="3" onClick={() => this.setKeySeriesChart('area')}>
                      <Icon type="area-chart" /> Area
                    </Menu.Item>
                  </Menu>
                )}
              >
                <div className="pointer">
                  Chart Type <Icon type="caret-down" className="ml2" />
                </div>
              </Dropdown>
            </div>
          </div>

          <div className="bl b--moon-gray relative">
            {loading && (
              <div className="bg-white-90 absolute w-100 h-100 z-999 flex flex-column justify-center tc">
                <div className="f2 silver center">
                  <Icon type="loading" className="f2" /> Loading
                </div>
              </div>
            )}

            <Chart
              key={`${filter.dimensionId}-${type}`}
              type={type}
              yAxis={filter.dimensionId}
              series={filter.visible ? filter.data : data}
              enableSwitch={false}
            />
            <div className="tc b pv2">
              {filter.visible ? filter.keySeries : keySeries}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default KeySeries;
