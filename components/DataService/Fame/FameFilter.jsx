import React from 'react';
import { Button, Form, Select, DatePicker, Input } from 'antd';
import moment from 'moment';
import _ from 'lodash';

class Filter extends React.PureComponent {
  constructor(props) {
    super(props);
    const { dimensions, keySeries = '', startPeriod, endPeriod, category, defaultFilter } = props;
    // const filter = {};
    const filter = this.getDefaultFilterByCategory(category);

    if (_.isArray(dimensions) && !!keySeries) {
      const keySeriesValues = keySeries.split('.');

      dimensions.forEach((dimension, i) => {
        if (dimension.id === 'TIME_PERIOD') {
          filter[dimension.id] = [moment(startPeriod), moment(endPeriod)];
        }
        else {
          filter[dimension.id] = _.filter(keySeriesValues[i].split('+'));
        }
      });
    }

    this.state = { filter: { ...defaultFilter, ...filter } };
    // this.setState({ filter: this.getDefaultFilterByCategory(category) });
    // this.getDefaultFilterByCategory(this.props.category);
  }

  componentWillReceiveProps({ category }) {
    if (this.props.category !== category) {
      this.setState({ filter: this.getDefaultFilterByCategory(category) });
    }
  }

  getDefaultFilterByCategory = (category) => {
    const defaultDimValues = category.split('.').slice(1);
    const filter = {};

    this.props.dimensions.forEach((dim, i) => {
      if (defaultDimValues[i] !== undefined) {
        filter[dim.id] = [defaultDimValues[i]];
      }
    });

    return filter;
  }

  getInputFilter(dimension, enabled = true) {
    const { startPeriod, endPeriod } = this.props;
    const { id, codeList } = dimension;

    const handleChange = (value) => {
      const filter = { ...this.state.filter };
      filter[id] = value;

      this.setState({ filter });
    };

    if (id === 'FREQ') {
      return (
        <Form.Item className="fl w-60">
          <Select
            showSearch
            placeholder="Select..."
            optionFilterProp="children"
            onChange={handleChange}
            defaultValue={this.state.filter[id]}
            filterOption={(input, option) => (
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            )}
          >
            {Object.keys(codeList.codes).map(keyCode => (
              <Select.Option key={keyCode} value={keyCode}>
                {codeList.codes[keyCode]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      );
    }
    else if (id === 'TIME_PERIOD') {
      return (
        <Form.Item className="fl w-60">
          <DatePicker.RangePicker
            defaultValue={this.state.filter[id]}
            onChange={handleChange}
          />
        </Form.Item>
      );
    }
    else if (codeList !== null) {
      return (
        <Form.Item className="fl w-60">
          <Select
            showSearch
            allowClear
            placeholder="All"
            optionFilterProp="children"
            disabled={!enabled}
            onChange={handleChange}
            mode="multiple"
            defaultValue={this.state.filter[id]}
            filterOption={(input, option) => (
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            )}
          >
            {Object.keys(codeList.codes).map(keyCode => (
              <Select.Option key={keyCode} value={keyCode}>
                {codeList.codes[keyCode]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      );
    }

    return (
      <Form.Item className="fl w-60">
        <Input
          name={id}
          disabled={!enabled}
          defaultValue={this.state.filter[id]}
          onChange={handleChange}
        />
      </Form.Item>
    );
  }

  render() {
    const { dimensions = [], category } = this.props;
    const defaultDimValues = category.split('.').slice(1);

    return (
      <div>
        <form onSubmit={this.props.handleSubmit} data={this.props.data}>
          {dimensions.map((dimension, i) => {
            const { id, concept } = dimension;

            return (
              <div key={id} className="cf">
                <label className="fl w-30 tr pr4 pv2" title={concept.description}>
                  {concept.name}
                </label>
                {this.getInputFilter(dimension, defaultDimValues[i] === undefined)}
              </div>
            );
          })}

          <div className="cf tr w-90 mt2">
            <Button type="primary" onClick={() => this.props.onSubmit({ ...this.state.filter })}>
              Filter
            </Button>
          </div>
        </form>
      </div>
    );
  }
}

export default Filter;
