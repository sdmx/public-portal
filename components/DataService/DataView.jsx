import React from 'react';
import _ from 'lodash';
import { Icon, Menu, Dropdown } from 'antd';
import moment from 'moment';

import Empty from '~/components/Empty';
import Pivot from '~/components/DataService/Pivot';
import Export from '~/components/DataService/Export';
import DataMap from '~/components/DataService/DataMap';
import Tabular from '~/components/DataService/Visual/Tabular';
import Links from '~/components/DataService/Visual/Links';

const VISUAL = {
  PIVOT: 'PIVOT',
  TABLE: 'TABLE',
  MAP: 'MAP',
  CHART: 'CHART',
  LINKS: 'LINKS',
};

// echart
/*const CHART_TYPES = [
  { type: 'CHART/line', label: 'Line', icon: 'line-chart' },
  { type: 'CHART/bar', label: 'Bar', icon: 'bar-chart' },
  { type: 'CHART/scatter', label: 'Scatter', icon: 'dot-chart' },
  { type: 'CHART/area', label: 'Area', icon: 'area-chart' },
  { type: 'CHART/pie', label: 'Pie', icon: 'pie-chart' },
  { type: 'CHART/doughnut', label: 'Doughnut', icon: 'loading-3-quarter' },
  { type: 'CHART/parallel', label: 'Parallel', icon: 'sliders' },
];*/

// plotly
const CHART_TYPES = [
  { type: 'CHART/Line Chart', label: 'Line', icon: 'line-chart' },
  { type: 'CHART/Area Chart', label: 'Area', icon: 'area-chart' },
  // { type: 'CHART/Pie Chart', label: 'Pie', icon: 'pie-chart' },
  { type: 'CHART/Grouped Column Chart', label: 'Bar', icon: 'bar-chart' },
  { type: 'CHART/Stacked Column Chart', label: 'Stacked', icon: 'bar-chart' },
  { type: 'CHART/Scatter Chart', label: 'Scatter', icon: 'dot-chart' },
  { type: 'CHART/Dot Chart', label: 'Dot', icon: 'dot-chart' },
];

export default class DataView extends React.Component {
  state = { showedVisual: null, defaultPrefer: {} }

  componentDidMount() {
    const { dimensions } = this.props;

    this.setState({
      defaultPrefer: this.getDefaultPreferDimension(dimensions),
    });

    this.setVisual(VISUAL.TABLE);
  }

  getDefaultPreferDimension = (dimensions = [], prefer = {}) => {
    const dimensionsRank = [];

    // rank dimensions
    dimensions.forEach(({ id, concept, codeList }) => {
      if (!(['TIME_PERIOD', 'OBS_VALUE'].includes(id))) {
        dimensionsRank.push({
          id,
          name: concept.name,
          size: Object.keys(codeList.codes).length,
        });
      }
    });

    _.sortBy(dimensionsRank, ['size']);

    // build default prefer
    const pullDimension = (id) => {
      const preferDimension = _.find(dimensionsRank, { id });
      const selDimension = preferDimension ? preferDimension : _.last(dimensionsRank);

      _.remove(dimensionsRank, item => item.id === selDimension.id);

      return selDimension ? selDimension.name : null;
    };

    return {
      xAxis: ['TIME_PERIOD'],
      yAxis: [pullDimension(prefer.yAxis)],
      obsVal: ['OBS_VALUE'],
    };
  }

  getVisual = (visualType, series = [], dimensions = []) => {
    const { source = 'sdmx' } = this.props;
    const { defaultPrefer } = this.state;

    if (visualType != null) {
      const split = visualType.split('/');

      switch (split[0]) {
        case VISUAL.CHART:
          // chart terpisah echart
          /*return (
            <div className="pa2 pr5 bg-white">
              <Chart
                key={visualType}
                type={split[1]}
                series={series}
                dimensions={dimensions}
                {...defaultPrefer}
              />
            </div>
          );*/

        case VISUAL.PIVOT:
          return (
            <div style={{ background: '#d9dde8' }}>
              <Pivot
                dimensions={dimensions}
                series={series}
                rendererName={split[1] || 'Table'}
                defaultPrefer={defaultPrefer}
              />
            </div>
          );

        case VISUAL.TABLE:
          return (
            <div className="ba b--moon-gray bg-white" style={{ overflowY: 'auto' }}>
              <Tabular dimensions={dimensions} series={series} />
            </div>
          );

        case VISUAL.MAP:
          return (
            <DataMap defaultPrefer={defaultPrefer} series={series} />
          );

        case VISUAL.LINKS:
          return (
            <Links source={source} />
          );

        default:
          return <div />;
      }
    }

    return null;
  }

  setVisual(type) {
    const { showedVisual } = this.state;

    this.setState({
      showedVisual: showedVisual != null && type === showedVisual ? null : type,
    });
  }

  render() {
    const {
      dataId,
      title = 'Data',
      protocol,
      dimensions = [],
      series: dataSeries = [],
      filter,
    } = this.props;
    const { showedVisual, defaultPrefer } = this.state;

    // normalize series with all of the dimensions
    const series = dataSeries.map((item) => {
      const data = { ...item };

      dimensions.forEach((dim) => {
        if (data[dim.id] === undefined) {
          data[dim.id] = '';
        }
      });

      return data;
    });

    return (
      <Empty isEmpty={series.length == 0} className="relative">
        {/*<div className="flex justify-between mb2">
          <span className="primary f3 b db ph3">{title}</span>
            <span className="gray f5 i db" style={{ lineHeight: '30px' }}>
            Last Update, {moment().format('DD MMMM YYYY')}
          </span>
        </div>*/}

        {/* Header Menu */}
        <div className="flex bg-primary justify-between">
          <Menu mode="horizontal" theme="dark" className="bg-transparent">
            {/* <Menu.Item key="metadata" style={{ border: 'none', background: 'none' }}>
              <span className="pointer white b">
                <Icon type="info" style={{ fontSize: '14px' }} className="white" /> Metadata
              </span>
            </Menu.Item> */}

            <Menu.Item
              key="table"
              style={{ border: 'none', background: 'none' }}
              onClick={() => this.setVisual(VISUAL.TABLE)}
            >
              <div className="white">
                <Icon type="table" style={{ fontSize: '14px' }} className="white" />
                <b>Tabular</b>
              </div>
            </Menu.Item>

            <Menu.Item
              key="pivot"
              style={{ border: 'none', background: 'none' }}
              onClick={() => this.setVisual(VISUAL.PIVOT)}
            >
              <div className="white">
                <Icon type="layout" style={{ fontSize: '14px' }} className="white" />
                <b>Pivot Table</b>
              </div>
            </Menu.Item>

            <Menu.Item key="chart" style={{ border: 'none', background: 'none' }}>
              <Dropdown
                overlay={(
                  <div className="bg-white ba b--black-10 br2 pv2 ph1 shadow-1 flex">
                    {CHART_TYPES.map(({ type, label, icon }) => (
                      <span
                        key={type}
                        className="pointer tc mh3 gray hover-dark-gray"
                        onClick={() => this.setVisual(type)}
                      >
                        <Icon type={icon} className="f1" /><br />
                        <span className="text-primary b"> {label} </span>
                      </span>
                    ))}
                  </div>
                )}
              >
                <span className="white">
                  <Icon type="area-chart" style={{ fontSize: '14px' }} className="white" />
                  <b>Chart</b>
                  <Icon type="caret-down" style={{ fontSize: '14px' }} className="ml2" />
                </span>
              </Dropdown>
            </Menu.Item>

            {_.find(dimensions, { id: 'GEO' }) && (
              <Menu.Item key="map" style={{ border: 'none', background: 'none' }}>
                <span className="white b" onClick={() => this.setVisual(VISUAL.MAP)}>
                  <Icon type="environment" className="white" /> Map
                </span>
              </Menu.Item>
            )}

            <Menu.Item key="export" style={{ border: 'none', background: 'none' }}>
              <Dropdown
                overlay={(
                  <Export
                    protocol={protocol}
                    dataId={dataId}
                    filter={filter}
                  />
                )}
              >
                <div className="white">
                  <Icon type="export" style={{ fontSize: '14px' }} className="ml2" />
                  <b>Export</b>
                  <Icon type="caret-down" className="ml2" />
                </div>
              </Dropdown>
            </Menu.Item>

            <Menu.Item
              key="links"
              style={{ border: 'none', background: 'none' }}
              onClick={() => this.setVisual(VISUAL.LINKS)}
            >
              <div className="white">
                <Icon type="link" style={{ fontSize: '14px' }} className="white" />
                <b>Links</b>
              </div>
            </Menu.Item>
          </Menu>
        </div>

        {/* Chart View */}
        {showedVisual && (
          <div className="relative">
            {/* Close Visual View */}
            {/*<div
              className="pointer absolute pa2 ma2"
              style={{ right: 0, top: 0, zIndex: 1 }}
              onClick={() => this.setVisual(null)}
            >
              <Icon type="close" className="f2 b" />
            </div>*/}

            {/* Display Visuals (map/chart/links) */}
            <div className="bg-moon-gray">
              {this.getVisual(showedVisual, series, dimensions)}
            </div>

            {/* Separator */}
            {/*<div className="bg-primary pa3" />*/}
          </div>
        )}

        {/* Pivot Table */}
        {/*<div style={{ background: '#d9dde8' }}>
          <Pivot
            dimensions={dimensions}
            series={series}
            // defaultPrefer={defaultPrefer}
            defaultPrefer={this.getDefaultPreferDimension(dimensions)}
          />
        </div>*/}
      </Empty>
    );
  }
}
