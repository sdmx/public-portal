import React from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import { Button, Form, Select, DatePicker, Input } from 'antd';
import moment from 'moment';
import _ from 'lodash';

class Filter extends React.PureComponent {
  constructor(props) {
    super(props);

    const { dimensions, filterKeys = {}, startPeriod = null, endPeriod = null } = props;

    // set default filter values
    const filter = {};

    if (_.isArray(dimensions)) {
      dimensions.forEach((dimension, i) => {
        if (filterKeys[dimension.id]) {
          filter[dimension.id] = _.filter(filterKeys[dimension.id].split('+'));
        }
      });
    }

    this.state = {
      filter,
      timePeriod: [
        startPeriod ? moment(startPeriod) : null,
        endPeriod ? moment(endPeriod) : null,
      ],
    };
  }

  handleChange = (id, value) => {
    const { filter } = this.state;

    filter[id] = value;
    this.setState({ filter: { ...filter } });
  };

  getInputFilter = ({ id, codeList }) => {
    const { filter } = this.state;

    // single select combobox
    if (id === 'FREQ') {
      return (
        <Form.Item className="fl w-60">
          <Select
            showSearch
            placeholder="Select..."
            optionFilterProp="children"
            onChange={(value) => this.handleChange(id, value)}
            defaultValue={filter[id]}
            filterOption={(input, option) => (
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            )}
          >
            {Object.keys(codeList.codes).map(keyCode => (
              <Select.Option key={keyCode} value={keyCode}>
                {codeList.codes[keyCode]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      );
    }
    // multiple select input
    else if (codeList !== null) {
      return (
        <Form.Item className="fl w-60">
          <Select
            showSearch
            allowClear
            placeholder="All"
            optionFilterProp="children"
            onChange={(value) => this.handleChange(id, value)}
            mode="multiple"
            defaultValue={filter[id]}
            filterOption={(input, option) => (
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            )}
          >
            {Object.keys(codeList.codes).map(keyCode => (
              <Select.Option key={keyCode} value={keyCode}>
                {codeList.codes[keyCode]}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      );
    }

    return (
      <Form.Item className="fl w-60">
        <Input
          name={id}
          defaultValue={filter[id]}
          onChange={(value) => this.handleChange(id, value)}
        />
      </Form.Item>
    );
  }

  render() {
    const { dimensions = [] } = this.props;
    const { filter, timePeriod } = this.state;

    return (
      <form onSubmit={this.props.handleSubmit} data={this.props.data}>
        <div className="cf mb3">
          {/* Dimensions Filter */}
          {dimensions.map((dimension, i) => {
            const { id, concept } = dimension;

            return (
              <div key={id} className="fl w-50">
                <label className="fl w-30 tr pr4 pv2" title={concept.description}>
                  {concept.name}
                </label>
                {this.getInputFilter(dimension)}
              </div>
            );
          })}

          {/* Time Period Filter */}
          <div className="fl w-50">
            <label className="fl w-30 tr pr4 pv2" title="Time Period"> Time Period </label>
            <Form.Item className="fl w-60">
              <DatePicker.RangePicker
                defaultValue={timePeriod}
                onChange={(value) => this.setState({ timePeriod: value })}
              />
            </Form.Item>
          </div>
        </div>

        {/* Submit Button */}
        <div className="cf tr pt3 pr4 mr3 bt b--moon-gray">
          <Button
            type="primary"
            onClick={() => {
              // format key filter series
              const filterKeys = {};

              dimensions.forEach(({ id }, i) => {
                // format array to string value (using '+')
                const value = _.isArray(filter[id]) ? filter[id].join('+') : filter[id];

                if (!_.isNil(value) && value.length > 0) {
                  filterKeys[id] = value;
                }
              });

              // submit parameter values
              this.props.onSubmit({
                filterKeys,
                startPeriod: timePeriod[0] ? moment(timePeriod[0]).format('YYYY-MM-DD') : null,
                endPeriod: timePeriod[1] ? moment(timePeriod[1]).format('YYYY-MM-DD') : null,
              });
            }}
          >
            Filter
          </Button>
        </div>
      </form>
    );
  }
}

export default Filter;
