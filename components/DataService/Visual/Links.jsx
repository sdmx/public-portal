import React from 'react';

export default class Links extends React.Component {
  render() {
    const { source } = this.props;

    return (
      <div className="pa3 pr5 bg-white" style={{ minHeight: '50px' }}>
        <b>Links: </b>
        <ul>
          {source === 'sdmx' && (
            <li>
              <a href={process.env.SDMX_REGISTRY_URL} target="_blank">
                {process.env.SDMX_REGISTRY_URL}
              </a>
            </li>
          )}
          {source === 'fame' && (
            <li>
              <a href={process.env.FAME_REST_ENDPOINT} target="_blank">
                {process.env.FAME_REST_ENDPOINT}
              </a>
            </li>
          )}
        </ul>
      </div>
    );
  }
}
