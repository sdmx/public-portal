import React from 'react';
import ReactTable from 'react-table';
import { Set } from 'immutable';
import { Modal, Checkbox, Icon } from 'antd';

export default class Tabular extends React.Component {
  constructor(props) {
    super(props);

    const { series, dimensions, includeTimeDimension = true } = props;
    const columns = this.prepareColumns(dimensions, series);

    this.state = {
      series,
      columns,
      showedColumns: Set(Object.keys(columns)),
      showModal: false,
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  prepareColumns = (dimensions, series) => {
    const keyColumns = Object.keys(series[0]);
    const columns = {};

    keyColumns.forEach((id) => {
      const dimension = series.find(dim => dim.id === id);

      columns[id] = {
        id,
        name: dimension ? dimension.concept.name : id,
      };
    });

    return columns;
  }

  openModal = () => {
    this.setState({ showModal: true });
  }

  closeModal = () => {
    this.setState({ showModal: false });
  }

  render() {
    const { series, showedColumns, columns, showModal } = this.state;

    const tblColumns = Object.values(columns).map(({ id, name }) => {
      const isColumnShown = showedColumns.includes(id);
      const colWidth = isColumnShown ? undefined : 0;

      return {
        accessor: id,
        Header: name,
        Cell: ({ original }) => (isColumnShown ? original[id] : null),
        show: () => isColumnShown,
        width: colWidth,
      };
    });

    tblColumns.push({
      sortable: false,
      filterable: false,
      width: 50,
      Header: () => (
        <Icon
          type="setting"
          className="pointer"
          onClick={() => {
            this.openModal();
          }}
        />
      ),
    });

    return (
      <div className="ba b--moon-gray bg-white" style={{ overflowY: 'auto' }}>
        <ReactTable
          filterable
          data={series}
          className="f7 tc"
          columns={tblColumns}
        />
        <Modal
          title="Visibility"
          visible={showModal}
          onCancel={this.closeModal}
          footer={null}
        >
          {Object.values(columns).map(({ id, name }) => (
            <div>
              <Checkbox
                checked={showedColumns.includes(id)}
                onChange={({ target }) => {
                  let updatedShowedColumns;

                  if (target.checked) {
                    updatedShowedColumns = showedColumns.toList().push(id).toSet();
                  }
                  else {
                    updatedShowedColumns = showedColumns.filter(v => v !== id);
                  }

                  this.setState({ showedColumns: updatedShowedColumns });
                }}
              >
                {name}
              </Checkbox>
            </div>
          ))}
        </Modal>
      </div>
    );
  }
}
