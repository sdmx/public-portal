import React from 'react';
import { message, Tree } from 'antd';
import Router from 'next/router';
import _ from 'lodash';

import Loading from '~/components/Loading';
import { DataService } from '~/utils/api';

// #dev will change to ajax
import ILO from './data/ILO.json';
import OECD from './data/OECD.json';
import EUROSTAT from './data/EUROSTAT.json';

class DataCategory extends React.Component {
  state = {
    expandedKeys: [],
    autoExpandParent: true,
    selectedKeys: [],
  }

  onExpand = (expandedKeys) => {
    this.setState({
      expandedKeys,
      autoExpandParent: false,
    });
  }

  onSelect = (selectedKeys, info) => {
    this.setState({ selectedKeys });
  }

  renderTreeNodes = data => data.map((item) => {
    if (item.children) {
      return (
        <Tree.TreeNode title={item.title} key={item.key} dataRef={item}>
          {this.renderTreeNodes(item.children)}
        </Tree.TreeNode>
      );
    }

    return <Tree.TreeNode {...item} />
  })

  getTreeData = (provider) => {
    switch (provider) {
      case 'ILO': return ILO;
      case 'OECD': return OECD;
      case 'EUROSTAT': return EUROSTAT;
      default: return [];
    }
  }

  render() {
    const { provider } = this.props;

    return (
      <div className="overflow-hidden w-100 h-100">
        <Tree
          onExpand={this.onExpand}
          expandedKeys={this.state.expandedKeys}
          autoExpandParent={this.state.autoExpandParent}
          onSelect={this.onSelect}
          selectedKeys={this.state.selectedKeys}
        >
          {this.renderTreeNodes(this.getTreeData(provider))}
        </Tree>
      </div>
    );
  }
}

export default DataCategory;
