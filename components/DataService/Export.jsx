import React from 'react';
import { Icon } from 'antd';

import ExportButton from './ExportButton';

const MenuContainer = ({ children }) => (
  <div className="bg-white ba b--black-10 br2 pv2 pr1 shadow-1 cf" style={{ width: '210px' }}>
    {children}
  </div>
);

class Export extends React.Component {
  render() {
    const { filter = {}, dataId, protocol } = this.props;

    return (
      <MenuContainer>
        <ExportButton
          type="xls"
          text="Excel"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<Icon type="file-excel" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
        />
        <ExportButton
          type="csv"
          text="CSV"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<Icon type="file-excel" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
        />
        <ExportButton
          type="pdf"
          text="PDF"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<Icon type="file-pdf" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
        />
        <ExportButton
          type="xml"
          text="XML"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<b className="f3 fl">{'</>'}</b>}
        />
        <ExportButton
          type="rdf"
          text="RDF"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<Icon type="file" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
        />
        <ExportButton
          type="xml"
          text="HTML"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<Icon type="html5" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
          // icon={<b className="f3 fl">{'</>'}</b>}
        />
        <ExportButton
          type="dspl"
          text="DSPL"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<Icon type="google" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
        />
        <ExportButton
          type="json"
          text="JSON"
          protocol={protocol}
          dataId={dataId}
          filter={filter}
          icon={<b className="f3 fl ph1">{'{}'}</b>}
        />
        <ExportButton
          text="SDMX"
          dataId={dataId}
          protocol={protocol}
          filter={filter}
          icon={<Icon type="file-text" className="f2 pv1 fl" style={{ fontSize: '24px' }} />}
          child={
            <MenuContainer>
              <ExportButton
                type="sdmx-ml"
                text="SDMX-ML 2.0"
                protocol={protocol}
                dataId={dataId}
                filter={filter}
                params={{ version: '2.0' }}
              />
              <ExportButton
                type="sdmx-ml"
                text="SDMX-ML 2.1"
                protocol={protocol}
                dataId={dataId}
                filter={filter}
                params={{ version: '2.1' }}
              />
              <ExportButton
                type="sdmx-dsd"
                text="SDMX-DSD"
                protocol={protocol}
                dataId={dataId}
                filter={filter}
              />
              <ExportButton
                type="sdmx-edi"
                text="SDMX-EDI"
                protocol={protocol}
                dataId={dataId}
                filter={filter}
              />
            </MenuContainer>
          }
        />
      </MenuContainer>
    );
  }
}

export default Export;
