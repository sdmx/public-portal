import React from 'react';
import Router from 'next/router';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Button, Checkbox, Pagination, Input, Icon } from 'antd';
import _ from 'lodash';
import uuid from 'uuid/v1';

import { Storage } from '~/utils/api';
import Link from '~/components/Link';
import Loading from '~/components/Loading';
import Empty from '~/components/Empty';

export default class DataflowList extends React.Component {
  static propTypes = {
    pageSize: PropTypes.number,
  };

  static defaultProps = {
    pageSize: 10,
  };

  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      selected: [],
      data: [],
      totalPages: 1,
      count: 0,
      page: 1,
      loading: true,
    };

    this.fetchList = this.fetchList.bind(this);
  }

  componentDidMount() {
    const { query = '', category } = this.props;
    this.fetchList(1, query, { category });
  }

  componentWillReceiveProps({ category }) {
    if (this.props.category !== category) {
      this.fetchList(1, '', { category });
    }
  }

  fetchList(page = 1, query = '', params = {}) {
    const { pageSize, source } = this.props;
    const queryParams = _.omitBy(params, _.isNull);

    this.setState({ searchQuery: query, loading: true });

    axios.get(`${process.env.SDMX_ENDPOINT}/data/${source}`, {
      params: {
        q: query,
        size: pageSize,
        page,
        ...queryParams,
      },
    })
      .then((res) => {
        const { content = [], totalPages, totalElements } = res.data;

        this.setState({
          page,
          data: content,
          totalPages,
          count: totalElements,
          loading: false,
        });
      })
      .catch(() => this.setState({ loading: false }));
  }

  buildDataset(source, selected) {
    this.setState({ loading: true });

    const expireTime = new Date().getTime() + (1000 * 3600 * 24 * 3); // 3 day
    const buildId = uuid();

    Storage.set(buildId, selected.join('|'), expireTime);
    Router.push(`/data/sdmx/${source}/build/${buildId}`);
  }

  toggleDataset(code, checked) {
    const { selected } = this.state;

    _.pull(selected, code);

    if (checked) {
      selected.push(code);
    }

    this.setState({ selected: [ ...selected ] });
  }

  render() {
    const { data = [], totalPages, page, count, searchQuery, loading, selected = [] } = this.state;
    const { pageSize, source } = this.props;

    return (
      <Loading isFinish={!loading}>
        {selected.length > 0 && (
          <div className="cf tr f7 pv1">
            <i className="pr2">{selected.length} items are selected.</i>
            <Button type="primary" onClick={() => this.buildDataset(source, selected)}>Go</Button>
          </div>
        )}

        <div className="cf overflow-auto">
          <table className="w-100 center" cellSpacing="0">
            <colgroup>
              <col width="60px" />
              <col width="120px" />
            </colgroup>
            <thead>
              <tr className="bg-primary">
                <th className="pa2 tc white"></th>
                <th className="pa2 tc white">Code</th>
                <th className="pa2 tc white">Name</th>
                <th className="pa2 tr">
                  <Input.Search
                    placeholder="Search..."
                    className=" navbar-search bg-primary white"
                    value={searchQuery}
                    onChange={e => this.setState({ searchQuery: e.target.value })}
                    onSearch={query => this.fetchList(1, query)}
                  />
                </th>
              </tr>
            </thead>
            <tbody className="lh-copy">
              {data.map(item => (
                <tr key={item.id}>
                  <td className="tc pv3">
                    <Checkbox onChange={({ target }) => this.toggleDataset(item.id, target.checked)} />
                  </td>
                  <td className="tc pa3 br mr2 b">{item.id}</td>
                  <td>
                    <div className="ml3 pa3 bb b--black-30 db" style={{ lineHeight: '26px' }}>
                      <Link className="b black-80" href={`/data/sdmx/${source}/view/${item.id}`}>
                        {item.name}
                      </Link>
                    </div>
                  </td>
                  <td className="tr">
                    <div className="pa3 bb b--black-30 db">
                      <Icon type="file-pdf" className="f3 ph2 dark-blue" />
                      <Icon type="file-text" className="f3 ph2 dark-blue" />
                      <Icon type="share-alt" className="dark-blue ph2" />
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <br />
        <div className="tc">
          {totalPages > 1 && (
            <Pagination
              total={count}
              current={page}
              pageSize={pageSize}
              onChange={pageNum => this.fetchList(pageNum, searchQuery)}
            />
          )}
        </div>
      </Loading>
    );
  }
}
