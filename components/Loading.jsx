import React from 'react';
import { Icon } from 'antd';

const LoadingDisplay = (
  <div className="w-100 h-100 tc gray f1" style={{ marginTop: '10%' }}>
    <Icon type="loading" className="f1" /> Loading
  </div>
);

class Loading extends React.Component {
  render() {
    const { isFinish = true, display = LoadingDisplay, children = '-' } = this.props;

    return isFinish ? children : display;
  }
}

export default Loading;
