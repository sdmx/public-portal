import React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import Slider from 'react-slick';
import dynamic from 'next/dynamic';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import dataInflasi from '~/samples/indicator/inflasi.json';
import dataBi7days from '~/samples/indicator/bi-7days.json';
import dataKursBiUsd from '~/samples/indicator/kursBI-USD.json';
import dataKursJisdorUsd from '~/samples/indicator/kursJISDOR-USD.json';

const Plot = dynamic(import('react-plotly.js'), { ssr: false });

const principalIndicator = [
  { label: 'GDP Growth Rate', value: -0.4, diff: 1.7, period: moment().format('YYYY/MM') },
  { label: 'CPI', value: 104.88, diff: 0.7, period: moment().format('YYYY/MM') },
  { label: 'PPI', value: 103.73, diff: 0.4, period: moment().format('YYYY/MM') },
  { label: 'Current Account', value: 4949, diff: 0, period: moment().format('YYYY/MM') },
  { label: 'M2 (Average)', value: 2763, diff: 6.6, period: moment().format('YYYY/MM') },
];

const sliderSettings = {
  // swipe: true,
  // autoplaySpeed: 2000,
  adaptiveHeight: true,
  autoplay: true,
  vertical: true,
  dots: false,
  arrows: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
};

const plotConfig = {
  layout: {
    height: 200,
    showlegend: false,
    paper_bgcolor: 'rgba(0,0,0,0)',
    plot_bgcolor: 'rgba(0,0,0,0)',
    font: { color: '#fff' },
    margin: { l: 40, r: 40, t: 20, b: 40 },
  },
  config: {
    displayModeBar: false,
    responsive: true,
  },
};

class Homepage extends React.Component {
  render() {
    return (
      <div className="overflow-hidden bg-primary-light pa3">
        <div className="bg-primary flex">
          {/* Logo Bank Indonesia */}
          <div className="w-25 pa3">
            <div className="">
              <img
                src="/static/img/logo-inverse.png"
                alt="Logo Bank Indonesia"
                style={{ width: '100%' }}
              />
            </div>
          </div>

          {/* Charts */}
          <div className="w-50 pr4">
            <Slider {...sliderSettings}>
              {/* Inflasi */}
              <div style="height: 200px">
                <div className="flex pt4 white">
                  <div className="f4 tc">Tingkat Inflasi</div>
                </div>
                <Plot
                  layout={plotConfig.layout}
                  config={plotConfig.config}
                  data={[
                    {
                      type: 'scatter',
                      mode: 'lines+points',
                      name: 'Tingkat Inflasi',
                      marker: { color: '#fff' },
                      y: dataInflasi.map(({ value }) => value),
                      x: dataInflasi.map(({ period }) => period),
                    },
                  ]}
                />
              </div>

              {/* BI 7 Days */}
              <div style="height: 200px">
                <div className="flex pt4 white">
                  <div className="f4 tc">7 Days</div>
                </div>
                <Plot
                  layout={plotConfig.layout}
                  config={plotConfig.config}
                  data={[
                    {
                      type: 'scatter',
                      mode: 'lines+points',
                      name: 'Repo Rate',
                      marker: { color: '#fff' },
                      y: dataBi7days.map(({ value }) => value),
                      x: dataBi7days.map(({ date }) => date),
                    },
                  ]}
                />
              </div>

              {/* Kurs JISDOR-USD */}
              <div style="height: 200px">
                <div className="flex pt4 white">
                  <div className="f4 tc">Kurs Referensi JISDOR USD-IDR</div>
                </div>
                <Plot
                  layout={plotConfig.layout}
                  config={plotConfig.config}
                  data={[
                    {
                      type: 'scatter',
                      mode: 'lines+points',
                      name: 'Kurs',
                      marker: { color: '#fff' },
                      y: dataKursJisdorUsd.map(({ value }) => value),
                      x: dataKursJisdorUsd.map(({ date }) => date),
                    },
                  ]}
                />
              </div>

              {/* Kurs BI-USD */}
              <div style="height: 200px">
                <div className="flex pt4 white">
                  <div className="f4 tc">Kurs USD</div>
                </div>
                <Plot
                  layout={plotConfig.layout}
                  config={plotConfig.config}
                  data={[
                    {
                      type: 'scatter',
                      name: 'Kurs Jual',
                      mode: 'lines+points',
                      marker: { color: '#fff' },
                      y: dataKursBiUsd.map(({ jual }) => jual),
                      x: dataKursBiUsd.map(({ date }) => date),
                    },
                    {
                      type: 'scatter',
                      name: 'Kurs Beli',
                      mode: 'lines+points',
                      marker: { color: '#f90' },
                      y: dataKursBiUsd.map(({ beli }) => beli),
                      x: dataKursBiUsd.map(({ date }) => date),
                    },
                  ]}
                />
              </div>

              {/* GDP */}
              {/* <div>
                <div className="flex pt4">
                  <div className="w-33 white tc">
                    <div className="f4">GDP</div>
                    <div className="f7">{moment().format('YYYY/MM')}</div>
                  </div>
                  <div className="w-33 orange tc">
                    <div className="f4">-0.3%</div>
                    <div className="f7">Quarter-on-quarter</div>
                  </div>
                  <div className="w-33 white tc">
                    <div className="f4">1.8%</div>
                    <div className="f7">Year on year</div>
                  </div>
                </div>
                <Plot
                  layout={plotConfig.layout}
                  config={plotConfig.config}
                  data={[
                    {
                      type: 'scatter',
                      mode: 'lines+points',
                      marker: { color: '#fff' },
                      y: [2, 4.6, 2.3, 2.6, 3.4, 3.9, -2, 6.2, 3],
                      // x: [1, 2, 3],
                    },
                    {
                      type: 'bar',
                      y: [2.3, 3.4, 3.9, -2, 2.6, 4.6, 2, 6.2, 3],
                      // x: [1, 2, 3],
                    },
                  ]}
                />
              </div> */}
              {/* PPI */}
              {/* <div>
                <div className="flex pt4">
                  <div className="w-40 white tc">
                    <div className="f4">PPI</div>
                    <div className="f7">{moment().format('YYYY/MM')}</div>
                  </div>
                  <div className="w-60 tc white">
                    <div className="f4">-0.2%</div>
                  </div>
                </div>
                <Plot
                  layout={plotConfig.layout}
                  config={plotConfig.config}
                  data={[
                    {
                      type: 'scatter',
                      mode: 'lines+points',
                      marker: { color: '#fff' },
                      y: [2.3, 2.6, 4.6, 3.4, 3.9, 2, -1, 6.2, 3],
                      // x: [1, 2, 3],
                    },
                  ]}
                />
              </div> */}
            </Slider>
          </div>

          {/* Indicators */}
          <div className="w-25 pa3">
            <div className="overflow-hidden br3">
              <div className="bg-primary-lightest pa2 tc b">
                Principal Indicators
              </div>
              <table className="w-100 bg-white" cellPadding="7px">
                {principalIndicator.map((item) => {
                  let diffValue;

                  if (item.diff === 0) {
                    diffValue = '-';
                  }
                  else if (item.diff > 0) {
                    diffValue = `+${item.diff}`;
                  }
                  else {
                    diffValue = item.diff;
                  }

                  return (
                    <tr key={item.label}>
                      <td><b>{item.label}</b></td>
                      <td className={classNames({ 'dark-blue': item.value < 0 })}>
                        <b>{item.value}</b>
                      </td>
                      <td className={classNames({ red: item.diff > 0, 'dark-blue': item.diff <= 0 })}>
                        <b>{diffValue}</b>
                      </td>
                      <td>{item.period}</td>
                    </tr>
                  );
                })}
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homepage;
