import React from 'react';

import Link from '~/components/Link';
import Loading from '~/components/Loading';

class DataflowResults extends React.Component {
  render() {
    const { data = [], loading = false, urlResolver } = this.props;

    return (
      <Loading isFinish={!loading}>
        {data.length > 0 ? (
          <div className="w-60-l">
            {data.map(({ id, name }) => (
              <div key={id} className="mv1 pv1 flex bb b--moon-gray">
                <Link href={urlResolver(id)} className="text-primary f6">{name}</Link>
              </div>
            ))}
          </div>
        ) : (
          <div className="f3 silver">No Search Results Found.</div>
        )}
      </Loading>
    );
  }
}

export default DataflowResults;
