import React from 'react';
import Img from 'react-image-fallback';
import striptags from 'striptags';
import { Icon } from 'antd';

import Link from '~/components/Link';
import { Content } from '~/utils/api';

const DESC_LIMIT = 200;

const ListItem = ({ name, url = '#', icon, body, thumbnail }) => (
  <div className="flex mv1 pt1 pb2 ph3 bb b--moon-gray">
    <div className="w-20 pr3">
      <Img
        alt={name}
        src={Content.fileUrl(thumbnail)}
        fallbackImage="/static/img/default/publication.png"
      />
    </div>
    <div className="w-80">
      <div className="flex">
        <Link href={url} className="text-primary f6 b">{name}</Link>
        {/*icon && <Icon type={icon} className="f6 ph3" />*/}
      </div>
      {body && (
        <p className="ma0 f7">
          {
            body.length > DESC_LIMIT
              ? `${striptags(body).substring(0, DESC_LIMIT)}...`
              : striptags(body)
          }
        </p>
      )}
    </div>
  </div>
);

export default ListItem;
