import React from 'react';

import Loading from '~/components/Loading';
import ListItem from '~/components/Search/ListItem';

class NewsResults extends React.Component {
  render() {
    const { data = [], loading } = this.props;

    return (
      <Loading isFinish={!loading}>
        {data.length > 0 ? (
          <div className="w-50-l">
            {data.map(({ id, ...props }) => (
              <ListItem key={id} url={`/content/news/${id}`} icon="profile" {...props} />
            ))}
          </div>
        ) : (
          <div className="f3 silver">No Search Results Found.</div>
        )}
      </Loading>
    );
  }
}

export default NewsResults;
