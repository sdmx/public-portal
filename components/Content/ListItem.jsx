import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Pagination, Icon, message, Input } from 'antd';
import axios from 'axios';
import Img from 'react-image-fallback';

import Empty from '~/components/Empty';
import Link from '~/components/Link';
import { Content } from '~/utils/api';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      data: [],
      totalPages: 1,
      count: 0,
      pageNum: 1,
      loading: true,
    };

    this.fetchList = this.fetchList.bind(this);

    this.fetchList();
  }

  fetchList(pageNum = 1, query = '') {
    const { pageSize, contentType } = this.props;

    Content.list(contentType, query, pageSize, (pageNum - 1) * pageSize)
      .then(({ data }) => {
        this.setState({
          pageNum,
          data: data.data,
          totalPages: Math.ceil(data.totalCount / pageSize),
          count: data.totalCount,
          loading: false,
        });
      })
      .catch((e) => {
        message.error(e.message);
        this.setState({ loading: false });
      });
  }

  render() {
    const { title, contentType, pageSize } = this.props;
    const { data, totalPages, pageNum, count, loading } = this.state;

    return !loading && (
      <div>
        <div className="bg-primary-dark tc pv3 white f3">
          {title.toUpperCase()}
        </div>
        <div className="pa2 bl br bb black-20">
          <Input.Search
            placeholder="Search..."
            className="navbar-search"
            value={this.state.searchQuery}
            onChange={e => this.setState({ searchQuery: e.target.value })}
            onSearch={query => this.fetchList(1, query)}
          />
        </div>
        <div className="pa2 bl br bb black-20">
          <Empty isEmpty={data.length === 0}>
            {data.map(item => (
              <Link
                key={item.id}
                className="black-80 dib mv1 w-100"
                href={`/content/${contentType}/${item.id}`}
              >
                <div className="flex">
                  <div className="w-20 br2 overflow-hidden" style={{ height: '40px' }}>
                    <Img
                      src={`${process.env.CMS_ENDPOINT}/file/${item.thumbnail}`}
                      fallbackImage="/static/img/default/content.png"
                      alt={item.name}
                    />
                  </div>
                  <div className="w-80 ph2">
                    <div className="b truncate">{item.name}</div>
                    <div className="gray f7">
                      {moment(item.created).format('DD MMM YYYY | HH:mm')}
                    </div>
                  </div>
                </div>
              </Link>
            ))}

            <div className="tc">
              {totalPages > 1 && (
                <Pagination
                  total={count}
                  current={pageNum}
                  pageSize={pageSize}
                  onChange={currPageNum => this.fetchList(currPageNum)}
                />
              )}
            </div>
          </Empty>
        </div>
      </div>
    );
  }
}


ListItem.propTypes = {
  pageSize: PropTypes.number,
};

ListItem.defaultProps = {
  pageSize: 10,
};

export default ListItem;
