import React from 'react';
import Img from 'react-image-fallback';
import striptags from 'striptags';
import moment from 'moment';
import _ from 'lodash';
import { Carousel } from 'antd';

import { Content } from '~/utils/api';
import Link from '~/components/Link';
import Loading from '~/components/Loading';

import ContentSlider from './ContentSlider';

class Newest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };

    this.fetchSlider = this.fetchSlider.bind(this);
  }

  componentDidMount() {
    this.fetchSlider(5);
  }

  fetchSlider(limit) {
    Content.list(this.props.contentType, '', limit)
      .then(({ data }) => this.setState({
        data: _.isArray(data.data) ? data.data : [],
        loading: false,
      }));
  }

  render() {
    const { contentType } = this.props;
    const { data, loading } = this.state;

    return (
      <Loading isFinish={!loading} className="w-100 h-100">
        <Carousel autoplay className="ph2">
          {data.map(item => (
            <ContentSlider
              key={item.id}
              title={item.name}
              href={`/content/${contentType}/${item.id}`}
              content={item.body}
              created={item.created}
              thumbnail={`${process.env.CMS_ENDPOINT}/file/${item.thumbnail}`}
            />
          ))}
        </Carousel>

        <div className="flex justify-between">
          {data.splice(1, 3).map(item => (
            <div key={item.id} className="w-33 ph2 br b--black-10">
              <Link
                title={item.name}
                className="black-70 hover-black"
                href={`/content/${contentType}/${item.id}`}
              >
                <div className="overflow-hidden" style={{ height: '100px' }}>
                  <Img
                    src={`${process.env.CMS_ENDPOINT}/file/${item.thumbnail}`}
                    fallbackImage="/static/img/default/content.png"
                    className="w-100"
                    alt={item.name}
                  />
                </div>
                <div className="b pv1 truncate"> {item.name} </div>
                <div className="f7 gray">
                  {moment(item.created).format('DD MMM YYYY | HH:mm')}
                </div>
                <div className="f7">
                  {striptags(item.body).length > 60
                    ? `${striptags(item.body).substr(0, 60)}...`
                    : striptags(item.body)}
                </div>
              </Link>
            </div>
          ))}
        </div>
      </Loading>
    );
  }
}

export default Newest;
