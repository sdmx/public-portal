import React from 'react';
import { message } from 'antd';
import Img from 'react-image-fallback';

import { Content } from '~/utils/api';
import Link from '~/components/Link';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };

    this.fetchList = this.fetchList.bind(this);

    this.fetchList();
  }

  fetchList(record = 7) {
    Content.list(this.props.contentType, '', record)
      .then(({ data }) => {
        this.setState({
          data: data.data,
          loading: false,
        });
      })
      .catch((e) => {
        message.error(e.message);
        this.setState({ loading: false });
      });
  }

  render() {
    const { contentType } = this.props;
    const { data, loading } = this.state;

    return (
      <div className="bg-primary-dark">
        {!loading && data.length > 0 && (
          <div>
            <div className="b white pv2">
              <div className="tc pv1">Highlight this week</div>
              <Link
                key={data[0].id}
                className="dib w-100 bb b--white-10 white-70 hover-white f7"
                href={`/content/${contentType}/${data[0].id}`}
              >
                <div className="overflow-hidden" style={{ maxHeight: '200px' }}>
                  <Img
                    src={`${process.env.CMS_ENDPOINT}/file/${data[0].thumbnail}`}
                    fallbackImage="/static/img/default/content.png"
                    className="w-100"
                    alt={data[0].name}
                  />
                </div>
                <div className="pa2">{data[0].name}</div>
              </Link>
            </div>
            <div className="ph2">
              {data.splice(1).map(item => (
                <Link
                  key={item.id}
                  className="db b w-100 pv2 bb b--white-10 white-80 hover-white f7"
                  href={`/content/${contentType}/${item.id}`}
                >
                  {item.name.length > 60 ? `${item.name.substr(0, 60)}...` : item.name}
                </Link>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default ListItem;
