import React from 'react';
import { Dropdown, Icon, Menu } from 'antd';

import Link from '~/components/Link';

class UserLogin extends React.Component {
  render() {
    const { userLoaded, user, ...attrs } = this.props;

    return (
      <div {...attrs}>
        {userLoaded && (user ? (
          <Dropdown
            trigger={['click']}
            placement="bottomRight"
            overlay={(
              <Menu>
                <Menu.Item key="0">
                  <a href={`${process.env.INTERNAL_PORTAL_URL}`}>
                    <Icon type="project" /> Self-Service
                  </a>
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="3">
                  <Link href="/auth/logout">
                    <Icon type="logout" /> Logout
                  </Link>
                </Menu.Item>
              </Menu>
            )}
          >
            <span className="b pointer f6">
              {user.name}
              <Icon type="down-square-o" className="pl1" />
            </span>
          </Dropdown>
        ) : (
          <div>
            <a href="/auth/login" className="b">Login</a>
          </div>
        ))}
      </div>
    );
  }
}

export default UserLogin;
