import React from 'react';
import { Menu } from 'antd';

import Link from '~/components/Link';

export default class TreeMenu extends React.Component {

  render() {
    const { menu, parent, menuKey = 'x', ...attrs } = this.props;

    const menuItems = menu.map((item) => {
      if (item.children && item.children.length > 0) {
        return (
          <TreeMenu key={item.label} menuKey={menuKey + 1} menu={item.children} parent={item} />
        );
      }

      return (
        <Menu.Item key={item.url} style={{ borderBottom: '0px' }}>
          <Link href={item.url}><b>{item.label}</b></Link>
        </Menu.Item>
      );
    })

    if (parent) {
      return (
        <Menu.SubMenu
          key={parent.label}
          title={<b>{parent.label}</b>}
          style={{ borderBottom: '0px' }}
          {...attrs}
        >
          {menuItems}
        </Menu.SubMenu>
      );
    }

    return (
      <Menu key={menuKey} {...attrs}>
        {menuItems}
      </Menu>
    );
  }
}
