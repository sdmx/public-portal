import React from 'react';
import { Layout, Dropdown, Icon, Menu } from 'antd';
import moment from 'moment';

import Auth from '~/utils/auth';
import Link from '~/components/Link';
import UserLogin from '~/components/Layout/UserLogin';

import TreeMenu from './TreeMenu';

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userLoaded: false,
      user: null,
      // query: props.router !== undefined ? props.router.query.q : '',
    };
  }

  componentDidMount() {
    Auth.user(
      user => this.setState({ user, userLoaded: true }),
      () => this.setState({ userLoaded: true }),
    );
  }

  render() {
    const { menu = [], onSearch } = this.props;
    const { user, userLoaded } = this.state;

    return (
      <div id="header" className="shadow-1 z-1 mb3">
        <div
          className="header-bar bg-primary flex white cover"
          style={{ backgroundImage: 'url(/static/img/bg-footer.jpg)', backgroundPosition: 'bottom' }}
        >
          <div className="tr f7" style={{ width: '230px', lineHeight: '27px' }}>
            <Link href="/about" className="white ph4 f7">About Us</Link>
          </div>
          <div className="tr pr4 pt1" style={{ width: 'calc(100% - 230px)' }}>
            {/* <span href="#">English</span> */}
            {/* <span className="mh2">|</span> */}
            <span>{moment().format('DD MMMM YYYY')}</span>
            {/* <span className="mh2">|</span>
              <span>{moment().format('HH:mm')}</span> */}
          </div>
        </div>

        <Layout.Header className="pa0 bg-white">
          <div className="flex">
            <Link
              href="/"
              className="tc bg-white ph2 h-100 b--silver"
              style={{ width: '230px', float: 'left', padding: '0 25px' }}
            >
              <div><img src="/static/img/logo.png" alt="Logo" /></div>
            </Link>

            <div className="overflow-hidden" style={{ flexGrow: 1 }}>
              <div className="flex justify-between pt3" style={{ fontSize: '20px', lineHeight: '30px' }}>
                {/*<Menu mode="horizontal" style={{ lineHeight: '36px', height: '37px' }}>
                  {menu && menu.map(item => (
                    <Menu.Item key={item.url}>
                      <Link href={item.url} className="b">{item.label}</Link>
                    </Menu.Item>
                  ))}
                </Menu>*/}

                <div style={{ flexGrow: 1 }}>
                  <TreeMenu
                    menu={menu}
                    mode="horizontal"
                    className="header-menu bw0"
                    style={{ lineHeight: '36px', height: '37px' }}
                  />
                </div>

                {/* User Login */}
                <UserLogin className="tr pb2 pr4" user={user} userLoaded={userLoaded} />
                {/* <div className="pr4">
                  <Input.Search
                    placeholder="Search..."
                    className="navbar-search"
                    value={this.state.query}
                    onChange={e => this.setState({ query: e.target.value })}
                    onSearch={
                      onSearch === undefined
                        ? (value => Router.replace(`/search?q=${value}`))
                        : onSearch
                    }
                  />
                </div> */}
              </div>
            </div>
          </div>
        </Layout.Header>
      </div>
    );
  }
}

export default Header;
