import React from 'react';
import striptags from 'striptags';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Card, Icon } from 'antd';

import Link from '~/components/Link';

const ListItem = ({
  cover, title, url = '#', description = '', right, timestamp,
}) => {
  const content = striptags(description);

  return (
    <tr>
      <td className="tc pa3 br mr2">
        {timestamp || moment().format('DD MMM YYYY')}
      </td>
      <td className="v-btm">
        <div className="ml3 pa3 bb b--black-30 db">
          <Link className="b black-80" href={url}>{title}</Link>
          <p className="f6 overflow-hidden black-50 mb0">
            {content.length > 100 ? `${content.substr(0, 100)}...` : content}
          </p>
        </div>
      </td>
      <td className="tr v-btm">
        {right || (
          <div className="pa3 bb b--black-30 db">
            <Icon type="file-pdf" className="f3 ph2 dark-blue" />
            <Icon type="file-text" className="f3 ph2 dark-blue" />
            <Icon type="share-alt" className="dark-blue ph2" />
          </div>
        )}
      </td>
    </tr>
  );

  // return (
  //   <div className="fl w-33-ns pa2">
  //     <Link href={url}>
  //       <Card className="hover-bg-black-10">
  //         <div className="cf">
  //           <div className="fl w-40-ns img-container" style={{ height: '95px' }}>
  //             {cover}
  //           </div>
  //           <div className="fl w-60-ns pl3">
  //             <h5 className="f4 fw2 truncate" title={title}> {title} </h5>
  //             <p className="f6 overflow-hidden black-50" style={{ height: '55px' }}>
  //               {content.length > 100 ? `${content.substr(0, 100)}...` : content}
  //             </p>
  //           </div>
  //         </div>
  //       </Card>
  //     </Link>
  //   </div>
  // );
};

export default ListItem;
