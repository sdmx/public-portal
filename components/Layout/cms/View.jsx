import React from 'react';
import { Icon } from 'antd';
import moment from 'moment';

import Layout from '~/components/Layout/Public';

const ContentView = ({
  pageTitle, title, children, sider,
}) => (
  <Layout>
    <div className="ph5">
      <div className="f4 b mb3">{pageTitle}</div>
      <div className="f6">{moment().format('DD MMM YYYY')}</div>
      <h1 className="f3 mb4 mt0 b bg-primary">{title}</h1>

      <div className="cf bb pv1 mb4">
        <div className="fl"><b>Departemen Komunikasi</b></div>
        <div className="fr">
          <Icon type="mail" className="f3 ph1" />
          <Icon type="twitter" className="f3 ph1 light-blue" />
          <Icon type="facebook" className="f3 ph1 blue" />
          <Icon type="google-plus" className="f3 ph1 light-red" />
        </div>
      </div>

      <div className="cf">
        <div className="fl w-70" style={{ textAlign: 'justify' }}>
          {children}
        </div>
        <div className="fl w-30 pl4">
          {sider}
        </div>
      </div>

      <div className="cf mt4 tr">
        <Icon type="printer" className="f3" />
      </div>
    </div>
  </Layout>
);

export default ContentView;
