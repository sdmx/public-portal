import React from 'react';

import Layout from '~/components/Layout/Public';

const ContentList = ({ title, children }) => (
  <Layout>
    <div className="cf">
      {/* <h1 className="fw2 ma0 mt2">{title}</h1><hr /> */}
      {children}
    </div>
  </Layout>
);

export default ContentList;
