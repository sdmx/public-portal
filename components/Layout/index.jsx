import React from 'react';
import { Layout as AntLayout, Icon } from 'antd';

import 'antd/dist/antd.min.css';
import 'react-table/react-table.css';

import '~/public/static/css/tachyons.min.css';
import '~/public/static/css/override.css';

import Header from './Header';
import Footer from './Footer';

const { Content, Sider } = AntLayout;

class Layout extends React.Component {
  constructor(props) {
    super(props);
    const { collapseLeft = true, collapseRight = false } = props;

    this.state = {
      collapsedLeft: collapseLeft,
      collapsedRight: collapseRight,
      scrollOffset: 0,
    };

    this.toggleCollapsedLeft = this.toggleCollapsedLeft.bind(this);
    this.toggleCollapsedRight = this.toggleCollapsedRight.bind(this);
    // this.handleScroll = this.handleScroll.bind(this);
    // this.handleScroll = debounce(this.handleScroll, 500);
  }

  // componentDidMount() {
  //   window.addEventListener('scroll', this.handleScroll);
  // }

  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.handleScroll);
  // }

  handleScroll = () => {
    // this.setState({ scrollOffset: window.scrollY });
  }

  toggleCollapsedLeft() {
    this.setState({ collapsedLeft: !this.state.collapsedLeft });
  }

  toggleCollapsedRight() {
    this.setState({ collapsedRight: !this.state.collapsedRight });
  }

  render() {
    const { children, left, top, right, router, onSearch, sidebarWidth = 230 } = this.props;
    const { scrollOffset, collapsedLeft, collapsedRight } = this.state;

    return (
      <AntLayout className="bg-white" style={{ minHeight: '100vh' }}>
        <Header
          menu={top}
          collapsed={collapsedLeft}
          router={router}
          onSearch={onSearch}
        />

        <AntLayout>
          {left && (
            <Sider
              collapsible
              collapsed={collapsedLeft}
              trigger={null}
              width={sidebarWidth}
              className="br b--moon-gray bg-white relative"
            >
              <Icon
                className="absolute z-5 pointer pv4 ph1 dark-blue hover-navy bg-white bt bb br b--moon-gray"
                type={!collapsedLeft ? 'left' : 'right'}
                onClick={this.toggleCollapsedLeft}
                style={{ left: collapsedLeft ? 0 : '100%', top: `calc(20% + ${scrollOffset}px)` }}
              />

              <Content className="h-100">
                {left}
              </Content>
            </Sider>
          )}

          <AntLayout
            className="pt0 pa4 pb5 bg-white"
            style={{
              overflow: 'visible',
              width: `calc(100% - ${sidebarWidth * ((left && !collapsedLeft) + (right && !collapsedRight))}px)`,
            }}
          >
            <Content style={{ minHeight: 600 }}>
              {children}
            </Content>
          </AntLayout>

          {right && (
            <Sider
              collapsible
              collapsed={collapsedRight}
              trigger={null}
              width={sidebarWidth}
              className="bl b--moon-gray bg-white relative"
            >
              <Icon
                className="absolute z-5 pointer pv4 ph1 dark-blue hover-navy bg-white bt bb bl b--moon-gray"
                type={!collapsedRight ? 'right' : 'left'}
                onClick={this.toggleCollapsedRight}
                style={{ right: collapsedRight ? 0 : '100%', top: `calc(20% + ${scrollOffset}px)` }}
              />
              <Content className="h-100">
                {right}
              </Content>
            </Sider>
          )}
        </AntLayout>

        <Footer />
      </AntLayout>
    );
  }
}

export default Layout;
