import React from 'react';
import moment from 'moment';
import { Icon } from 'antd';

import Link from '~/components/Link';
import app from '~/config/app.json';

class Footer extends React.Component {
  render() {
    return (
      <div
        className="white pa4 z-999 bg-primary cover bg-center"
        style={{ backgroundImage: 'url(/static/img/bg-footer.jpg)' }}
      >
        {/* <div className="f4 ma0 white">
          {app.company.name}<sup>&reg;</sup>
        </div> */}
        <div className="flex pv2 mb4">
          <div className="mr4 w-80">
            <p className="mb2 f3 white">{app.company.name}</p>
            {/* <p className="white">{app.company.description}</p> */}
            <p className="mb2 f7">{app.company.address}</p>
            <p className="mb2 f7">{app.company.contact}</p>
            <p className="mb2 f7">{app.company.email}</p>
          </div>

          {/*<div className="ph4 w-20">
            <div className="white mb3">PRANALA</div>
            <ul className="list pl0">
              <li><a className="white f7" href="https://www.bappenas.go.id/id/">Bappenas</a></li>
              <li><a className="white f7" href="https://www.bi.go.id">Bank Indonesia</a></li>
              <li><a className="white f7" href="https://www.kemenkeu.go.id">Kementrian Keuangan</a></li>
              <li><a className="white f7" href="https://www.big.go.id">Badan Informasi Geospatial</a></li>
            </ul>
          </div>*/}

          {/*<div className="ph4 w-40">
            <div className="white mb3">SOENDA DSS</div>
            <div className="flex">
              <div className="w-50">
                <ul className="list pl0">
                  {Object.keys(app.links).map(name => (
                    <li key={name}>
                      <a className="white f7" href={app.links[name].url}>
                        {app.links[name].name}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
              <div className="w-50">
                <ul className="list pl0">
                  {app.other_links.map(item => (
                    <li key={item.name}>
                      <a className="white f7" href={item.url}>
                        {item.name}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>*/}

          <div className="ph4 w-20">
            <div className="white mb3">Follow Us</div>
            <div className="flex mb2">
              <a href={app.company.social.facebook} className="white pr3 f4" target="_blank">
                <Icon type="facebook" />
              </a>
              <a href={app.company.social.twitter} className="white pr3 f4" target="_blank">
                <Icon type="twitter" />
              </a>
              <a href={app.company.social.youtube} className="white pr3 f4" target="_blank">
                <Icon type="youtube" />
              </a>
              <a href={app.company.social.instagram} className="white pr3 f4" target="_blank">
                <Icon type="instagram" />
              </a>
            </div>
            <div className="flex">
              <a href={app.company.social.playstore} target="_blank">
                <img className="mv1 pr1" src="/static/img/icon/playstore.png" width="100px" alt="" />
              </a>
              <a href={app.company.social.appstore} target="_blank">
                <img className="mv1 pr1" src="/static/img/icon/appstore.png" width="100px" alt="" />
              </a>
            </div>
          </div>
        </div>

        <hr style={{ margin: '0 -32px' }} />
        <div className="flex pt1 pb3 f7">
          <div className="w-50">
            copyright &copy; {moment().format('YYYY')} Bank Indonesia All Rights Reserved
          </div>
          <div className="w-50 flex justify-end">
            <Link href="/" className="white f7 ph3"> Legal Notice </Link>
            <Link href="/" className="white f7 ph3"> Security </Link>
            <Link href="/" className="white f7 ph3"> Privacy Policy </Link>
            <Link href="/" className="white f7 ph3"> Information </Link>
            <Link href="/" className="white f7 ph3"> Services </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
