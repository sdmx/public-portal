import React from 'react';
import PropType from 'prop-types';

import Loading from '~/components/Loading';
import app from '~/config/app.json';

import Layout from './index';

class PublicLayout extends React.Component {
  render() {
    const { children, right, isLoading, ...props } = this.props;

    const headMenu = [
      {
        label: 'Data',
        children: [
          { url: '/data/internal', label: 'Bank Indonesia' },
          {
            label: 'Local',
            children: [
              { url: '/data/bps', label: 'Badan Pusat Statistik' },
            ],
          },
          {
            label: 'Others',
            children: app.dataSource.sdmx.map(({ id, label }) => ({ url: `/data/sdmx/${id}`, label })),
          },
        ],
      },
      { url: '/content/publication', label: 'Publication' },
      { url: '/content/news', label: 'News' },
    ];

    return (
      <Layout top={headMenu} right={right} {...props}>
        <Loading isFinish={!isLoading}>
          {children}
        </Loading>
      </Layout>
    );
  }
}

PublicLayout.propTypes = {
  children: PropType.node.isRequired,
};

export default PublicLayout;
