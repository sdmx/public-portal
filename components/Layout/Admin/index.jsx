import React from 'react';
import PropTypes from 'prop-types';
import Layout from '~/components/Layout';

// import './layout.css';

class AdminLayout extends React.Component {
  render() {
    const leftMenu = [
      { url: '/admin/user', icon: 'user', label: 'User' },
      { url: '/admin/role', icon: 'idcard', label: 'Role' },
      { url: '/admin/category', icon: 'appstore', label: 'Category' },
      { url: '/admin/news', icon: 'exception', label: 'News' },
      { url: '/admin/publication', icon: 'solution', label: 'Publication' },
    ];

    return (
      <Layout left={leftMenu}>
        {this.props.children}
      </Layout>
    );
  }
}

export default AdminLayout;
