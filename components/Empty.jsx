import React from 'react';
import { Icon } from 'antd';

const EmptyDisplay = ({ message = 'Empty', icon = 'exclamation-circle' }) => (
  <div className="tc f2 pa5 silver">
    {icon && <Icon type={icon} className="f2" />} {message}
  </div>
);

class Empty extends React.Component {
  render() {
    const { isEmpty = false, display, children, message, icon, ...attrs } = this.props;
    const displayEmpty = display ? display : <EmptyDisplay message={message} icon={icon} />;

    return (
      <div {...attrs}>
        {isEmpty ? displayEmpty : children}
      </div>
    );
  }
}

export default Empty;
