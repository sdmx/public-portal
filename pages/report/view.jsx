import React from 'react';
import moment from 'moment';
import { message } from 'antd';

import Layout from '~/components/Layout/cms/List';
import Loading from '~/components/Loading';
import { Report } from '~/utils/api';
import page from '~/page';

class ContentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      label: '',
      description: '',
      created: '',
    };

    this.fetchDetail = this.fetchDetail.bind(this);
  }

  componentDidMount() {
    this.fetchDetail();
  }

  fetchDetail() {
    const { router } = this.props;

    Report.get(router.query.path)
      .then(({ data }) => {
        this.setState({ ...data, loading: false });
      })
      .catch((err) => {
        this.setState({ loading: false });
        message.error(err.message);
      });
  }

  render() {
    const { router } = this.props;
    const { label, description, created, loading } = this.state;

    return (
      <Layout title="Report">
        <Loading isFinish={!loading}>
          <div className="f2 primary b">{label}</div>

          <div className="cf mb3">
            <div className="fl w-70">
              <div className="gray">{description}</div>
            </div>
            <div className="fl w-30 tr vertical-bottom">
              <span className="gray i f6">{moment(created, 'x').format('DD MMM YYYY')}</span>
            </div>
          </div>

          <div className="ba b--moon-gray">
            <iframe
              title={router.query.path}
              src={Report.exportUrl('html', `/${router.query.path}`)}
              width="100%"
              height="550px"
              frameBorder="0"
              allowTransparency="true"
            />
          </div>
        </Loading>
      </Layout>
    );
  }
}

export default page(ContentList);
