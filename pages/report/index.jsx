import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Input, message } from 'antd';
import moment from 'moment';

import Layout from '~/components/Layout/cms/List';
import Link from '~/components/Link';
import Loading from '~/components/Loading';
import ListItem from '~/components/Layout/cms/ListItem';
import { Report } from '~/utils/api';
import page from '~/page';

class ContentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
      data: [],
      totalPages: 1,
      count: 0,
      pageNum: 1,
      loading: true,
    };

    this.fetchList = this.fetchList.bind(this);
  }

  componentDidMount() {
    this.fetchList();
  }

  async fetchList(pageNum = 1, query = '') {
    const { pageSize } = this.props;
    const count = 20;

    this.setState({ loading: true });

    Report.list(query, pageSize, (pageNum - 1) * pageSize)
      .then(({ data }) => {
        this.setState({
          pageNum,
          data,
          count,
          totalPages: Math.ceil(count / pageSize),
          loading: false,
        });
      })
      .catch((err) => {
        this.setState({ loading: false });
        message.error(err.message);
      });
  }

  render() {
    const { data, totalPages, pageNum, count, loading } = this.state;
    const { pageSize } = this.props;

    return (
      <Layout title="Report">
        <Loading isFinish={!loading}>
          {data.length > 0 ? (
            <div>
              <div className="cf mt3 overflow-auto">
                <table className="w-100 center" cellSpacing="0">
                  <colgroup>
                    <col width="120px" />
                  </colgroup>
                  <thead>
                    <tr className="bg-primary">
                      <th className="pa2 tc white">Created</th>
                      <th className="pa2 tc white">Name</th>
                      <th className="pa2 tr">
                        <Input.Search
                          placeholder="Search..."
                          className="navbar-search bg-light-gray white"
                          value={this.state.searchQuery}
                          onChange={e => this.setState({ searchQuery: e.target.value })}
                          onSearch={query => this.fetchList(1, query)}
                        />
                      </th>
                    </tr>
                  </thead>
                  <tbody className="lh-copy">
                    {data.map(item => (
                      <ListItem
                        key={item.name}
                        timestamp={moment(item.created, 'x').format('DD MMM YYYY')}
                        title={item.name}
                        url={`/report/view${item.path}`}
                        description={item.description}
                        right={(
                          <div className="pa3 bb b--black-30 db">
                            <Link href={Report.exportUrl('pdf', item.path)}>
                              <Icon type="file-pdf" className="f3 ph2 dark-blue" />
                            </Link>
                            <Link href={Report.exportUrl('xls', item.path)}>
                              <Icon type="file-excel" className="f3 ph2 dark-blue" />
                            </Link>
                          </div>
                        )}
                      />
                    ))}
                  </tbody>
                </table>
              </div>

              <br />
              <div className="tc">
                {/* totalPages > 1 && (
                  <Pagination
                    total={count}
                    current={pageNum}
                    pageSize={pageSize}
                    onChange={(pageNum) => this.fetchList(pageNum)}
                  />
                ) */}
              </div>
            </div>
          ) : (
            <div className="tc f2 pa5 silver">
              <Icon type="inbox" className="f2" /> Empty
            </div>
          )}
        </Loading>
      </Layout>
    );
  }
}

ContentList.propTypes = {
  pageSize: PropTypes.number,
};

ContentList.defaultProps = {
  pageSize: 9,
};

export default page(ContentList);
