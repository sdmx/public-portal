import React from 'react';
import Error from 'next/error';

export default class Page extends React.Component {
  static async getInitialProps({ req, res }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    return (
      <div>
        <h1>Error</h1>
        <Error statusCode={this.props.statusCode} />
      </div>
    );
  }
}
