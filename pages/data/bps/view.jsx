import React from 'react';
import axios from 'axios';
import Head from 'next/head';
import { message, Icon, Tabs } from 'antd';
import { bindActionCreators } from 'redux';

import page from '~/page';
import { Storage, DataService } from '~/utils/api';

import { getDimensions } from '~/store/data-service/dsd';
import { getSeries } from '~/store/data-service/dataset';

import Layout from '~/components/Layout/Public';
import Loading from '~/components/Loading';
import Filter from '~/components/DataService/Filter';
import DataView from '~/components/DataService/DataView';
import Category from '~/components/DataService/Category';

const TABS = {
  DATA: 'DATA',
  FILTER: 'FILTER',
};

const srcProvider = 'api:bps';
const protocol = 'sdmx';

class DataServiceBuild extends React.Component {
  static getInitialProps = ({ query: { id } }) => ({ id })

  constructor(props) {
    super(props);

    switch (props.type) {
      case 'build':
        this.dataIds = Storage.get(props.id, '').split('|');
        break;

      default:
      case 'view':
        this.dataIds = `${srcProvider}/${props.id}`;
        break;
    }

    this.state = {
      selectedTab: TABS.FILTER,
      dataflow: {},
    };
  }

  componentDidMount() {
    const { id, dsd: { error } } = this.props;

    this.props.getDimensions(this.dataIds).then(() => {
      if (error) {
        message.error(error.message);
      }
    });

    // get dataflow title
    axios.get(`${DataService.endpoint}/data/api/api:bps/dataflow/${id}`).then(({ data }) => {
      this.setState({ dataflow: data });
    });
  }

  render() {
    const { dataflow } = this.state;
    const { dsd, dataset: { error, filterParams = {}, isFinished, series, id } } = this.props;

    if (error) {
      message.error(error.message);
    }

    return (
      <Layout title={dataflow.name}>
        <Head>
          <title>{dataflow.name}</title>
          <link href="/static/css/leaflet.css" rel="stylesheet" />
          <link href="/static/css/pivottable.css" rel="stylesheet" />
        </Head>

        <Loading isFinish={dsd.isFinished && isFinished}>
          {/* Data Title */}
          <div className="flex justify-between mb1">
            <span className="primary f3 b db">{dataflow.name}</span>
          </div>

          <Tabs
            activeKey={this.state.selectedTab}
            onTabClick={key => this.setState({ selectedTab: key })}
          >
            {/* Filter by Dimensions and Time Period */}
            <Tabs.TabPane key={TABS.FILTER} tab={<div><Icon type="filter" /> Filter</div>}>
              <Filter
                filterKeys={filterParams.filter}
                startPeriod={filterParams.startPeriod}
                endPeriod={filterParams.endPeriod}
                dimensions={dsd.dimensions}
                onSubmit={({ filterKeys, startPeriod, endPeriod }) => {
                  this.props.getSeries(this.dataIds, filterKeys, startPeriod, endPeriod)
                    .then(({ payload }) => {
                      const emptyResult = payload.length === 0;

                      if (!emptyResult && !error) {
                        this.setState({ selectedTab: TABS.DATA });
                      }

                      if (emptyResult) {
                        message.error('Could not find any results');
                      }
                    });
                }}
              />
            </Tabs.TabPane>

            {/* Pivot Table, Charts & Map */}
            <Tabs.TabPane key={TABS.DATA} tab={<div><Icon type="bars" /> Data</div>}>
              <DataView
                protocol={protocol}
                provider={srcProvider}
                dataId={this.dataIds}
                title={dataflow.name}
                dimensions={dsd.dimensions}
                series={series}
                filter={filterParams}
              />
            </Tabs.TabPane>
          </Tabs>
        </Loading>
      </Layout>
    );
  }
}

const mapStateToProps = ({ dsd, dataset }) => ({ dsd, dataset });

const mapDispatchToProps = dispatch => bindActionCreators({ getDimensions, getSeries }, dispatch);

export default page(DataServiceBuild, mapStateToProps, mapDispatchToProps);
