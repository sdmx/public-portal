import React from 'react';
import Head from 'next/head';
import { message, Icon, Menu, Dropdown, Modal } from 'antd';
import { withRouter } from 'next/router';
import nextRedux from 'next-redux-wrapper';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import _ from 'lodash';

import Layout from '~/components/Layout/Public';
import store from '~/store/fame';
import { getDataset } from '~/store/fame/dataset';
import Pivot from '~/components/DataService/Fame/Pivot';
import Chart from '~/components/DataService/Fame/Chart';
import DataMap from '~/components/DataService/Fame/DataMap';
import Filter from '~/components/DataService/Fame/Filter';
import Export from '~/components/DataService/Fame/Export';
import DataCategory from '~/components/DataService/Fame/DataCategory';
import Metadata from '~/components/DataService/Metadata';

const source = 'fame';
const dataflowId = 'EXIM_BI';

class Page extends React.Component {
  static getInitialProps({ query }) {
    return {
      id: query.id,
    };
  }

  constructor(props) {
    super(props);

    this.containerRef = React.createRef();
    this.defaultPrefer = {
      xAxis: 'TIME_PERIOD',
      yAxis: 'GEO',
      obsVal: 'OBS_VALUE',
    };
    this.state = {
      loading: true,
      showFilter: false,
      view: null,
    };
  }

  componentDidMount() {
    const { id } = this.props;

    this.props.getDataset(source, id)
      .then(() => this.setState({ loading: false }))
      .catch(() => this.setState({ loading: false }));
  }

  getDefaultPreferDimension = (dimensions = [], prefer) => {
    const dimensionsRank = [];

    // rank dimensions
    dimensions.forEach((dimension) => {
      if (!dimension.timeDimension) {
        dimensionsRank.push({
          id: dimension.id,
          size: Object.keys(dimension.codeList.codes).length,
        });
      }
    });

    _.sortBy(dimensionsRank, ['size']);

    // build default prefer
    const pullDimension = (id) => {
      const preferDimension = _.find(dimensionsRank, { id });
      const dimension = preferDimension ? { id } : _.last(dimensionsRank);

      _.remove(dimensionsRank, item => item.id === dimension.id);

      return dimension ? dimension.id : id;
    };

    return {
      xAxis: pullDimension(prefer.xAxis),
      yAxis: pullDimension(prefer.yAxis),
      // obsVal: pullDimension(prefer.obsVal),
      obsVal: 'OBS_VALUE',
    };
  }

  setVisual(type) {
    this.setState({ view: this.state.view !== null && type === this.state.view ? null : type });
  }

  getVisual(series, dimensions = []) {
    if (this.state.view != null) {
      const split = this.state.view.split('/');

      switch (split[0]) {
        case 'map':
          return (
            <DataMap defaultPrefer={this.defaultPrefer} />
          );

        case 'chart':
          return (
            <div className="pa2 pr5 bg-white">
              <Chart
                key={split[1]}
                type={split[1]}
                series={series}
                dimensions={dimensions}
                {...this.defaultPrefer}
              />
            </div>
          );

        case 'links':
          return (
            <div className="pa3 pr5 bg-white" style={{ minHeight: '50px' }}>
              <b>Links: </b>
              <ul>
                {source === 'sdmx' && (
                  <li>
                    <a href={process.env.SDMX_REGISTRY_URL} target="_blank">
                      {process.env.SDMX_REGISTRY_URL}
                    </a>
                  </li>
                )}
                {source === 'fame' && (
                  <li>
                    <a href={process.env.FAME_REST_ENDPOINT} target="_blank">
                      {process.env.FAME_REST_ENDPOINT}
                    </a>
                  </li>
                )}
              </ul>
            </div>
          );

        default:
          return <div />;
      }
    }

    return null;
  }

  render() {
    const { router, data } = this.props;

    if (this.state.loading) {
      return (
        <Layout>
          <div className="ma6 tc gray f1">
            <Icon type="loading" className="f1" /> Loading
          </div>
        </Layout>
      );
    }
    else if (typeof data === 'undefined') {
      return (
        <Layout>
          <div className="ma6 tc gray f1">
            <Icon type="exclamation-circle" className="f1" /> Not Found
          </div>
        </Layout>
      );
    }

    const { startPeriod, endPeriod, keySeries, series, dimensions = [], title } = data;
    const topView = this.getVisual(series, dimensions);
    const visualTypes = [
      { type: 'chart/line', label: 'Line', icon: 'line-chart' },
      { type: 'chart/bar', label: 'Bar', icon: 'bar-chart' },
      { type: 'chart/scatter', label: 'Scatter', icon: 'dot-chart' },
      { type: 'chart/area', label: 'Area', icon: 'area-chart' },
      // { type: "map", label: "Map", icon: "environment" },
    ];

    const { _filter, showFilter } = this.state;
    const filter = _.size(_filter) > 0 ? _filter : { key: keySeries, startPeriod, endPeriod };

    // Default Prefer Dimensions
    // const defaultPrefer = this.getDefaultPreferDimension(dimensions, this.defaultPrefer);
    const dimensionsRank = [];

    // rank dimensions
    dimensions.forEach((dimension) => {
      if (!dimension.timeDimension) {
        dimensionsRank.push({
          id: dimension.id,
          size: Object.keys(dimension.codeList.codes).length,
        });
      }
    });

    if (dimensionsRank.length > 0) {
      _.sortBy(dimensionsRank, ['size']);
      this.defaultPrefer.yAxis = _.last(dimensionsRank).id;
    }

    return (
      <Layout
        title={title}
        left={<DataCategory source={source} />}
        right={<Metadata dataflowId={dataflowId} title={title} />}
      >
        <Head>
          <title>{title}</title>
          <link href="/static/css/leaflet.css" rel="stylesheet" />
          <link href="/static/css/pivottable.css" rel="stylesheet" />
        </Head>

        <div ref={this.containerRef} className="relative">
          <div className="flex justify-between mv2">
            <span className="primary f3 b db ph3">{title}</span>
          </div>

          {/* Header Menu */}
          <div className="flex bg-primary justify-between">
            <Menu mode="horizontal" theme="dark" className="bg-transparent w-80">
              <Menu.Item key="chart" style={{ border: 'none', background: 'none' }}>
                <Dropdown
                  overlay={(
                    <div className="bg-white ba b--black-10 br2 pv2 ph1 shadow-1 flex">
                      {visualTypes.map(visual => (
                        <span
                          key={visual.type}
                          className="pointer tc mh3 gray hover-dark-gray"
                          onClick={() => this.setVisual(visual.type)}
                        >
                          <Icon type={visual.icon} className="f1" /><br />
                          <span className="text-primary b"> {visual.label} </span>
                        </span>
                      ))}
                    </div>
                  )}
                >
                  <span className="white">
                    <Icon type="area-chart" style={{ fontSize: '14px' }} className="white" />
                    <b>Chart</b>
                    <Icon type="caret-down" style={{ fontSize: '14px' }} className="ml2" />
                  </span>
                </Dropdown>
              </Menu.Item>

              {_.find(dimensions, { id: 'GEO' }) && (
                <Menu.Item key="map" style={{ border: 'none', background: 'none' }}>
                  <span className="white b" onClick={() => this.setVisual('map')}>
                    <Icon type="environment" className="white" /> Map
                  </span>
                </Menu.Item>
              )}

              <Menu.Item key="export" style={{ border: 'none', background: 'none' }}>
                <Dropdown
                  overlay={(
                    <Export
                      dataId={router.query.id}
                      filter={filter}
                      source={source}
                    />
                  )}
                >
                  <div className="white">
                    <Icon type="export" style={{ fontSize: '14px' }} className="ml2" />
                    <b>Export</b>
                    <Icon type="caret-down" className="ml2" />
                  </div>
                </Dropdown>
              </Menu.Item>

              <Menu.Item
                key="links"
                style={{ border: 'none', background: 'none' }}
                onClick={() => this.setVisual('links')}
              >
                <div className="white">
                  <Icon type="link" style={{ fontSize: '14px' }} className="white" />
                  <b>Links</b>
                </div>
              </Menu.Item>
            </Menu>
          </div>

          {/* Chart View */}
          {topView && (
            <div className="relative">
              <div
                className="pointer absolute pa2 ma2"
                style={{ right: 0, top: 0, zIndex: 1 }}
                onClick={() => this.setVisual(null)}
              >
                <Icon type="close" className="f2 b" />
              </div>

              <div className="bg-moon-gray ph1">
                {topView}
              </div>
              <div className="bg-primary pa3" />
            </div>
          )}

          {/* Pivot Table */}
          <div style={{ background: '#d9dde8' }}>
            <Pivot
              filter={filter}
              getDataset={this.props.getDataset}
              router={router}
              defaultPrefer={this.defaultPrefer}
            />
          </div>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = props => ({ ...props.dataset });

const mapDispatchToProps = dispatch => ({
  getDataset: bindActionCreators(getDataset, dispatch),
});

export default nextRedux(store, mapStateToProps, mapDispatchToProps)(withRouter(Page));
