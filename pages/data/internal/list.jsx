import React from 'react';
import { Tabs } from 'antd';

import page from '~/page';
import Layout from '~/components/Layout/Public';
import FameDataflowList from '~/components/DataService/Fame/FameDataflowList';
import DataCategory from '~/components/DataService/Fame/DataCategory';

const srcProvider = 'fame:bi';
const source = 'fame';
const dataId = `${srcProvider}/EXIM_BI_1_1_M`;

class ContentList extends React.Component {
  state = {
    categoryKey: null,
  }

  filterByCategory = (key) => {
    this.setState({ categoryKey: key });
  }

  render() {
    const { categoryKey } = this.state;
    const title = 'Bank Indonesia';

    return (
      <Layout
        title={title}
        collapseLeft={false}
        left={<DataCategory source={source} onSelect={this.filterByCategory} />}
      >
        <h1 className="primary b mb0">{title}</h1>
        <hr />

        <div className="card-container">
          <FameDataflowList
            source={source}
            provider={srcProvider}
            dataId={dataId}
            category={categoryKey || ''}
          />
        </div>
      </Layout>
    );
  }
}

export default page(ContentList);
