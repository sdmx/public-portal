import React from 'react';
import Head from 'next/head';
import { message, Icon, Menu, Dropdown, Modal, Tabs } from 'antd';
import { bindActionCreators } from 'redux';
import moment from 'moment';

import page from '~/page';
import { Storage } from '~/utils/api';

import { getDimensions } from '~/store/data-service/dsd';
import { getSeries } from '~/store/data-service/dataset';

import Layout from '~/components/Layout/Public';
import Loading from '~/components/Loading';
import Empty from '~/components/Empty';
import Filter from '~/components/DataService/Filter';
import DataView from '~/components/DataService/DataView';
import Category from '~/components/DataService/Category';
import Metadata from './metadata';

const TABS = {
  DATA: 'DATA',
  FILTER: 'FILTER',
};

class DataServiceBuild extends React.Component {
  static getInitialProps = ({ query: { id, provider } }) => ({ id, provider, srcProvider: `sdmx:${provider}` })

  constructor(props) {
    super(props);
    this.dataIds = Storage.get(props.id, '').split('|');
    this.state = {
      selectedTab: TABS.FILTER,
    };
  }

  componentDidMount() {
    const { dsd: { error } } = this.props;

    this.props.getDimensions(this.dataIds).then(() => {
      if (error) {
        message.error(error.message);
      }
    });
  }

  render() {
    const title = 'Data';
    const { id, provider, dsd, dataset: { error, filterParams = {}, isFinished, series } } = this.props;

    if (error) {
      message.error(error.message);
    }

    const metadata = (
      <div className="pl3 h-100 overflow-y-auto overflow-x-hidden relative">
        <div className="absolute">
          <div dangerouslySetInnerHTML={{ __html: Metadata(title) }} />
        </div>
      </div>
    );

    return (
      <Layout title={title} left={<Category provider={provider} />} right={metadata}>
        <Head>
          <title>{title}</title>
          <link href="/static/css/leaflet.css" rel="stylesheet" />
          <link href="/static/css/pivottable.css" rel="stylesheet" />
        </Head>

        <Loading isFinish={dsd.isFinished && isFinished}>
          <Tabs
            activeKey={this.state.selectedTab}
            onTabClick={key => this.setState({ selectedTab: key })}
          >
            {/* Filter by Dimensions and Time Period */}
            <Tabs.TabPane key={TABS.FILTER} tab={<div><Icon type="filter" /> Filter</div>}>
              <Filter
                filterKeys={filterParams.filter}
                startPeriod={filterParams.startPeriod}
                endPeriod={filterParams.endPeriod}
                dimensions={dsd.dimensions}
                onSubmit={({ filterKeys, startPeriod, endPeriod }) => {
                  this.props.getSeries(this.dataIds, filterKeys, startPeriod, endPeriod).then((res) => {
                    const emptyResult = series.length === 0;

                    if (!emptyResult && !error) {
                      this.setState({ selectedTab: TABS.DATA });
                    }

                    if (emptyResult) {
                      message.error('Could not find any results');
                    }
                  });
                }}
              />
            </Tabs.TabPane>

            {/* Pivot Table, Charts & Map */}
            <Tabs.TabPane key={TABS.DATA} tab={<div><Icon type="bars" /> Data</div>}>
              <DataView
                provider={provider}
                title={title}
                dimensions={dsd.dimensions}
                series={series}
                filter={filterParams} />
            </Tabs.TabPane>
          </Tabs>
        </Loading>
      </Layout>
    );
  }
}

const mapStateToProps = ({ dsd, dataset }) => ({ dsd, dataset });

const mapDispatchToProps = dispatch => bindActionCreators({ getDimensions, getSeries }, dispatch);

export default page(DataServiceBuild, mapStateToProps, mapDispatchToProps);
