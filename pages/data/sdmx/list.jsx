import React from 'react';
import { bindActionCreators } from 'redux';
import { message, Pagination, Checkbox, Button } from 'antd';
import _ from 'lodash';

import page from '~/page';
import app from '~/config/app.json';
import Loading from '~/components/Loading';
import Link from '~/components/Link';
import Layout from '~/components/Layout/Public';
import ListTable from '~/components/DataService/ListTable';
import Category from '~/components/DataService/Category';

import {
  getDataflows,
  selectDataflow,
  unselectDataflow,
  buildDataset,
} from '~/store/data-service/dataflow';

class ContentList extends React.Component {
  static getInitialProps = ({ query: { provider } }) => ({
    provider, srcProvider: `sdmx:${provider}`,
  })

  componentDidMount() {
    const { srcProvider } = this.props;
    this.props.getDataflows(srcProvider);
  }

  render() {
    const {
      provider,
      srcProvider,
      isFinished,
      error,
      selectedDataIds,
      param,
      payload: { content, totalPages, totalElements, number: pageNum, size: pageSize },
    } = this.props;
    const title = _.find(app.dataSource.sdmx, { id: provider }).label;

    if (error) {
      message.error(error.message);
    }

    return (
      <Layout
        title={title}
        collapseLeft={false}
        left={<Category provider={provider} />}
      >
        <h1 className="primary b mb0">{title}</h1>
        <hr />

        <div className="card-container">
          {/* <DataflowList source={provider} category={''} /> */}
          <Loading isFinish={isFinished}>
            {selectedDataIds.length > 0 && (
              <div className="cf tr f7 pv1">
                <i className="pr2">{selectedDataIds.length} dataflows are selected.</i>
                <Button
                  type="primary"
                  onClick={() => this.props.buildDataset(provider, selectedDataIds)}
                >
                    Go
                </Button>
              </div>
            )}

            {/* Dataflow List Table */}
            <ListTable
              onSearch={(query) => this.props.getDataflows(srcProvider, query)}
              content={content}
              columns={[
                {
                  border: false,
                  content: ({ id }) => (
                    <div className="tc pv3">
                      <Checkbox
                        onChange={({ target }) => {
                          if (target.checked) {
                            this.props.selectDataflow(`${srcProvider}/${id}`)
                          }
                          else {
                            this.props.unselectDataflow(`${srcProvider}/${id}`)
                          }
                        }}
                      />
                    </div>
                  ),
                },
                {
                  label: 'Code',
                  border: false,
                  content: ({ id }) => (
                    <div className="tc">{id}</div>
                  ),
                },
                {
                  label: 'Name',
                  content: ({ id, name }) => (
                    <Link className="b black-80" href={`/data/sdmx/${provider}/view/${id}`}>
                      {name}
                    </Link>
                  ),
                },
              ]}
            />

            <br />
            <div className="tc">
              {totalPages > 1 && (
                <Pagination
                  total={totalElements}
                  current={pageNum + 1}
                  pageSize={pageSize}
                  onChange={pageNum => this.props.getDataflows(srcProvider, param.query, pageNum)}
                />
              )}
            </div>
          </Loading>
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = ({ dataflow }) => dataflow;

const mapDispatchToProps = dispatch => bindActionCreators({
  getDataflows,
  selectDataflow,
  unselectDataflow,
  buildDataset,
}, dispatch);

export default page(ContentList, mapStateToProps, mapDispatchToProps);
