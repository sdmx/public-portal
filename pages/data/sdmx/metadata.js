export default title => (`${'<div id="html-preview"> \n' +
  '   <div class="Section1"> \n' +
  '    <h1>'}${title || 'Economic accounts for agriculture'}</h1>` +
  '    <p style="line-height:150%" class="Heading">Reference Metadata in Euro SDMX Metadata Structure (ESMS)</p> <p class="Heading"> Compiling agency: Eurostat, the statistical office of the European Union.</p>' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td valign="top" width="45%"> \n' +
  '        <table width="80%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td style="text-align:center;" class="Cl-Level1">Eurostat metadata</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td align="center" style="border-bottom:none" class="Cl-Content"><b>Reference metadata</b></td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td style="border-top:none" class="Cl-Content"><a href="#contact1523958106829">1. Contact</a> <br /> <a href="#meta_update1523958106829">2. Metadata update</a> <br /> <a href="#stat_pres1523958106829">3. Statistical presentation</a> <br /> <a href="#unit_measure1523958106829">4. Unit of measure</a> <br /> <a href="#ref_period1523958106829">5. Reference Period</a> <br /> <a href="#inst_mandate1523958106829">6. Institutional Mandate</a> <br /> <a href="#conf1523958106829">7. Confidentiality</a> <br /> <a href="#rel_policy1523958106829">8. Release policy</a> <br /> <a href="#freq_diss1523958106829">9. Frequency of dissemination</a> <br /> <a href="#accessibility_clarity1523958106829">10. Accessibility and clarity</a> <br /> <a href="#quality_mgmnt1523958106829">11. Quality management</a> <br /> <a href="#relevance1523958106829">12. Relevance</a> <br /> <a href="#accuracy1523958106829">13. Accuracy</a> <br /> <a href="#timeliness_punct1523958106829">14. Timeliness and punctuality</a> <br /> <a href="#coher_compar1523958106829">15. Coherence and comparability</a> <br /> <a href="#cost_burden1523958106829">16. Cost and Burden</a> <br /> <a href="#data_rev1523958106829">17. Data revision</a> <br /> <a href="#stat_process1523958106829">18. Statistical processing</a> <br /> <a href="#comment_dset1523958106829">19. Comment</a> <br /> <a href="#relatedmd1523958106829">Related Metadata</a> <br /> <a href="#annex1523958106829">Annexes</a> (including footnotes) </td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td>\n' +
  '       <td width="10%"></td>\n' +
  '       <td valign="top" width="45%"></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" class="DocCommon" cellpadding="0" cellspacing="0" border="0"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td> <br />For any question on data and metadata, please contact: <a target="_blank" href="http://ec.europa.eu/eurostat/help/support">EUROPEAN STATISTICAL DATA SUPPORT</a></td>\n' +
  '       <td width="2%"></td>\n' +
  '       <td valign="bottom" align="right"> <p style="text-align:right;margin-right:20px;" class="DocCommon"> <a href="http://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=metadata/aact_esms.sdmx.zip">Download </a> <br /> </p> </td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="contact"></a><a name="contact1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">1. Contact</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td width="206" class="Cl-Level2"><a name="contact_organisation"></a>1.1. Contact organisation</td>\n' +
  '       <td class="Cl-Content"><p>Eurostat, the statistical office of the European Union.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="organisation_unit"></a>1.2. Contact organisation unit</td>\n' +
  '       <td class="Cl-Content"><p>E1: Agriculture and fisheries</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="contact_mail"></a>1.5. Contact mail address</td>\n' +
  '       <td class="Cl-Content"><p>Postal address: Rue Alcide de Gasperi L-2920 Luxembourg LUXEMBOURG</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="meta_update"></a><a name="meta_update1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">2. Metadata update</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td width="206" class="Cl-Level2"><a name="meta_certified"></a>2.1. Metadata last certified</td>\n' +
  '       <td class="Cl-Content">28/03/2018</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="meta_posted"></a>2.2. Metadata last posted</td>\n' +
  '       <td class="Cl-Content">28/03/2018</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="meta_last_update"></a>2.3. Metadata last update</td>\n' +
  '       <td class="Cl-Content">28/03/2018</td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="stat_pres"></a><a name="stat_pres1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">3. Statistical presentation</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="data_descr"></a>3.1. Data description</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;">The Economic Accounts for Agriculture (EAA) provide detailed information on income in the agricultural sector. The purpose is to analyse the production process of the agricultural industry and the primary income generated by this production. The accounts are therefore based on the industry concept.</p> <p>The EAA are detailed data on the value of output (measured in both producer prices and basic prices), intermediate consumption, subsidies and taxes, consumption of fixed capital, rent and interest, capital formation etc. The values are available in both current prices and constant prices.</p> <p>Agricultural Labour Input (ALI) statistics and Unit Values (UV) are an integrated part of the overall concept of the EAA.</p> <p style="text-align: justify;">The EAA are a satellite account of the European System of Accounts (ESA), providing complementary information and concepts adapted to the particular nature of the agricultural industry. Although their structure very closely matches that of the national accounts, their compilation requires the formulation of appropriate rules and methods.</p> <p>National Statistical Institutes or Ministries of Agriculture are responsible for data collection and calculations of national EAA, in accordance with EC Regulations. Eurostat is responsible for the EU aggregations.</p> <p><strong>Regional data</strong></p> <p>The EAA are also compiled at regional level (NUTS2), but only in values at current prices. The agricultural labour input data and unit values, however, are not available at regional levels.</p> <p>Please note that for paragraphs where no metadata for regional data has been specified, the regional metadata is identical to the metadata provided for national data.</p> <p><strong>Frequency of data collection for data under Regulation (EC) 138/2004 and gentlemen\'s agreement</strong></p> \n' +
  '        <table border="1" cellspacing="0" cellpadding="3"> \n' +
  '         <tbody> \n' +
  '          <tr> \n' +
  '           <td> </td> \n' +
  '           <td style="text-align: center;">Transmission date via eDamis</td> \n' +
  '           <td style="text-align: center;">Reg. CE 138/2004</td> \n' +
  '           <td style="text-align: center;">Gentlemen\'s agreement</td> \n' +
  '           <td style="text-align: center;">Web Form in eDamis</td> \n' +
  '           <td style="text-align: center;">Excel SDTT file in <a href="https://circabc.europa.eu/w/browse/a04805b6-8651-48f9-b40d-7b5c8c2e9aab">CIRCABC</a></td> \n' +
  '           <td style="text-align: center;">eDamis DATASET to use</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>EAA previous year prices Final - N-1</td> \n' +
  '           <td style="text-align: center;" rowspan="5">30 September year N</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td>COSAEA_AGR3CON_A</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>EAA current prices Final - N-1</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td>COSAEA_AGR3CUR_A</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>UV (unit values) N-1</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td>COSAEA_UV_A</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>EAA Regional data N-2</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td>COSAEA_REGION_A</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>ALI (Labour Input) final N-1</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td>COSAEA_ALI3_A</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>EAA First Estimates N</td> \n' +
  '           <td style="text-align: center;">30 November year N</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td>COSAEA_AGR1_A</td> \n' +
  '          </tr> \n' +
  '          <tr> \n' +
  '           <td>EAA Second Estimates N</td> \n' +
  '           <td style="text-align: center;">31 January year N+1</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">-</td> \n' +
  '           <td style="text-align: center;">X</td> \n' +
  '           <td>COSAEA_AGR2_A</td> \n' +
  '          </tr> \n' +
  '         </tbody> \n' +
  '        </table></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="class_system"></a>3.2. Classification system</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The EAA are an integral part of the ESA. As such, they are compiled based on NACE REV.2, the statistical classification of economic activities in the European Community. The list of activities which defines the agricultural industry corresponds, in principle, to Division 01 of that classification: Crop and animal production, hunting and related service activities. The list of products collected is enclosed in annex.</p> <p>The data are collected for BE, BG, CZ, DK, DE, EE, IE, EL, ES, FR, HR, IT, CY, LV, LT, LU, HU, MT, NL, AT, PL, PT, RO, SI, SK, FI, SE, UK and CH, IS and NO</p> <p><strong>Regional data</strong></p> <p>The territorial classification of regional data is broken down according to the <a title="NUTS classification" href="http://ec.europa.eu/eurostat/c/portal/layout?p_l_id=629283&p_v_l_s_g_id=0" target="_blank">NUTS classification</a>.</p> <p>For most countries the regional data is available at NUTS 2 level.</p><br /> <br /> <span class="annexesHead">Annexes:</span> <br /> <a title="aact_esms_an1.xls" href="../Annexes/aact_esms_an1.xls" target="_blank">List of products collected for Economic Accounts for Agriculture</a> <br /> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="coverage_sector"></a>3.3. Coverage - sector</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The sector covers the income of all units involved in agricultural production, even if some of those units have more important economic activities or if the purpose of some others is not commercial. Note, however, that kitchen gardens (producing for own-consumption only) are not included.</p> <p>Division 01 of NACE Rev. 2 comprises:</p> <p>•Group 01.1: Growing of non-perennial crops;</p> <p>•Group 01.2: Growing of perennial crops;</p> <p>•Group 01.3: Plant propagation;</p> <p>•Group 01.4: Animal production;</p> <p>•Group 01.5: Mixed farming;</p> <p>•Group 01.6: Support activities to agriculture and post-harvest crop activities;</p> <p>•Group 01.7: Hunting, trapping and related service activities.</p> <p>The list of agricultural activities characteristic of the EAA corresponds to these seven groups of activities (01.1to 01.7), but with the following caveats:</p> <p>- the inclusion of the production of wine and olive oil (exclusively using grapes and olives grown by the same holding),</p> <p>- the exclusion of certain activities which, in NACE Rev. 2, are considered as agricultural services (e.g. the operation of irrigation systems - only agricultural contract work is taken into account here).</p> <p>The agricultural industry of the EAA differs in some respects from the branch as defined for national accounts purposes. The differences relate to the definition of both characteristic activities and units. They can be summarised as follows:</p> <p>EAA agricultural industry =</p> <p>NA agricultural branch</p> <p>- Production units providing associated agricultural services other than agricultural contract work (i.e. the operation of irrigation systems)</p> <p>+ Agricultural activities of units whose principal activity is not agricultural</p> <p>The NACE-classification (rev2) has been implemented by September 2011 in the ESA and in EAA.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="stat_conc_def"></a>3.4. Statistical concepts and definitions</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;">A detailed methodology can be found in Annex I of the EAA Regulation and in the \'Manual on the Economic Accounts for Agriculture and Forestry EAA/EAF 97 (Rev.1.1)\'. A detailed methodology on agricultural labour input can be consulted in the \'Target methodology for agricultural labour input (ALI) statistics (Rev.1)\'. All these documents can be found in the annexes.</p> <p>When differences between the Manuals and the Annex in the Regulation are found, pre-eminence should be given to the Annex.</p> <p>The EAA are satellite accounts of the <a title="European System of Accounts" href="http://ec.europa.eu/eurostat/statistics-explained/index.php/European_system_of_national_and_regional_accounts_-_ESA_2010" target="_blank">European System of Accounts</a> (ESA) providing complementary information and concepts adapted to the particular nature of the agricultural industry.</p> <p>The EAA are shown as a sequence of inter-related accounts. As the EAA are based on the industry concept, the sequence of accounts in accordance with ESA has to be limited to the first accounts of the current account:</p> <p>- the <strong>production account and</strong></p> <p>- the generation-of-income account</p> <p>whose balancing items are value added and operating surplus, respectively.</p> <p>Nevertheless, it is thought that, given the specific features of agriculture, it should be possible to compile other accounts, at least in part, in so far as the relevant flows can be clearly attributed to them. The accounts in question are the following:</p> <p>- the <strong>entrepreneurial income account </strong>(one of the current accounts) and</p> <p>- the <strong>capital account </strong>(one of the accumulation accounts).</p> <p style="text-align: justify;">The EAA provide a wide range of indicators on the economic activities in the agricultural sector: these include output, intermediate consumption, gross and net value added, gross fixed capital formation (GFCF), both in current prices and in constant prices, as well as compensation of employees, other taxes and subsidies on production, net operating surplus or net mixed income, property income and net entrepreneurial income in current prices.</p> <p style="text-align: justify;">Estimates of the volume of agricultural labour provided during the calendar year, measured in Annual Work Units (AWUs), are also collected. A distinction is drawn between non-salaried and salaried AWUs, which together make up total AWUs. The total number of hours worked in agriculture represents the aggregate number of hours actually worked as an employee or a self-employed person during the calendar year, when the output of this work is included in the output of the agricultural industry. One AWU corresponds to the input, measured in working time, of one person who is engaged in agricultural activities on an agricultural unit on a full-time basis over an entire year.   The total labour input corresponds to the total hours worked divided by the average annual number of hours worked in full-time jobs within the economic territory. Full-time work should be based on the definition used in Eurostat\'s FSS and is typically 1800 hours per year, if no specific national provisions are detailed.</p> <p style="text-align: justify;">One person cannot represent more than one AWU. This constraint holds even if someone is working in the agricultural industry for more than the number of hours defining full time. The agricultural labour input of persons who work less than full time on agricultural holdings is calculated as the quotient of the number of hours actually worked (per week or per year) and the number of hours actually worked (per week or per year) in a full-time job. Total hours worked do not cover work for the private household of the holder or manager.</p> <p style="text-align: justify;">Unit values refer to the concept of the output of agricultural activity. They are obtained by dividing current values (in producer prices and in basic prices) by the corresponding physical quantities. They differ from prices in as much as the variation in unit values includes any variation in quality. The following equations illustrate these relationships:</p> <p>Unit value statistics:     unit value  =  (current value) / ( physical quantity)</p> <p>EAA in general:          "pure" price   =  (current value) / (volume)</p> <p>Three <strong>indicators of the economic performance</strong> of agriculture are defined in the EAA:</p> <p>•Indicator A: Index of the real income of factors in agricultural per annual work unit.            <br /> This yardstick corresponds to the real net value added at factor cost of agriculture per total AWU</p> <p>•Indicator B: Index of real net agricultural entrepreneurial income per unpaid annual work unit.           <br /> This indicator presents the changes in net entrepreneurial income over time, per non-salaried AWU.</p> <p>•Indicator C: Net entrepreneurial income of agriculture. <br /> This income aggregate is presented as an absolute value (or in the form of an index in real terms). It allows comparability over time of the income of the agricultural industry between Member States.</p> <p><strong>Regional data</strong></p> <p>The concept for compiling of EAA at regional level is described in Chapter 7 of the Manual and the definition of unit values is described in Chapter 6. The provision of these data is based on a gentleman\'s agreement and not on a legal basis.</p> <p>Not all national tables are necessarily available at regional level. To consult the list of available national- and regional- datasets, please consult the annex.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="stat_unit"></a>3.5. Statistical unit</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;">The overall unit is the agricultural sector. However, in order to provide more detailed information and to analyse flows generated by the production process and the use of goods and services, it is necessary to select units which emphasise relationships of a technical-economic kind. This requirement means that, as a rule, institutional units must be broken-down into smaller and more homogeneous units with regard to the kind of production. Local kind-of-activity units (local KAUs) are intended to meet this requirement as a first but practically oriented operational approach (ESA 2010, 2.147).</p> <p style="text-align: justify;">The local KAU is defined as the part of a KAU which corresponds to a local unit. The KAU groups all the parts of an institutional unit in its capacity as producer contributing to the performance of an activity at class level (four digits) in NACE Rev. 2 (the reference classification for economic activities) and corresponds to one or more operational subdivisions of the institutional unit. The institutional unit\'s information system must be capable of indicating or calculating for each local KAU at least the value of output, intermediate consumption, compensation of employees, the operating surplus and employment and gross fixed capital formation (ESA 2010, 2.148).</p> <p style="text-align: justify;">The <strong>agricultural holding</strong>, which is the unit currently used for statistical studies of agriculture (censuses, surveys of the structure of agricultural holdings), is the local KAU most appropriate to the agricultural industry (even though certain other units, such as wine or olive oil cooperatives, or units performing contract work, etc., have to be included in it). Nevertheless, it should be pointed out that the variety of agricultural activities that can be performed on agricultural holdings makes them a special type of local KAU. The strict application of the ESA rule to units and their group should in fact result in a division of the agricultural holding into several separate local KAUs in cases where several activities of the NACE Rev. 2 four-digit class are performed on the same holding. The adoption of the agricultural holding as the local KAU of the agricultural industry in the national accounts and EAA is based on a statistical approach.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="stat_pop"></a>3.6. Statistical population</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;">Although the ESA gives pre-eminence to local KAUs, the unit best suited to analyse the production process is the unit of homogeneous production (UHP). This unit is used to analyse inputs and outputs, since it corresponds exactly to a type of activity. Institutional units are thus divided into as many UHPs as there are activities (other than ancillary). By grouping these units of homogeneous production it is possible to break down the economy into \'pure\' (homogeneous) branches. A UHP cannot, as a rule, be directly observed. Therefore, the accounts of homogeneous branches cannot be compiled on the basis of groups of UHPs. The ESA describes a method for compiling these accounts. It involves attributing secondary production and the corresponding costs of activity branches to the appropriate homogeneous branches (ESA 2010, 9.52 to 9.63).</p> <p style="text-align: justify;">The use of the local KAU as the basic unit for the agricultural industry entails recording non-agricultural secondary activities where they cannot be distinguished from the main agricultural activity. Inseparable non-agricultural secondary activities of local agricultural KAUs are defined as activities closely linked to agricultural production for which information on any of output, intermediate consumption, compensation of employees, labour input or GFCF cannot be separated from information on the main agricultural activity during the period of statistical observation. Only that part of a specific non-agricultural secondary activity which is inseparable must be included. As a consequence, a given non-agricultural activity will be included in the agricultural industry if it is impossible to separate it from the main agricultural activity of a local KAU, but will be excluded if it can be separated from the main agricultural activity, in which case the secondary activity gives rise to a non-agricultural local KAU. The selection criterion for inseparable non-agricultural secondary activities is not so much the nature of the product as the type of activity. For example, agro-tourism services provided by a farm must only be included if they cannot be separated from its agricultural activities. This would probably not be the case when these activities become important. Thus, non-agricultural products accounted for in the production of the agricultural industry may vary geographically and over time.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="ref_area"></a>3.7. Reference area</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Data are collected for the Member States of the European Union: BE, BG, CZ, DK, DE, EE, IE, EL, ES, FR, HR, IT, CY, LV, LT, LU, HU, MT, NL, AT, PL, PT, RO, SI, SK, FI, SE, UK; as well as EFTA countries: CH, IS and NO; and Candidate countries: MK and TR.</p> <p>Eurostat publishes aggregates for EU-15, EU-25, EU-27 and EU-28 in Eurobase.</p> <p>The regional accounts for agriculture (RAA) are established at the NUTS 2 level, the most detailed level available for the countries mentioned.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="coverage_time"></a>3.8. Coverage - Time</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>For the EEA, data availability is generally as described here:</p> <p>- EU-15 data are available for the year 1993 onwards</p> <p>- EU-27 data are available for a number of years as from 1998.</p> <p>- EU-28 data are available for the year 2005 onwards</p> <p>- EFTA country data availability: Switzerland (for 1985 onwards), Iceland (for 2007 onwards), Norway (for 1995 onwards).</p> <p>- Member State data: for some countries data go back as far as 1973 (Denmark, France, Sweden and United Kingdom)</p> <p>For most countries, regional data are available for fewer years (often starting in 1980) than the series for the whole country, with the exception of Denmark (also available for 1973 onwards).</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="base_per"></a>3.9. Base period</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Accounts for year n are calculated at current prices, at prices of year n-1 and at prices of year 2010 and 2005. Value, volume and price indices are also obtained for reference years: n-1=100, 2010=100 and 2005=100.</p> <p>Labour input data are shown as total Annual Work Units and as indices 2010=100 and 2005=100.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="unit_measure"></a><a name="unit_measure1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">4. Unit of measure</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Accounts data are given in:</p> <p>•Millions of euro (from 1.1.1999)/Millions of ECU (up to 31.12.1998),</p> <p>•Millions of national currency (including \'euro fixed\' series for euro area countries)</p> <p>•MIO_PPS Millions of PPS (Purchasing Power Standard)</p> <p>Furthermore, the core data and the indicators are expressed using indices. The reference years are 2010=100, 2005 = 100 and n-1 = 100, respectively</p> <p>Agricultural labour input data are given in</p> <p>•Thousands of annual work units (AWU)</p> <p>•Indices related to 2010=100 and 2005=100</p> <p>Unit value statistics are given in</p> <p><span lang="FR-BE">•1000 tonnes (Quantities) </span></p> <p><span lang="FR-BE">•Euro per tonnes (unit value)</span></p> <p>•National currency per tonnes (unit values)</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="ref_period"></a><a name="ref_period1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">5. Reference Period</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The reference period is the calendar year.</p> <p>The reference year is related to the time for production and use, which does not necessarily correspond to the time for sale and purchase.</p> <p>As an example, cereals produced (harvested) in year n are valued in the EAA for year n, including the value of the part sold or used in the following year.</p> <p><strong>Regional data</strong></p> <p>The reference period is the calendar year.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="inst_mandate"></a><a name="inst_mandate1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">6. Institutional Mandate</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="inst_man_la_oa"></a>6.1. Institutional Mandate - legal acts and other agreements</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;"><a title="regulation (EC) No 138/2004" href="http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:02004R0138-20140110&from=EN" target="_blank">Regulation (EC) No 138/2004</a> of the European Parliament and of the Council of 5 December 2003 on the economic accounts for agriculture in the Community (OJ L33, 05.02.2004) sets the legal basis for establishing a harmonized methodology for the compilation of the EAA. The regulation has been amended by Regulations 306/2005; 909/2006, 212/2008 (212/2008 relates to data from 2010) and 1350/2013.</p> <p>Provision of unit values is based on Gentlemen\'s agreement.</p> <p><strong>Regional data</strong></p> <p>Provision of regional data is based on Gentlemen\'s agreement.</p> <p style="text-align: justify;">Regions are defined according the NUTS as specified in <a title="Regulation (EC) No 1059/2003" href="http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32003R1059&from=EN" target="_blank">Regulation (EC) No 1059/2003</a> of the European Parliament and of the Council of 26 May 2003 on the establishment of a common classification of territorial units for statistics (NUTS). See the list of <a title="Region NUTS" href="http://ec.europa.eu/eurostat/c/portal/layout?p_l_id=629283&p_v_l_s_g_id=0" target="_blank">Regions NUTS 2013 used for EAA regional data</a>.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="inst_man_shar"></a>6.2. Institutional Mandate - data sharing</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The is no data sharing</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="conf"></a><a name="conf1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">7. Confidentiality</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="conf_policy"></a>7.1. Confidentiality - policy</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;"><a href="http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2009:087:0164:0173:En:PDF" target="_blank">Regulation (EC) No 223/2009 on European statistics</a> (recital 24 and Article 20(4)) of 11 March 2009 (OJ L 87, p. 164), stipulates the need to establish common principles and guidelines to ensure the confidentiality of data used for the production of European statistics and the access to those confidential data, with due account for technical developments and the requirements of users in a democratic society.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="conf_data_tr"></a>7.2. Confidentiality - data treatment</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Only non-confidential data are received. Data can be subject to agreed embargoes related to national release dates.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="rel_policy"></a><a name="rel_policy1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">8. Release policy</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="rel_cal_policy"></a>8.1. Release calendar</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>There is no release calendar.</p> <p>The results of the first estimates of the accounts and of the agricultural labour input statistics are normally published 2-3 weeks after the deadline (30 November in year N) through the online database.</p> <p>The results of the second estimates are published through the online database 5-6 weeks after the deadline (31 January in year N+1).</p> <p>Final/complete data are published through the online database 5-6 weeks after the deadline (30 September in year N+1) and in a News Release.</p> <p>Unit values are uploaded to the database upon receipt and validation.</p> <p><strong>Regional data</strong></p> <p>Regional data are uploaded in the database upon receipt and validation.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="rel_cal_access"></a>8.2. Release calendar access</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Not applicable.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="rel_pol_us_ac"></a>8.3. Release policy - user access</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p style="text-align: justify;">In line with the Community legal framework and the <a href="http://ec.europa.eu/eurostat/product?code=KS-32-11-955&mode=view&language=en" target="_blank">European Statistics Code of Practice</a> Eurostat disseminates European statistics on Eurostat\'s website respecting professional independence. It is done in an objective, professional and transparent manner in which all users are treated equitably. The detailed arrangements are governed by the <a href="http://ec.europa.eu/eurostat/product?code=IMPARTIAL_ACCESS_2012_OCT&mode=view&language=en" target="_blank">Eurostat protocol on impartial access to Eurostat data for users</a>.</p> <p style="text-align: justify;">In line with this protocol and on a strictly regulated basis, summary data on the first and second estimates of the EAA are sent for quality assurance to the European Commission Directorate General for Agriculture and Rural Development  (DG AGRI). This is done under embargo, 5 working days before data dissemination.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="freq_diss"></a><a name="freq_diss1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">9. Frequency of dissemination</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Annual - updates occur as there are more deadlines during the year.</p> <p>Regional data are also published annually.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="accessibility_clarity"></a><a name="accessibility_clarity1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">10. Accessibility and clarity</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="news_rel"></a>10.1. Dissemination format - News release</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>News releases on-line for final data.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="publications"></a>10.2. Dissemination format - Publications</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Eurostat publishes a <a href="http://ec.europa.eu/eurostat/documents/2995521/8468116/5-22112017-AP-EN.pdf/4b8a66c5-b003-4263-9de6-efe5fe5499a0">News Release</a> on agricultural output and <a href="http://ec.europa.eu/eurostat/documents/4031688/8527136/KS-06-17-250-EN-N.pdf/53f50971-fdc7-4e80-96d1-155eb9fa1185">Facts and figures</a> on agriculture in the European Union.</p> <p style="text-align: justify;">There is a Statistics Explained article on <a href="http://ec.europa.eu/eurostat/statistics-explained/index.php/Agricultural_accounts_and_prices">Agricultural output, price indices and income</a> which forms part of the set of articles making up the Agriculture, forestry and fishery statistics pocketbook. It gives an overview of indicators on agricultural output, of agricultural income and of agricultural prices in the European Union (EU). The data are extracted from Eurostat collections of agricultural statistics: the Economic Accounts for Agriculture (EAA), agricultural price indices (API) and absolute agricultural prices.</p> <p>DG AGRI publishes several data in the annual report for agriculture - 1st estimates for latest year.</p> <p><strong>Regional data</strong></p> <p>See chapter on agriculture in annual editions of <a href="http://ec.europa.eu/eurostat/web/products-statistical-books/-/KS-HA-16-001">Eurostat Regional Yearbook.</a></p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="online_db"></a>10.3. Dissemination format - online database</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Please consult free data on-line (Internet address: <a title="Eurostat\'s database" href="http://ec.europa.eu/eurostat/web/agriculture/data/database">http://ec.europa.eu/eurostat/web/agriculture/data/database</a>) or refer to <a title="contact address" href="mailto:ESTAT-EAP@ec.europa.eu">contact details</a></p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="micro_dat_acc"></a>10.4. Dissemination format - microdata access</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>No micro-data.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="diss_other"></a>10.5. Dissemination format - other</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Not applicable.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="doc_method"></a>10.6. Documentation on methodology</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The "Manual on the Economic Accounts for Agriculture and Forestry EAA/EAF 97 (Rev.1.1)" and the "Target methodology for agricultural labour input (ALI) statistics (Rev.1)" were published in 2000.</p> <p>Annex 1 of the EAA Regulation is an updated handbook/manual for the elaboration of the EAA and agricultural labour input.</p> <p>When differences between the original manuals and the Annex are found, pre-eminence should be given to the Annex.</p> <p>The Member States have submitted inventories about sources and compilation methods used for the calculation of the EAA at national level.</p> <p>The Manual, the Target Methodology (ALI) and the EAA Regulation 138/2004 can be found in the Annexes.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="quality_doc"></a>10.7. Quality management - documentation</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The inventories from the Member States contribute significantly to the documentation of quality. The inventories help improving the quality of data as the descriptions can be compared with the common methodology.</p> <p>Cross-checking analyses of data are discussed with the Member States at every annual Working Group meeting. Examples on issues raised are available on <a title="Documents for working group meetings" href="https://circabc.europa.eu/faces/jsp/extension/wai/navigation/container.jsp?FormPrincipal:_idcl=FormPrincipal:_id3&FormPrincipal_SUBMIT=1&id=d23c72f0-5537-4a9a-94bd-fc9b22b8d75f&javax.faces.ViewState=rO0ABXVyABNbTGphdmEubGFuZy5PYmplY3Q7kM5YnxBzKWwCAAB4cAAAAAN0AAE4cHQAKy9qc3AvZXh0ZW5zaW9uL3dhaS9uYXZpZ2F0aW9uL2NvbnRhaW5lci5qc3A=" target="_blank">CIRCABC (registration needed to enter into CircaBc). </a></p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="quality_mgmnt"></a><a name="quality_mgmnt1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">11. Quality management</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="quality_assure"></a>11.1. Quality assurance</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The consistency of the EAA data received is checked as a matter of routine.</p> <p>Comparability of the EAA data for year N against the data for year N-1 is checked. Member States are asked for clarifications where needed.</p> <p>The quality of uploads to production and dissemination databases are checked.</p> <p>The figures used in News Releases and statistical books are checked before release.</p> <p>Checks are carried out to ensure the consistency between national and regional EAA data.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="quality_assmnt"></a>11.2. Quality management - assessment</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The overall quality of the EAA statistics is considered to be good. The concept is well consolidated in most countries.</p> <p>The comparisons between first/second estimates and final data for factor income, the accounts and labour input (ALI) statistics demonstrate good reliability for most Member States in most years.</p> <p>Furthermore, the ALI data are compared with figures collected in the bi-annual Farm Structure Survey statistics.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="relevance"></a><a name="relevance1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">12. Relevance</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="user_needs"></a>12.1. Relevance - User Needs</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The main users are DG AGRI and Agricultural Ministries, as the main objective of the EAA is to provide data for monitoring and assessing the Common Agricultural Policy.</p> <p>Politicians, farming associations, agricultural researchers and journalists are also important users.</p> <p>DG AGRI is consulted about plans for changes and revisions in methodologies or data collection methods. DG AGRI has pre-access to data before publishing according to a Memorandum of Understanding.</p> <p style="text-align: justify;">Users may request EAA-data broken down by type of farming, type of management, etc. This request cannot be met without further increasing the statistical burden, and most of the sources for the EAA are not suitable for such breakdowns. It should be noted that comparisons between farm types are a primary purpose of the Farm Accountancy Data Network statistics.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="user_sat"></a>12.2. Relevance - User Satisfaction</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Feedback from users who have asked for data and/or explanations is positive.</p> <p>The press coverage of News Releases and other publications indicates the high relevance of the information provided.</p> <p>Regarding agricultural statistics in general, user satisfaction is discussed by the Standing Committee on Agricultural Statistics.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="completeness"></a>12.3. Completeness</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>EAA data at national level exist for all Member States  (BE, BG, CZ, DK, DE, EE, IE, EL, ES, FR, HR, IT, CY, LV, LT, LU, HU, MT, NL, AT, PL, PT, RO, SI, SK, FI, SE, UK) plus CH, IS and NO.</p> <p>The data sets consist of more than one hundred accounting items per country. For most countries, the data sets are quite complete, as the account is a comprehensive structure where no important element can be missing without severe consequences.</p> <p>Gaps in the data are in most cases related to the completeness of specifications. The most serious gaps in the data are due to lack of information in constant prices regarding capital formation.</p> <p>The data on labour input relate to only three characteristics per country and are complete.</p> <p>The data for the regional EAA are less complete. Most countries submit complete data, however some years are missing in a number of cases.</p> <p>Member States are regularly informed about gaps and asked to improve the situation.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="accuracy"></a><a name="accuracy1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">13. Accuracy</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="accuracy_overall"></a>13.1. Accuracy - overall</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Due to the multi-source character of the EAA, it is not possible to provide an overall indicator. The labour input data are normally based on the Farm Structure Survey, where the accuracy is high.</p> <p>The differences between the first/second estimates and final data for indicators are monitored. In most cases (years, countries), comparisons show a high level of reliability in the first/second estimates.</p> <p>The accuracy of regional EAA data is considered to be lower than for the country level data as the regional data are breakdowns of national data (often based on a top-down approach).</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="sampling_err"></a>13.2. Sampling error</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>It is not possible to measure a figure in this context.</p> <p>For most of the source statistics (Farm Structure Survey, crops statistics, milk statistics etc.) sampling errors are compiled.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="nonsampling_err"></a>13.3. Non-sampling error</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>There can be errors in the sources used by countries for the EAA and agricultural labour input statistics. The errors can derive from:</p> <p>- problems in identifying the sector coverage exactly</p> <p>- limited information about the activities in the units below the thresholds applied</p> <p>- difficulties in measuring the output and use of forage</p> <p>- a lack of prices for several product qualities</p> <p>- difficulties in allocating output, subsidies etc. according to the accruals principle</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="timeliness_punct"></a><a name="timeliness_punct1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">14. Timeliness and punctuality</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="timeliness"></a>14.1. Timeliness</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p><strong>EAA data and agricultural labour input data:</strong></p> <p>First estimates: received in November of the reference year (N); published in December N.</p> <p>Second estimates: received in January of N+1; published in March N+1</p> <p>Final data: received in September of N+1 and published in November N+1</p> <p>Unit value data are normally received at the same time as provisional accounts data. Revised and definitive data are received in parallel with revised/definitive accounts data.</p> <p>The situation regarding the timetable for the first revision and the definitive data is different from country to country.</p> <p>Regional data</p> <p>Regional data for year N are received in September N+2 and revisions received in September N+3.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="punctuality"></a>14.2. Punctuality</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>In almost all cases, countries submit their data according to the deadlines under Regulation. Delays of more than a few days are rare.</p> <p>Regarding the data provided under Gentlemen\'s Agreement, the delays can be somewhat longer.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="coher_compar"></a><a name="coher_compar1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">15. Coherence and comparability</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="compar_geo"></a>15.1. Comparability - geographical</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Due to the different sources, data across countries are not fully comparable. However, for items compiled based on consolidated data sources (agricultural production statistics and price statistics) the comparability is considered to be high.</p> <p>Regarding the harmonisation of the implementation of the methodology, there are still areas where the situation can be improved.</p> <p>Furthermore, it can be difficult to compare meaningfully the net factor income between countries with high capital input and low labour input in agriculture with countries having the opposite profile.</p> <p>The comparability of agricultural labour input statistics is considered to be high. However, the number of working hours per Annual Work Unit can differ from country to country. Furthermore, it can be difficult to measure the input related to small farms.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="compar_time"></a>15.2. Comparability - over time</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The comparability over time can be considered as good, in particular for long-standing Member States. For the newer Member States, the comparability over time has improved in recent years.</p> <p>It should be noted that the significant changes in EU subsidy regimes have had an impact over time on the comparability of some accounting items.</p> <p>The change in type of subsidy under the CAP has also affected the comparisons of unit value prices.</p> <p><strong>Regional data</strong></p> <p>Comparability of regional data over time will be affected by breaks in the NUTS classification.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="coher_x_dom"></a>15.3. Coherence - cross domain</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Developments in the EAA data have largely been coherent with the combined developments of agricultural production statistics and agricultural price statistics. The EAA validation includes comparisons with these statistics.</p> <p style="text-align: justify;">The main indicators (output, intermediate consumption, GVA, etc) in the EAA are, with respect to different sector definitions, comparable with national accounts figures for the agricultural sector (NACE 01). Major deviations are communicated to the Member States for investigation.</p> <p>EAA figures are also compared with data from the Farm Accountancy Data Network (micro-economic statistics) as far as possible and with respect to limitations.</p> <p>The differences between first/second estimates and final data for the main indicators of the EAA (Factor Income, Labour Input and the Index of real factor income per labour input (Indicator A)) are monitored closely.</p> <p>Unit Values can to some extent be compared with absolute prices. However, the unit values show prices for average quality for the year, while absolute prices follow a specific, defined quality.</p> <p style="text-align: justify;"><strong>Regional data</strong> are coherent with the national data transmitted by Member States in September year N+2. However, whereas national data can be revised continuously, regional data are revised only once per year. As a result, revisions of national data can occur after the regional data have been transmitted and released. This can lead to differences between the data in domains agr_r_accts and aact_eaa01.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="coher_internal"></a>15.4. Coherence - internal</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Overall, the EAA data can be considered consistent; the indicators for development provide reliable statistical results.</p> <p>However, there is still room to improve the level of harmonisation.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="cost_burden"></a><a name="cost_burden1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">16. Cost and Burden</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The response burden related to data collection for EAA is low, as the EAA are mainly compiled by using other statistics plus administrative information.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="data_rev"></a><a name="data_rev1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">17. Data revision</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="rev_policy"></a>17.1. Data revision - policy</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>National data are revised according to national schedules and revisions are applied to Eurostat\'s online database. When revised data are received from a country, calculations are made to obtain derived sets of tables.</p> <p>From the point of view of the timetable for disseminating the data, EAA and ALI timetables are identical:</p> <p>•First estimates: received in November of the reference year (N) and published in December N.</p> <p>•Second estimates: received in January of N+1 and published in March N+1</p> <p>•Final data: received in September of N+1 and published in November N+1</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="rev_practice"></a>17.2. Data revision - practice</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The EAA are under a process of continuous revisions which depend on the data available in each country. There can be changes in the methodology as a result of implementing EC Regulations. Substantial changes in the methodology are explained by the countries when releasing for the first time the data affected by such methodological changes.</p> <p>Revisions of data considered as final according to 17.1 can happen as an outcome of initiatives to improve quality and comparability.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="stat_process"></a><a name="stat_process1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">18. Statistical processing</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="source_type"></a>18.1. Source data</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The main sources used for the compilation of the EAA are agricultural statistics and administrative sources.</p> <p style="text-align: justify;">The evaluation of crop output can normally be based on resources, i.e. the estimate of quantities produced (harvested) based on estimates of areas under crops and yields, or on uses, i.e. on estimates of purchases by the user branches of agricultural products, exports net of imports, to which should be added certain quantities used for intermediate consumption by the agricultural industry, changes in producer stocks and use for own account (much of which is own final consumption). The latter approach can be proved highly appropriate in cases where the buyers of these agricultural products are readily identifiable and the four other components of uses are limited (for example, products requiring preliminary processing before they can be used, such as sugar beet, tobacco, etc.). Nevertheless, a physical balance sheet is necessary in order to verify the consistency and reliability of the data.</p> <p style="text-align: justify;">Statistics on slaughtering, exports/imports of live animals and the size of herds are the main sources of data for measuring the output of animals. The output of animal products (mainly milk) is generally estimated using sales to user branches (dairies, packers) because of the specific uses to which they are put.</p> <p>Agricultural Price Statistics is a main source for the valuation of agricultural output as well as intermediate consumption. Prices and price indices are also an important source for compiling values in constant prices.</p> <p style="text-align: justify;">Most of the intermediate goods that form part of the EAA can broadly only be used in agriculture (seeds and planting stock, fertilisers, pesticides, etc.). In this case, purchases by the agricultural sector are based on the data relating to sales by the economic branches which supply these intermediate goods (after inclusion of external trade).</p> <p style="text-align: justify;">The Farm Structure Survey is normally the main data source for agricultural labour input data. In years when this EU Farm Structure Survey is not carried out, there is often some part of the structure survey on agricultural holdings carried out for national purposes (for example, often specific to labour). In some Member States, these are sample surveys, in others exhaustive surveys. The scope and the substance of these (national) surveys are (partly) the same as for the EU Farm Structure Survey.</p> <p>The Farm Accountancy Data Network can be a source that contributes to the valuation of intermediate consumption and the elements in the income account.</p> <p>Administrative information on, for example, subsidies and taxes is also an important source.</p> <p>Expert estimates are also used in some cases - often for adjusting sector coverage.</p> <p>Unit values only build upon existing data.</p> <p><strong>Regional data</strong></p> <p>This varies from country to country. Specific data collections can be organised for compiling the regional EAA or they can be compiled as a breakdown of the national EAA.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="freq_coll"></a>18.2. Frequency of data collection</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Data collection specifically for EAA is implemented annually.</p> <p>Data collection related to other domains is defined by the specific regulations/decisions. </p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="coll_method"></a>18.3. Data collection</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Data is collected in line with Eurostat policies and procedures via eDAMIS using webforms or Excel files.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="data_validation"></a>18.4. Data validation</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Incoming EAA, ALI and UV data are validated according to logical rules, including reporting of mandatory data items, data formats and trends.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="data_comp"></a>18.5. Data compilation</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>The countries compile national EAA data in current prices and in prices of the previous year for each variable, as required by Regulation 138/2004.</p> <p>Data in base year prices (n = 2010 and n = 2005) are calculated using data in current (n) and previous year prices (n-1).</p> <p>Real values data (2010, 2005 and n-1 values) are compiled using national GDP-deflators.</p> <p>Indicators A, B and C are compiled using data in real-terms values and labour input data.</p> <p>Indices for volumes, prices and values, nominal and real-terms are compiled on the basis of current values, constant price values and real-terms values.</p> <p style="text-align: justify;">Aggregation: EU aggregates for the EAA are constructed by Eurostat by summing up the national EAA after applying, if necessary, the annual exchange rate. The EU aggregates for labour input are constructed by summing up the absolute Annual Work Units. Due to the nature of unit values, there is no aggregation.</p> <p><strong>Regional data</strong></p> <p>Regional data are received in current prices only and are not further compiled except converting into euro for countries using other currencies.</p></td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Level2"><a name="adjustment"></a>18.6. Adjustment</td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>No adjustments are applied. </p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="comment_dset"></a><a name="comment_dset1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">19. Comment</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><p>Not defined.</p></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="relatedmd"></a><a name="relatedmd1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">Related metadata</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"></td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '    <p class="DocCommon"> <br /> </p> \n' +
  '    <table width="99%" cellpadding="0" cellspacing="0" border="0" class="TblBottom"> \n' +
  '     <tbody>\n' +
  '      <tr> \n' +
  '       <td class="Cl-Level1" colspan="2"> <a name="annex"></a><a name="annex1523958106829"></a>\n' +
  '        <table border="0" cellpadding="0" cellspacing="0" width="100%"> \n' +
  '         <tbody>\n' +
  '          <tr> \n' +
  '           <td class="Cl-Level1-NoBorder">Annexes</td>\n' +
  '           <td align="right"><a class="DocCommon" href="#">Top</a></td> \n' +
  '          </tr> \n' +
  '         </tbody>\n' +
  '        </table> </td> \n' +
  '      </tr> \n' +
  '      <tr> \n' +
  '       <td class="Cl-Content"><a title="aact_esms_an2.pdf" href="../Annexes/aact_esms_an2.pdf" target="_blank">List of available national- and regional- datasets</a> <br /> <a title="aact_esms_an3.pdf" href="../Annexes/aact_esms_an3.pdf" target="_blank">The Manual on the economic accounts for Agriculture and Forestry EAA/EAF 97 (Rev.1.1)</a> <br /> <a title="aact_esms_an4.pdf" href="../Annexes/aact_esms_an4.pdf" target="_blank">Target methodology for agricultural labour input (ALI) statistics (Rev.1)</a> <br /> <a title="aact_esms_an5.pdf" href="../Annexes/aact_esms_an5.pdf" target="_blank">Regulation (EC) No 138/2004 of the European Parliament and of the Council of 5 December 2003 on the economic accounts for agriculture in the Community</a> <br /> </td> \n' +
  '      </tr> \n' +
  '     </tbody>\n' +
  '    </table> \n' +
  '   </div> \n' +
  '  </div>');
