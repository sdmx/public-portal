import React from 'react';
import Head from 'next/head';
import { message, Icon, Menu, Dropdown, Modal, Tabs } from 'antd';
import { bindActionCreators } from 'redux';

import page from '~/page';
import { Storage, DataService } from '~/utils/api';

import { getDimensions } from '~/store/data-service/dsd';
import { getSeries } from '~/store/data-service/dataset';

import Layout from '~/components/Layout/Public';
import Loading from '~/components/Loading';
import Empty from '~/components/Empty';
import Filter from '~/components/DataService/Filter';
import DataView from '~/components/DataService/DataView';
import Category from '~/components/DataService/Category';
import Metadata from './metadata';

const TABS = {
  DATA: 'DATA',
  FILTER: 'FILTER',
};

class DataServiceBuild extends React.Component {
  static getInitialProps = ({ query: { id, provider, type } }) => {
    const protocol = 'sdmx';
    const srcProvider = `${protocol}:${provider}`;
    const dataId = `${srcProvider}/${id}`;

    return { id, type, protocol, srcProvider, dataId };
  }

  constructor(props) {
    super(props);

    switch (props.type) {
      case 'build':
        this.dataIds = Storage.get(props.id, '').split('|');
        break;

      default:
      case 'view':
        this.dataIds = props.dataId;
        break;
    }

    this.state = {
      selectedTab: TABS.FILTER,
      title: 'Data',
    };

    this.getTitle = this.getTitle.bind(this);
  }

  componentDidMount() {
    const { dsd: { error } } = this.props;

    this.props.getDimensions(this.dataIds).then(() => {
      if (error) {
        message.error(error.message);
      }
    });

    this.getTitle(this.dataIds);
  }

  getFilterKey = (dsd, filters = {}) => {
    const keys = [];
    const { dimensions = [] } = dsd;

    dimensions.forEach(({ id }) => {
      if (filters[id] !== undefined) {
        keys.push(filters[id]);
      }
      else {
        keys.push('');
      }
    });

    return keys.join('.');
  }

  getTitle = (dataIds) => {
    const { srcProvider } = this.props;
    const isMultiple = dataIds instanceof Array;

    if (!isMultiple) {
      const args = dataIds.split('/');

      DataService.getDataflows(srcProvider, [].concat(args).pop())
        .then(({ data }) => {
          const df = [].concat(data.content).shift();

          if (df) {
            this.setState({ title: df.name });
          }
        });
    }
  }

  render() {
    const {
      protocol,
      dataId,
      provider,
      dsd = { dimensions: [] },
      dataset: { error, filterParams = {}, isFinished, series },
    } = this.props;
    const { title } = this.state;

    if (error) {
      message.error(error.message);
    }

    const metadata = (
      <div className="pl3 h-100 overflow-y-auto overflow-x-hidden relative">
        <div className="absolute">
          <div dangerouslySetInnerHTML={{ __html: Metadata(title) }} />
        </div>
      </div>
    );

    return (
      <Layout title={title} left={<Category provider={provider} />} right={metadata}>
        <Head>
          <title>{title}</title>
          <link href="/static/css/leaflet.css" rel="stylesheet" />
          <link href="/static/css/pivottable.css" rel="stylesheet" />
        </Head>

        <Loading isFinish={dsd.isFinished && isFinished}>
          <Tabs
            activeKey={this.state.selectedTab}
            onTabClick={key => this.setState({ selectedTab: key })}
          >
            {/* Filter by Dimensions and Time Period */}
            <Tabs.TabPane key={TABS.FILTER} tab={<div><Icon type="filter" /> Filter</div>}>
              <Filter
                filterKeys={filterParams.filter}
                startPeriod={filterParams.startPeriod}
                endPeriod={filterParams.endPeriod}
                dimensions={dsd.dimensions}
                onSubmit={({ filterKeys, startPeriod, endPeriod }) => {
                  this.props.getSeries(this.dataIds, filterKeys, startPeriod, endPeriod).then(({ payload }) => {
                    const emptyResult = payload.length === 0;

                    if (!emptyResult && !error) {
                      this.setState({ selectedTab: TABS.DATA });
                    }

                    if (emptyResult) {
                      message.error('Could not find any results');
                    }
                  });
                }}
              />
            </Tabs.TabPane>

            {/* Pivot Table, Charts & Map */}
            <Tabs.TabPane key={TABS.DATA} tab={<div><Icon type="bars" /> Data</div>}>
              <DataView
                protocol={protocol}
                dataId={dataId}
                title={title}
                dimensions={dsd.dimensions}
                series={series}
                filter={{
                  key: this.getFilterKey(dsd, filterParams.filter),
                  startPeriod: filterParams.startPeriod,
                  endPeriod: filterParams.endPeriod,
                }}
              />
            </Tabs.TabPane>
          </Tabs>
        </Loading>
      </Layout>
    );
  }
}

const mapStateToProps = ({ dsd, dataset }) => ({ dsd, dataset });

const mapDispatchToProps = dispatch => bindActionCreators({ getDimensions, getSeries }, dispatch);

export default page(DataServiceBuild, mapStateToProps, mapDispatchToProps);
