import React from 'react';

import Layout from '~/components/Layout/Public';
import page from '~/page';

const Homepage = () => (
  <Layout>
    <div className="center w-60 tj">
      <h1 className="mv4 bb bw2 b--dark-gray dark-gray"> PT. Sarana Primadata </h1>

      <p>Founded in 2001, PT. Sarana Primadata (SPD) is a private company that is engaged in the field of consultancy services that are professional. SPD eager to participate to volunteer and mind in accordance with the field service company, in order supporting the implementation of development programs for welfare society, nation and state.</p>

      <p>SPD always uphold professionalism in every implementation of the work with our expert and team work of experienced technical and managerial, devices and adequate equipment and support strong management. The success of our services came from a clear understanding of customer’s needs.</p>

      <p>Since 2014, SPD has begun transformed into information technology-based consulting firm that covered not only in the processing and management of spatial data, but also includes the development of database applications in its class, Enterprise Analytic & Statistics, and Data Warehouse & Business Intelligence.</p>

      <p>SPD offers a comprehensive package of services from solution architecture planning to big data management and deployment/ sustainment.</p>

      <p>Our technology, business process and software experts specialize in many of today’s most complex business challenges.</p>
    </div>
  </Layout>
);

export default page(Homepage);
