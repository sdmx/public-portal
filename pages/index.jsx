import React from 'react';
import { Icon, Input } from 'antd';
import moment from 'moment';
import startCase from 'lodash/startCase';
import classNames from 'classnames';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { Content } from '~/utils/api';
import utils from '~/utils';
import Auth from '~/utils/auth';
import page from '~/page';
import Layout from '~/components/Layout/Public';
import Link from '~/components/Link';
import Banner from '~/components/Homepage/Banner';

const statsIndicator = [
  { name: 'USD', values: [2.92330, 2.93310] },
  { name: 'EUR', values: [3.1965, 3.1967] },
  { name: 'GBP', values: [4.3489, 4.3571] },
  { name: 'EURO/USD', values: [1.0979, 1.0980] },
  { name: 'BIST 30', values: [3.346, 3.345] },
];

const commodity = [
  { label: 'DUBAI', value: '00:00', status: '=', dateTime: moment().format('DD MMM') },
  { label: 'BRENT', value: '00:01', status: '+', dateTime: moment().format('DD MMM') },
  { label: 'GOLD', value: '00:01', status: '+', dateTime: moment().format('DD MMM') },
  { label: 'COPPER', value: '1:50', status: '-', dateTime: moment().format('DD MMM') },
];

const dailyIndicator = [
  { label: 'CD (91 DAYS)', status: 'Closed', time: moment().format('HH:mm') },
  { label: 'TB (3 YEARS)', status: 'Closed', time: moment().format('HH:mm') },
  { label: 'CB (3 YEARS, AA-)', status: 'Closed', time: moment().format('HH:mm') },
  { label: 'USD/KRW', status: 'Closed', time: moment().format('HH:mm') },
  { label: 'USD/JPY', status: 'Closed', time: moment().format('HH:mm') },
];

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    Auth.user(
      user => this.setState({ user, userLoaded: true }),
      () => this.setState({ userLoaded: true }),
    );

    this.fetchNews = this.fetchNews.bind(this);

    this.state = {
      news: [],
      publication: [],
      userLoaded: false,
      user: null,
    };
  }

  componentDidMount() {
    this.fetchNews();
    this.fetchPublications();
  }

  fetchNews() {
    Content.list('news', '', 5)
      .then(({ data }) => this.setState({ news: data.data }));
  }

  fetchPublications() {
    Content.list('publication', '', 7)
      .then(({ data }) => this.setState({ publication: data.data }));
  }

  render() {
    const { router } = this.props;
    const { user, userLoaded, news = [], publication = [] } = this.state;
    const sliderSettings = {
      // swipe: true,
      // autoplaySpeed: 2000,
      autoplay: true,
      vertical: true,
      dots: false,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    const dsSubjects = [
      "finance", "population", "education",
      "environment", "government", "science",
      "region", "legal", "traffic",
      "agriculture", "energy", "health",
    ];

    return (
      <Layout>
        {/* User Login */}
        {/* <div className="tr pb2">
          {userLoaded && (user ? (
            <Dropdown
              trigger={['click']}
              overlay={(
                <Menu>
                  <Menu.Item key="0">
                    <Link href="/dashboard">
                      <Icon type="dashboard" />
                      Dashboard
                    </Link>
                  </Menu.Item>
                  <Menu.Divider />
                  <Menu.Item key="3">
                    <Link href="/auth/logout">
                      <Icon type="logout" />
                      Logout
                    </Link>
                  </Menu.Item>
                </Menu>
              )}
            >
              <span className="b pointer">
                {user.name}
                <Icon type="down-square-o" />
              </span>
            </Dropdown>
          ) : (
            <div>
              <a href="/auth/login" className="b">Login</a>
              <span className="ph3">|</span>
              <Link href={`${process.env.AUTH_ENDPOINT}/register`} className="b">Sign Up</Link>
            </div>
          ))}
        </div> */}

        {/* Body Wrapper */}
        <div className="bg-primary-lightest pa3" style={{ margin: '0 -20px' }}>
          {/** Search Input */}
          <div className="pb2 flex items-center justify-center">
            <Input.Search
              placeholder="Search"
              className="pa2 w-50"
              // value={this.state.query}
              // onChange={e => this.setState({ query: e.target.value })}
              onSearch={value => router.replace(`/search?q=${value}`)}
            />
          </div>

          {/* Currency Indicators */}
          <div className="overflow-hidden bg-primary-dark">
            <marquee>
              <div className="pv2 ph4 flex justify-center nowrap">
                {statsIndicator.map(stat => (
                  <div key={stat.name} className="white-60 ph2 f7 nowrap hover-white-80">
                    <span className="b ph1">
                      {stat.name}
                    </span>
                    <span className="ph1">
                      <Icon type="caret-left" />
                      {stat.values[0]}
                    </span>
                    <span className="ph1">
                      <Icon type="caret-right" />
                      {stat.values[1]}
                    </span>
                  </div>
                ))}
              </div>
            </marquee>
          </div>

          {/* Content Heading */}
          <Banner />

          {/* Content Body */}
          <div className="flex mt3">
            {/* Left Content Body */}
            <div className="w-20">
              {/* Commodity */}
              <div className="pa2 bg-white mb2">
                <div className="bg-primary-lightest b pa1 tc"> COMMODITY </div>
                <table className="w-100" cellPadding="5px">
                  {commodity.map(item => (
                    <tr>
                      <td>{item.label}</td>
                      <td className={classNames({ tr: true, red: item.status === '+' })}>
                        {item.status !== '=' && item.status}
                        {item.value}
                      </td>
                      <td className="tr">{item.dateTime}</td>
                    </tr>
                  ))}
                </table>
              </div>

              {/* Daily Indicator */}
              <div className="pa2 bg-white mb2">
                <div className="bg-primary-lightest b pa1 tc"> DAILY INDICATOR </div>
                <table className="w-100" cellPadding="5px">
                  {dailyIndicator.map(item => (
                    <tr>
                      <td>{item.label}</td>
                      <td>{item.time}</td>
                      <td>{item.status}</td>
                    </tr>
                  ))}
                </table>
              </div>

              <div classnames="mv3">
                <Link href="/" className="pa2 bg-primary-dark white tc db mv3">
                  <span className="b f7">VISUAL STAT</span>
                  <Icon type="pie-chart" theme="filled" className="f6 ml3" />
                </Link>

                <Link href="/" className="pa2 bg-primary white tc db mv3">
                  <span className="b f7">INDONESIA STAT</span>
                  <Icon type="line-chart" className="f6 ml3" />
                </Link>
              </div>
            </div>

            {/* Main Content Body */}
            <div className="w-60 ph2">
              {/* Icon Menu */}
              <div className="flex">
                <div className="w-33 pa2">
                  <Link href="/data/internal" className="tc bg-secondary db shadow-hover pa2">
                    {/* <img src="/static/img/icon/icon-statistic.png" alt="Statistic" height="100px" /> */}
                    <div className="pt2">
                      <Icon type="line-chart" className="f1 white" />
                    </div>
                    <div className="white b tc pa1 ttu mt2">Data</div>
                  </Link>
                </div>
                <div className="w-33 pa2">
                  <Link href="/news" className="tc bg-secondary db shadow-hover pa2">
                    {/* <img src="/static/img/icon/icon-news.png" alt="News" height="100px" /> */}
                    <div className="pt2">
                      <Icon type="pic-right" className="f1 white" />
                    </div>
                    <div className="white b tc pa1 ttu mt2">News</div>
                  </Link>
                </div>
                <div className="w-33 pa2">
                  <Link href="/publication" className="tc bg-secondary db shadow-hover pa2">
                    {/* <img src="/static/img/icon/icon-publication.png" alt="Publication" height="100px" /> */}
                    <div className="pt2">
                      <Icon type="file-text" className="f1 white" />
                    </div>
                    <div className="white b tc pa1 ttu mt2">Publication</div>
                  </Link>
                </div>
              </div>

              {/* news */}
              <div className="bg-white pa2 mb2">
                <div className="bg-primary-lightest pa2 b f6"> STATISTIC INFORMATION </div>
                <table className="w-100" cellPadding="5px">
                  {news.slice(0, 3).map(({ id, name, timestamp }) => (
                    <tr>
                      <td>
                        <Link href={`/content/news/${id}`} className="black-60">
                          <span className="f7">{name}</span>
                        </Link>
                      </td>
                      <td className="tr" width="100px">
                        {timestamp || moment().format('DD MMM YYYY')}
                      </td>
                    </tr>
                  ))}
                </table>
              </div>

              {/* publication */}
              <div className="bg-white pa2 mb2">
                <div className="bg-primary-lightest pa2 b f6"> PUBLICATION </div>
                <table className="w-100" cellPadding="5px">
                  {publication.map(({ id, name, timestamp }) => (
                    <tr>
                      <td>
                        <Link href={`/content/publication/${id}`} className="black-60">
                          <span className="f7">{name}</span>
                        </Link>
                      </td>
                      <td className="tr" width="100px">
                        {timestamp || moment().format('DD MMM YYYY')}
                      </td>
                    </tr>
                  ))}
                </table>
              </div>
            </div>

            {/* Right Content Body */}
            <div className="w-20">
              {/* News */}
              <div className="bg-white pa2 mb2">
                <div className="bg-primary-lightest pa2 b"> NEWS </div>
                <div>
                  {news.slice(-1).map(({ id, name, thumbnail }) => (
                    <div key={id} className="cf">
                      <Link href={`/content/news/${id}`} className="black-60">
                        <div className="fl w-30 pa2">
                          <img alt={name} src={Content.fileUrl(thumbnail)} />
                        </div>
                        <div className="fl pv2 bb b--white-20 w-70">
                          <span className="f7">{utils.truncate(name, 40)}</span>
                        </div>
                      </Link>
                    </div>
                  ))}
                </div>
              </div>

              {/* Dataset by Subject */}
              <div className="pa2 mb2">
                <div className="bg-primary-lightest pa2 b"> DATASETS BY SUBJECT </div>
                <div className="cf">
                  {dsSubjects.map(subject => (
                    <div key={subject} className="fl w-33 pa2" style={{'height': '80px'}}>
                      <Link href={`/`} className="black-60 tc">
                        <div> <img alt={subject} src={`/static/img/icon/ds-subjects/${subject}.png`} height="50px" /> </div>
                        <div className="f7">{startCase(subject)}</div>
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default page(Homepage);
