import React from 'react';
import { Icon, message } from 'antd';
import Img from 'react-image-fallback';
import moment from 'moment';

import Layout from '~/components/Layout/Public';
import { Content } from '~/utils/api';
import page from '~/page';

const SiderItem = ({ label, children }) => (
  <div className="mb3">
    {label && <p className="b">{label}</p>}
    {children}
  </div>
);

class ContentView extends React.Component {
  static getInitialProps({ query }) {
    return {
      id: query.id,
      contentType: query.contentType,
    };
  }

  constructor(props) {
    super(props);
    this.state = { data: {} };

    this.fetchData = this.fetchData.bind(this);

    this.fetchData(props.id);
  }

  fetchData(id) {
    Content.get(this.props.contentType, id)
      .then(({ data }) => this.setState({ data }))
      .catch(err => message.error(err.message));
  }

  render() {
    const { contentType } = this.props;
    const { content, categories, attachments } = this.state.data;

    return (
      <Layout isLoading={!content} title={content ? content.name : contentType}>
        {content && (
          <div className="ph5 pt2">
            <div className="f6">{moment(content.created).format('DD MMM YYYY')}</div>
            <h1 className="f3 mb4 mt0 b primary">{content.name}</h1>

            <div className="cf bb pv1">
              <div className="fl"></div>
              <div className="fr">
                <Icon type="mail" className="f3 ph1" />
                <Icon type="twitter" className="f3 ph1 light-blue" />
                <Icon type="facebook" className="f3 ph1 blue" />
                <Icon type="google-plus" className="f3 ph1 light-red" />
              </div>
            </div>

            <div className="cf">
              <div className="fl w-70" style={{ textAlign: 'justify' }}>
                <div dangerouslySetInnerHTML={{ __html: content.body }} /> <br />
              </div>
              <div className="fl w-30 pl4">
                <div className="pl4">
                  <SiderItem>
                    <Img
                      src={`${process.env.CMS_ENDPOINT}/file/${content.thumbnail}`}
                      alt={content.name}
                      fallbackImage="/static/img/default/content.png"
                      className="w-100"
                    />
                  </SiderItem>

                  {attachments && attachments.length > 0 && (
                    <SiderItem label="Documents">
                      {attachments && attachments.map(attachment => (
                        <a
                          href={`${process.env.CMS_ENDPOINT}/file/${attachment.filepath}`}
                          className="ba b--moon-gray pa2 br1 mt1 db"
                          target="_blank"
                        >
                          <Icon type="file-pdf" /> {attachment.displayName}
                        </a>
                      ))}
                    </SiderItem>
                  )}

                  {categories && categories.length > 0 && (
                    <SiderItem label="Categories">
                      {categories.map(category => (
                        <div className="fl pv1 ph2 br2 ba b--moon-gray mr2">
                          {category.name}
                        </div>
                      ))}
                    </SiderItem>
                  )}
                </div>
              </div>
            </div>

            <div className="cf mt4 tr">
              <Icon type="printer" className="f3" />
            </div>
          </div>
        )}
      </Layout>
    );
  }
}

export default page(ContentView);
