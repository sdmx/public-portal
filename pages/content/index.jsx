import React from 'react';
import v from 'voca';

import Layout from '~/components/Layout/Public';
import ListItem from '~/components/Content/ListItem';
import Newest from '~/components/Content/Newest';
import Highlight from '~/components/Content/Highlight';
import page from '~/page';

class ContentList extends React.Component {
  static getInitialProps(ctx) {
    const { query } = ctx;

    return {
      contentType: query.contentType,
    };
  }

  render() {
    const { contentType } = this.props;
    const title = v.titleCase(contentType);

    return (
      <Layout title={title}>
        <div className="flex">
          <div className="w-25">
            <ListItem title={title} contentType={contentType} />
          </div>

          <div className="w-50">
            <Newest contentType={contentType} />
          </div>

          <div className="w-25">
            <Highlight contentType={contentType} />
          </div>
        </div>
      </Layout>
    );
  }
}

export default page(ContentList);
