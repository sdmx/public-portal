import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';

export default class MainDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <html lang={this.props.locale}>
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <meta name="description" content={process.env.APP_NAME} />
          <meta name="viewport" content="width=device-width, initial-scale=1" />

          <title>{process.env.APP_NAME}</title>

          <link rel="shortcut icon" href="/static/favicon.ico" />

          {/* <link rel="manifest" href="/static/site.webmanifest" /> */}

          {/* <link href="/static/css/normalize.min.css" rel="stylesheet" />
          <link href="/static/css/tachyons.min.css" rel="stylesheet" /> */}

          {/* <link href="/static/css/app.min.css" rel="stylesheet" /> */}
          {/* <link href="/static/css/antd.min.css" rel="stylesheet" /> */}

          {/* <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" /> */}
          {/* <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" /> */}
          {/* <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css" rel="stylesheet" /> */}
          {/* <link href="https://cdnjs.cloudflare.com/ajax/libs/tachyons/4.9.1/tachyons.min.css" rel="stylesheet" /> */}
          {/* <link href="https://unpkg.com/@blueprintjs/core@^2.0.0/lib/css/blueprint.css" rel="stylesheet" /> */}
          {/* <link href="https://unpkg.com/@blueprintjs/icons@^2.0.0/lib/css/blueprint-icons.css" rel="stylesheet" /> */}
          {/* <link href="/static/css/main.css" rel="stylesheet" /> */}
          {/*<link rel="stylesheet" type="text/css" href="/_next/static/style.css" />*/}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
