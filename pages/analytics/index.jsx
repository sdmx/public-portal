import React from 'react';
import PropTypes from 'prop-types';

import Layout from '~/components/Layout';
import page from '~/page';

const SUPERSET_HOST = 'http://localhost:11000';

const Dashboard = ({ data }) => (
  <Layout>
    <iframe
      src={SUPERSET_HOST}
      width="100%"
      height="550px"
      frameBorder="0"
      allowTransparency="true"
      // scrolling="no"
    />
  </Layout>
);

Dashboard.propTypes = {
  data: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.node)),
};

Dashboard.defaultProps = {
  data: [],
};

export default page(Dashboard);
