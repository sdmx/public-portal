import React from 'react';
import { Icon, Tabs, Input } from 'antd';
import Router from 'next/router';
import axios from 'axios';
import { Map } from 'immutable';
import isArray from 'lodash/isArray';

import Layout from '~/components/Layout/Public';
import { Content, DataService } from '~/utils/api';
import page from '~/page';

import NewsResults from '~/components/Search/NewsResults';
import PublicationResults from '~/components/Search/PublicationResults';
import DataflowResults from '~/components/Search/DataflowResults';

const sources = [
  {key: 'sdmx:EUROSTAT', url:'/data/sdmx/EUROSTAT', label: 'Eurostat'},
  {key: 'sdmx:OECD', url:'/data/sdmx/OECD', label: 'OECD'},
  {key: 'sdmx:ILO', url:'/data/sdmx/ILO', label: 'ILO'},
  {key: 'sdmx:IMF2', url:'/data/sdmx/IMF2', label: 'IMF'},
  {key: 'sdmx:ECB', url:'/data/sdmx/ECB', label: 'ECB'},
  // {key: 'sdmx:EUROSTAT', url:'/data/bps', label: 'BPS'},
];

class ContentList extends React.Component {
  static getInitialProps = ({ query }) => {
    return { searchQuery: query.q };
  }

  constructor(props) {
    super(props);
    this.state = {
      news: {
        loading: true,
        data: [],
      },
      publication: {
        loading: true,
        data: [],
      },
      dataflow: {
        loading: true,
        data: {},
      },
    };

    sources.forEach(({ key }) => {
      this.state.dataflow.data[key] = [];
    });

    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.fetchData(this.props.searchQuery);
  }

  fetchData(query) {
    // news
    Content.list('news', query, 5)
      .then(({ data }) => this.setState({
        news: {
          loading: false,
          data: isArray(data.data) ? data.data : [],
        },
      }))
      .catch(() => this.setState({ news: { data: [], loading: false } }));

    // publication
    Content.list('publication', query, 5)
      .then(({ data }) => this.setState({
        publication: {
          loading: false,
          data: isArray(data.data) ? data.data : [],
        },
      }))
      .catch(() => this.setState({ publication: { data: [], loading: false } }));

    // data service
    axios.all(sources.map(({ key }) => DataService.getDataflows(key, query)))
      .then((results) => {
        const dataflow = {};

        sources.forEach(({ key }, i) => {
          const data = results[i].data;
          dataflow[key] = isArray(data.content) ? data.content : [];
        });

        this.setState({ dataflow: { data: dataflow, loading: false } });
      });

    Router.replace(`/search?q=${query}`);
  }

  render() {
    const { news = {}, publication = {}, dataflow = {} } = this.state;
    const { searchQuery } = this.props;

    return (
      <Layout title="News" router={this.props.router} onSearch={this.fetchData}>
        {/** Search Input */}
        <div className="bb b--light-gray pb2 flex items-center justify-center">
          <Input.Search
            allowClear
            placeholder="Search"
            className="pa2 w-50"
            defaultValue={searchQuery}
            onSearch={value => {
              this.fetchData(value);
              Router.replace(`/search?q=${value}`);
            }}
          />
        </div>

        <Tabs type="card" className="ml5-l mt3">
          <Tabs.TabPane
            key="news"
            tab={<div><Icon type="profile" /> News</div>}
            className="ph3 ba bt-0 b--light-gray nt4 pv4 mb4"
          >
            <NewsResults
              data={news.data}
              loading={news.loading}
            />
          </Tabs.TabPane>
          <Tabs.TabPane
            key="publication"
            tab={<div><Icon type="notification" /> Publication</div>}
            className="ph3 ba bt-0 b--light-gray nt4 pv4 mb4"
          >
            <PublicationResults
              data={publication.data}
              loading={publication.loading}
            />
          </Tabs.TabPane>
          <Tabs.TabPane
            key="data-service"
            tab={<div><Icon type="file-text" /> Data Service</div>}
            className="ph3 ba bt-0 b--light-gray nt4 pv4 mb4"
          >
            <Tabs defaultActiveKey="1" tabPosition="left">
              {sources.map(({ key, label, url }) => !!dataflow.data[key] && (
                <Tabs.TabPane tab={label} key={key}>
                  <DataflowResults
                    data={dataflow.data[key]}
                    urlResolver={id => `${url}/view/${id}`}
                    loading={dataflow.loading}
                  />
                </Tabs.TabPane>
              ))}
            </Tabs>
          </Tabs.TabPane>
        </Tabs>
      </Layout>
    );
  }
}

export default page(ContentList);
