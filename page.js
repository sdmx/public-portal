import withRedux from 'next-redux-wrapper';
import { withRouter } from 'next/router';

import store from '~/store';

// export default component => withAuth(withRedux(store)(component));
export default (component, mapStateToProps = null, mapDispatchToProps = null) => {
    return withRedux(store, mapStateToProps, mapDispatchToProps)(withRouter(component));
}
// export default component => withRouter(component);
