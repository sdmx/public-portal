const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes.add('/content/:contentType', '/content/index')
routes.add('/content/:contentType/:id', '/content/view')

routes.add('/data/sdmx/:provider', '/data/sdmx/list')
routes.add('/data/sdmx/:provider/:type/:id', '/data/sdmx/view')

routes.add('/data/internal', '/data/internal/list')
routes.add('/data/internal/build/:id', '/data/internal/build')
routes.add('/data/internal/view/:id', '/data/internal/view')

routes.add('/data/bps/view/:id', '/data/bps/view')

routes.add('/report/view/:path*', '/report/view')
