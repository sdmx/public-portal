require('dotenv').config();

const express = require('express');
const session = require('express-session');
const compression = require('compression');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const next = require('next');

const nextRoutes = require('./routes');
const auth = require('./auth');

const dev = process.env.NODE_ENV !== 'production';
const port = parseInt(process.env.HTTP_PORT, 10) || 3001;
const nextApp = next({ dev });

nextApp.prepare()
  .then(() => {
    const app = express();

    if (!dev) {
      app.use(compression());
    }

    app.use(session({
      secret: process.env.SECRET_KEY,
      resave: true,
      saveUninitialized: true,
      // cookie: { secure: true }
    }));

    app.use(cors());
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    // Handle Authentication
    auth(app);

    app.use(nextRoutes.getRequestHandler(nextApp));
    app.get('*', nextApp.getRequestHandler());

    app.listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  });
