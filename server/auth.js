const jwt = require("jsonwebtoken");
const axios = require("axios");
const url = require('url');

const REDIRECT_URI = "/";

module.exports = (app) => {
  app.get('/auth/login', function(req, res) {
    res.redirect(url.format({
      pathname: `${process.env.AUTH_ENDPOINT}/authenticate`,
      query: {
        client_id: process.env.APP_CODE,
        redirect_url: url.format({
          protocol: req.protocol,
          host: req.get('host'),
          pathname: "/auth/callback",
        }),
      },
    }));

    res.end();
  });

  app.get('/auth/logout', function(req, res) {
    const { token } = req.session;

    jwt.verify(token, process.env.SECRET_KEY, function(err, decoded) {
      if (!err) {
        req.session.destroy(() => {
          res.redirect(url.format({
            pathname: `${process.env.AUTH_ENDPOINT}/logout`,
            query: {
              client_id: process.env.APP_CODE,
              redirect_url: url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: REDIRECT_URI,
              })
            },
          }));
          res.end();
        });
      }
      else {
        res.status(401);
        res.end();
      }
    });
  });

  app.get('/auth/callback', function(req, res) {
    const { token } = req.query;

    jwt.verify(token, process.env.SECRET_KEY, function(err, decoded) {
      if (!err) {
        req.session.token = token;
      }

      res.redirect(REDIRECT_URI);
      res.end();
    });
  });

  app.get("/auth/user", function(req, res) {
    jwt.verify(req.session.token, process.env.SECRET_KEY, function(err, decoded) {
      if (!err) {
        axios.get(`${process.env.AUTH_ENDPOINT}/auth/info/${req.session.token}`)
          .then(({ data }) => res.send(data))
          .catch(err => {
            res.status(401);
            res.send(err);
          });
      }
      else {
        res.status(401);
        res.end();
      }
    });
  });

  app.get('/auth/check', function(req, res) {
    jwt.verify(req.session.token, process.env.SECRET_KEY, function(err, decoded) {
      console.log(err, req.session.token, decoded);
      if (err) res.status(401);
      res.end();
    });
  });
}
