module.exports = {
  presets: ['next/babel'],
  plugins: [
    ['module-resolver', { alias: { '~': '.' } }],
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    // ["import", {"libraryName": "antd", "libraryDirectory": "es", "style": "css"}],
    'styled-jsx/babel',
    'lodash',
  ],
};
