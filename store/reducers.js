import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import dataflow from '~/store/data-service/dataflow';
import dsd from '~/store/data-service/dsd';
import dataset from '~/store/data-service/dataset';

export default combineReducers({ form, dataflow, dsd, dataset });
