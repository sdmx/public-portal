import http from 'axios';

/* ACTIONS */
export const type = {
  REQ_DATAFLOW: 'REQ_DATAFLOW',
  RCV_DATAFLOW: 'RCV_DATAFLOW',
};

export const reqDataflow = query => ({ type: type.REQ_DATAFLOW, query });

export const rcvDataflow = (query, data) => ({ type: type.RCV_DATAFLOW, query, data });

export const findDataflow = (query = '', params = { page: 1 }) => ((dispatch) => {
  dispatch(reqDataflow(query));

  return http.get(`${process.env.SDMX_ENDPOINT}/data?page=${params.page}&q=${query}`)
    .then((res) => {
      dispatch(rcvDataflow(query, res.data));
    })
    .catch(() => {
      dispatch(rcvDataflow(query, { content: [] }));
    });
});

/* REDUCERS */

export default (state = { data: { content: [] } }, action) => {
  switch (action.type) {
    case type.RCV_DATAFLOW:
      // return { ...state, ...action, loading: false );
      return { ...state, ...action };

    // case type.REQ_DATAFLOW:
    //   return { ...state, ...action, loading: true );

    default: return state;
  }
};
