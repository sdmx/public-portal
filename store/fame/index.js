import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import thunkMiddleware from 'redux-thunk';

import dataflow from './dataflow';
import dataset from './dataset';

export default initialState => createStore(
  combineReducers({ form, dataflow, dataset }),
  initialState,
  applyMiddleware(thunkMiddleware),
);
