import axios from 'axios';
import _ from 'lodash';

/* ACTIONS */
export const type = {
  REQ_DATASET: 'REQ_DATASET',
  RCV_DATASET: 'RCV_DATASET',
};

export const reqDataset = id => ({ type: type.REQ_DATASET, id });

export const rcvDataset = (id, dataset) => ({ type: type.RCV_DATASET, id, data: dataset });

export const getDataset = (
  source,
  id,
  key = null,
  startPeriod = null,
  endPeriod = null,
) => ((dispatch) => {
  dispatch(reqDataset(id));
  const params = _.omitBy({ key, startPeriod, endPeriod }, _.isNull);

  return axios.get(`${process.env.SDMX_ENDPOINT}/data/${source}/${id}`, { params })
    .then((res) => {
      dispatch(rcvDataset(id, res.data));
    });
  // .catch((err) => {
  //   dispatch(rcvDataset(query, { content: [] }));
  // });
});

/* REDUCERS */

export default (state = { }, action) => {
  switch (action.type) {
    case type.RCV_DATASET:
      return {
        ...state,
        id: action.id,
        data: action.data,
      };

    default: return state;
  }
};
