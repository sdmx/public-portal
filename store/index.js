import { createStore, applyMiddleware, combineReducers } from 'redux';
import loggerMiddleware from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import reducers from './reducers';

export default initialState => {
  const store = createStore(reducers, initialState, applyMiddleware(thunkMiddleware, loggerMiddleware));

  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept("./reducers", () => store.replaceReducer(require("./reducers").default));
  }

  return store;
}
