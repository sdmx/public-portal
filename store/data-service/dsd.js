import { createAction, handleActions } from 'redux-actions';

import { DataService } from '~/utils/api';

/* ACTIONS */

export const received = createAction('DSD_RECEIVED');
export const request = createAction('DSD_REQUEST');
export const failed = createAction('DSD_FAILED');

export const getDimensions = (dataIds) => (
  (dispatch) => {
    dispatch(request());

    return DataService.getDimensions(dataIds)
      .then(({ data }) => dispatch(received(data)))
      .catch(err => dispatch(failed({ message: 'Failed to get the dimensions.', err })));
  }
);

/* REDUCERS */

export default handleActions({
  [request]: ({ error, ...state }, { payload }) => ({
    ...state,
    ...payload,
    isFinished: false,
  }),
  [failed]: (state, { payload }) => ({
    ...state,
    isFinished: true,
    error: payload,
  }),
  [received]: ({ error, ...state }, { payload }) => ({
    ...state,
    isFinished: true,
    dimensions: payload,
  }),
}, { isFinished: false });
