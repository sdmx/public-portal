import { createAction, handleActions } from 'redux-actions';
import numeral from 'numeral';

import { DataService } from '~/utils/api';
import utils from '~/utils';

/* ACTIONS */

export const received = createAction('DATASET_RECEIVED');
export const request = createAction('DATASET_REQUEST');
export const failed = createAction('DATASET_FAILED');

export const getSeries = (dataIds, filter = {}, startPeriod = null, endPeriod = null) => (
  (dispatch) => {
    const filterParams = { dataIds, filter, startPeriod, endPeriod };

    dispatch(request({ filterParams }));

    // dispatch(failed({ message: 'Test' }));
    // return new Promise((resolve, reject) => { resolve() });

    return DataService.getSeries(dataIds, filter, startPeriod, endPeriod)
      .then(({ data }) => dispatch(received(data.map(({ keys: { ID, CONNECTORS_AUTONAME, OBS_VALUE, ...keys } }) => ({
        ...keys,
        OBS_VALUE: utils.isNumeric(OBS_VALUE) ? numeral(OBS_VALUE).value() : 0,
      })))))
      .catch(err => dispatch(failed({ message: 'Failed to get the series.', err })));
  }
);

/* REDUCERS */

export default handleActions({
  [request]: ({ error, ...state }, { payload }) => ({
    ...state,
    ...payload,
    isFinished: false,
  }),
  [failed]: (state, { payload }) => ({
    ...state,
    isFinished: true,
    error: payload,
  }),
  [received]: ({ error, ...state }, { payload }) => ({
    ...state,
    isFinished: true,
    series: payload,
  }),
}, { isFinished: true, series: [] });
