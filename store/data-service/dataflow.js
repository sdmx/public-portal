import { createAction, handleActions } from 'redux-actions';
import Router from 'next/router';
import uuid from 'uuid/v1';
import _ from 'lodash';

import { Storage, DataService } from '~/utils/api';

/* ACTIONS */

export const received = createAction('DATAFLOWS_RECEIVED');
export const request = createAction('DATAFLOWS_REQUEST');
export const failed = createAction('DATAFLOWS_FAILED');
export const toggleSelect = createAction('DATAFLOWS_TOGGLE_SELECT');
export const select = createAction('DATAFLOWS_SELECT');
export const unselect = createAction('DATAFLOWS_UNSELECT');

export const getDataflows = (provider, query, pageNum = 1, pageSize = 10, queryParams = {}) => (
  (dispatch) => {
    const param = { provider, query, pageNum, pageSize, queryParams };
    dispatch(request({ param }));

    return DataService.getDataflows(provider, query, pageNum, pageSize, queryParams)
      .then(({ data }) => dispatch(received(data)))
      .catch(err => dispatch(failed({ message: 'Failed to get the dataflow.', err })));
  }
);

export const toggleSelectDataflow = (dataId) => (
  dispatch => dispatch(toggleSelect(dataId))
);

export const selectDataflow = (dataId) => (
  dispatch => dispatch(select(dataId))
);

export const unselectDataflow = (dataId) => (
  dispatch => dispatch(unselect(dataId))
);

export const buildDataset = (provider, selected) => (
  (dispatch) => {
    const expireTime = new Date().getTime() + (1000 * 3600 * 24 * 3); // 3 days
    const buildId = uuid();

    dispatch(request());

    Storage.set(buildId, selected.join('|'), expireTime);
    Router.push(`/data/sdmx/${provider}/build/${buildId}`);
  }
);

/* REDUCERS */

export default handleActions({
  [request]: (state, { payload }) => ({
    ...state,
    ...payload,
    isFinished: false,
  }),
  [failed]: (state, { payload }) => ({
    ...state,
    isFinished: true,
    error: payload,
  }),
  [received]: (state, { payload }) => ({
    ...state,
    isFinished: true,
    payload,
  }),
  [select]: ({ selectedDataIds, ...state }, { payload }) => ({
    ...state,
    selectedDataIds: [...selectedDataIds, payload],
  }),
  [unselect]: ({ selectedDataIds, ...state }, { payload }) => {
    const nextSelected = [...selectedDataIds];
    const index = nextSelected.indexOf(payload);

    if (index !== -1) {
      nextSelected.splice(index, 1);
    }

    return { ...state, selectedDataIds: nextSelected };
  },
  [toggleSelect]: ({ selectedDataIds, ...state }, { payload }) => {
    const nextSelected = [...selectedDataIds];
    const index = nextSelected.indexOf(payload);

    if (index !== -1) {
      nextSelected.splice(index, 1);
    }
    else {
      nextSelected.push(payload);
    }

    return { ...state, selectedDataIds: nextSelected };
  },
}, { isFinished: false, payload: {}, selectedDataIds: [], param: { query: '' } });
